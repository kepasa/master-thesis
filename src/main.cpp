#include <iostream>
#include <compression/szip/szipe/szipe.h>
#include <generators/generators.h>
#include <compression/lms/lms.h>
#include <utils/graph.h>
#include "benchmarks/nauty_toolkit.h"
#include <benchmarks/graph_benchmarks.h>
#include <io/graph_io.h>
#include "loguru.hpp"
#include "cxxopts.hpp"

PUNGraph getTestGraph() {
    PUNGraph graph = PUNGraph::New();

    // a - d
    graph->AddEdge2(0, 3);
    graph->AddEdge2(3, 0);

    // a - e
    graph->AddEdge2(0, 4);
    graph->AddEdge2(4, 0);

    // b - c
    graph->AddEdge2(1, 2);
    graph->AddEdge2(2, 1);

    // b - f
    graph->AddEdge2(1, 5);
    graph->AddEdge2(5, 1);

    // c - d
    graph->AddEdge2(2, 3);
    graph->AddEdge2(3, 2);

    // c - f
    graph->AddEdge2(2, 5);
    graph->AddEdge2(5, 2);

    // c - g
    graph->AddEdge2(2, 6);
    graph->AddEdge2(6, 2);

    // c - h
    graph->AddEdge2(2, 7);
    graph->AddEdge2(7, 2);

    // d - e
    graph->AddEdge2(3, 4);
    graph->AddEdge2(4, 3);

    // d - f
    graph->AddEdge2(3, 5);
    graph->AddEdge2(5, 3);

    // d - h
    graph->AddEdge2(3, 7);
    graph->AddEdge2(7, 3);

    // d - i
    graph->AddEdge2(3, 8);
    graph->AddEdge2(8, 3);

    // e - g
    graph->AddEdge2(4, 6);
    graph->AddEdge2(6, 4);

    // e - h
    graph->AddEdge2(4, 7);
    graph->AddEdge2(7, 4);

    // f - g
    graph->AddEdge2(5, 6);
    graph->AddEdge2(6, 5);

    // f - h
    graph->AddEdge2(5, 7);
    graph->AddEdge2(7, 5);

    // f - i
    graph->AddEdge2(5, 8);
    graph->AddEdge2(8, 5);

    // f - j
    graph->AddEdge2(5, 9);
    graph->AddEdge2(9, 5);

    // g - h
    graph->AddEdge2(6, 7);
    graph->AddEdge2(7, 6);

    // g - i
    graph->AddEdge2(6, 8);
    graph->AddEdge2(8, 6);

    // g - j
    graph->AddEdge2(6, 9);
    graph->AddEdge2(9, 6);

    // h - j
    graph->AddEdge2(7, 9);
    graph->AddEdge2(9, 7);

    // i - j
    graph->AddEdge2(8, 9);
    graph->AddEdge2(9, 8);

    return graph;
}

void printProgramHelp() {
    std::cout << "Unrecognized program. Possible options are:" << std::endl
              << "\t --p=generate --g=ER,<numberOfNodes>,<edgeProbability>" << std::endl
              << "\t --p=generate --g=PA,<numberOfNodes>,<parameterM>" << std::endl
              << "\t --p=generate --g=DD,<numberOfNodes>,<parameterP>,<parameterR>,<pathToSeedGraph>" << std::endl
              << "\t --p=benchmark --b=<nameOfBenchmark>/\"benchmarkAll\"" << std::endl;
}

void runER(const std::vector<std::string> &generator, bool isDraw, bool isDegDist) {
    auto noNodes = std::stoi(generator[1]);
    auto p = std::stod(generator[2]);
    int noEdges = int(double(noNodes * (noNodes - 1)) / (double) 2) * p;
    LOG_F(INFO, "Generating ER %d %d", noNodes, noEdges);
    auto graph = Graph::Generators::GenerateER(noNodes, noEdges);
    LOG_F(INFO, "Graph generated");

    auto graphName = "ER_" + generator[1] + "_" + std::to_string(noEdges);
    auto outputPath = "files/generated_graphs/" + graphName;
    if (isDraw) {
        TSnap::DrawGViz<PUNGraph>(graph, gvlSfdp, (outputPath + ".png").c_str());
        LOG_F(INFO, "Drawn graph");
    }
    TSnap::SaveEdgeList<PUNGraph>(graph, (outputPath + ".txt").c_str());
    if (isDegDist) {

        TSnap::PlotInDegDistr(graph, (graphName + "_degrees").c_str());
        LOG_F(INFO, "Generated degree distribution");
    }
}

void runPA(const std::vector<std::string> &generator, bool isDraw, bool isDegDist) {
    auto noNodes = std::stoi(generator[1]);
    auto m = std::stoi(generator[2]);
    LOG_F(INFO, "Generating PA %d %d", noNodes, m);

    auto graph = Graph::Generators::GeneratePE(noNodes, m, true);

    auto graphName = "PA_" + generator[1] + "_" + generator[2];
    auto outputPath = "files/generated_graphs/" + graphName;
    if (isDraw) {
        TSnap::DrawGViz<PNEGraph>(graph, gvlSfdp, (outputPath + "_DIR.png").c_str());
        LOG_F(INFO, "Directed drawn");
    }
    outputPath = graphName;
    if (isDegDist) {
        TSnap::PlotInDegDistr(graph, (outputPath + "_DIR_degrees").c_str());
        LOG_F(INFO, "Plotted in deg distribution");
    }

    auto graphUndir = TSnap::ConvertGraph<PUNGraph>(graph);
    outputPath = "files/generated_graphs/" + graphName;
    if (isDraw) {
        TSnap::DrawGViz<PUNGraph>(graphUndir, gvlSfdp, (outputPath + "_UNDIR.png").c_str());
        LOG_F(INFO, "Undirected drawn");
    }
    TSnap::SaveEdgeList<PNEGraph>(graph, (outputPath + ".txt").c_str());
    LOG_F(INFO, "Edge list saved");

    if (isDegDist) {
        outputPath = graphName;
        TSnap::PlotInDegDistr(graph, (outputPath + "_UNDIR_degrees").c_str(), "Degree distribution", false, true);
        LOG_F(INFO, "Plotted undirected in deg distribution");
    }
}

void runDD(const std::vector<std::string> &generator, bool isDraw, bool isDegDist) {
    auto noNodes = std::stoi(generator[1]);
    auto p = std::stod(generator[2]);
    auto r = std::stod(generator[3]);
    auto seedGraph = IO::read_graph("files/generated_graphs/" + generator[4] + ".txt", true);

    LOG_F(INFO, "Generating DD %d %s", noNodes, generator[4].c_str());

    auto graph = Graph::Generators::GenerateDD(noNodes, p, r, seedGraph);

    LOG_F(INFO, "Graph generated");

    auto graphName = "DD_" + generator[1] + "_" + generator[2] + "_" + generator[3] + "_" + generator[4];
    auto outputPath = "files/generated_graphs/" + graphName;
    if (isDraw) {
        TSnap::DrawGViz<PUNGraph>(graph, gvlSfdp, (outputPath + ".png").c_str());
        LOG_F(INFO, "Graph drawn");
    }
    TSnap::SaveEdgeList<PUNGraph>(graph, (outputPath + ".txt").c_str());
    if (isDegDist) {
        auto maxCC = TSnap::GetMxScc(graph);
        outputPath = graphName;
        TSnap::PlotInDegDistr(maxCC, (outputPath + "_degrees").c_str(), "Degree distribution", false, true);
        LOG_F(INFO, "Plotted deg distribution");
    }
}

void runBenchmark(const std::string &benchmarkName, bool isoCheck) {
    std::vector<int> graphSizes(120);
    std::generate(graphSizes.begin(), graphSizes.end(), [n = 1000]() mutable {
        if (n < 10000) n += 100;
        else n += 1000;
        return n;
    });

    std::vector<int> graphSizesSuiteBenchmarks = {
            1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 12000, 15000
    };

    if (benchmarkName == "benchmarkAll") {
        Graph::Benchmark::modelGraphsBench1(graphSizes, isoCheck);
        Graph::Benchmark::modelGraphsBench2(isoCheck);
        Graph::Benchmark::modelGraphsBench3(graphSizes, isoCheck);
        Graph::Benchmark::modelGraphsBench4(isoCheck);
//        Graph::Benchmark::modelGraphsBench5(graphSizesSuiteBenchmarks, isoCheck, 100, 0.01);
//        Graph::Benchmark::modelGraphsBench5(graphSizesSuiteBenchmarks, isoCheck, 100, 0.2);
        Graph::Benchmark::modelGraphsBench6(graphSizesSuiteBenchmarks, isoCheck);
        Graph::Benchmark::modelGraphsBench7(isoCheck);
        Graph::Benchmark::realWorldGraphs();
        Graph::Benchmark::realWorldGraphsGiant();
    } else if (benchmarkName == "benchmark1") {
        Graph::Benchmark::modelGraphsBench1(graphSizes, isoCheck, 100);
    } else if (benchmarkName == "benchmark2") {
        Graph::Benchmark::modelGraphsBench2(isoCheck, 100);
    } else if (benchmarkName == "benchmark3") {
        Graph::Benchmark::modelGraphsBench3(graphSizes, isoCheck, 100);
    } else if (benchmarkName == "benchmark4") {
        Graph::Benchmark::modelGraphsBench4(isoCheck, 100);
    } else if (benchmarkName == "benchmark5") {
//        Graph::Benchmark::modelGraphsBench5(graphSizesSuiteBenchmarks, isoCheck, 10, 0.2);
//        Graph::Benchmark::modelGraphsBench5(graphSizesSuiteBenchmarks, isoCheck, 100, 0.2);
    } else if (benchmarkName == "benchmark6") {
        Graph::Benchmark::modelGraphsBench6(graphSizesSuiteBenchmarks, isoCheck);
    } else if (benchmarkName == "benchmark7") {
        Graph::Benchmark::modelGraphsBench7(isoCheck);
    } else if (benchmarkName == "realWorld") {
        Graph::Benchmark::realWorldGraphs();
    } else if (benchmarkName == "realWorldGiant") {
        Graph::Benchmark::realWorldGraphsGiant();
    } else if (benchmarkName == "testErrBars") {
        Graph::Benchmark::testErrBars(graphSizes, 1000);
    } else if (benchmarkName == "paSmall") {
        Graph::Benchmark::modelGraphsBench5(graphSizesSuiteBenchmarks, isoCheck, 10, [](int n) {
            return ::Utils::dirToUndir(Graph::Generators::GeneratePE(n, 3, true));
        }, "paSmall");
    } else if (benchmarkName == "paLarge") {
        Graph::Benchmark::modelGraphsBench5(graphSizesSuiteBenchmarks, isoCheck, 10, [](int n) {
            return ::Utils::dirToUndir(Graph::Generators::GeneratePE(n, 120, true));
        }, "paLarge");
    } else if (benchmarkName == "ddSmall") {
        auto seed = IO::read_graph("files/generated_graphs/ER_20_190.txt", true);
        Graph::Benchmark::modelGraphsBench5(graphSizesSuiteBenchmarks, isoCheck, 10, [seed](int n) {
            return Graph::Generators::GenerateDD(n, 0.3, 0.7, seed);
        }, "ddSmall");
    } else if (benchmarkName == "ddLarge") {
        auto seed = IO::read_graph("files/generated_graphs/ER_20_190.txt", true);
        Graph::Benchmark::modelGraphsBench5(graphSizesSuiteBenchmarks, isoCheck, 10, [seed](int n) {
            return Graph::Generators::GenerateDD(n, 0.6, 4, seed);
        }, "ddLarge");
    } else if (benchmarkName == "erLarge") {
        const double p = 0.2;
        Graph::Benchmark::modelGraphsBench5(graphSizesSuiteBenchmarks, isoCheck, 10, [p](int n) {
            int noEdges = int((double((ull) n * ((ull) n - 1)) / (double) 2) * p);
            return Graph::Generators::GenerateER(n, noEdges);
        }, "erLarge");
    } else {
        LOG_F(ERROR, "Incorrect benchmark name");
    }
}


int main(int argc, const char **argv) {

    loguru::g_stderr_verbosity = loguru::Verbosity_INFO;
    loguru::init(argc, const_cast<char **>(argv));

    LOG_SCOPE_F(INFO, "Main");
    LOG_F(INFO, "Logger initiated");

    cxxopts::Options options("Random graph compressors", "Benchmarks for random graph compressors");

    // TODO(pawelp): add compression program and option to draw graphs with vertex numbers / debug option for compressors.
    options.add_options()
            ("p,program", "Program to be run.", cxxopts::value<std::string>()->default_value("None"))
            ("g,generator", "Generator to be used in case p=generate",
             cxxopts::value<std::vector<std::string>>())
            ("b,benchmark", "Benchmark to be run", cxxopts::value<std::string>()->default_value("None"))
            ("isoCheck", "Check for isomorphism in benchmarks", cxxopts::value<bool>()->default_value("false"))
            ("c,compress", "Print compressed graph size", cxxopts::value<std::vector<std::string>>())
            ("dbg", "Prints debug information in compression algorithms",
             cxxopts::value<bool>()->default_value("false"))
            ("draw", "Draws generated graph using GraphViz",
             cxxopts::value<bool>()->default_value("false"))
            ("degDist", "Plots degree distribution of generated graph",
             cxxopts::value<bool>()->default_value("false"));

    auto parsedOptions = options.parse(argc, argv);

    auto program = parsedOptions["p"].as<std::string>();
    if (program == "generate") {
        auto generator = parsedOptions["g"].as<std::vector<std::string>>();
        bool isDraw = parsedOptions["draw"].as<bool>();
        bool isDegDist = parsedOptions["degDist"].as<bool>();

        if (generator.empty()) {
            printProgramHelp();
            exit(1);
        }

        if (generator[0] == "ER") {
            runER(generator, isDraw, isDegDist);
        } else if (generator[0] == "PA") {
            runPA(generator, isDraw, isDegDist);
        } else if (generator[0] == "DD") {
            runDD(generator, isDraw, isDegDist);
        }
        return 0;
    } else if (program == "benchmark") {
        auto benchmarkName = parsedOptions["b"].as<std::string>();
        auto isoCheck = parsedOptions["isoCheck"].as<bool>();

        if (benchmarkName == "None") {
            printProgramHelp();
            exit(1);
        }

        runBenchmark(benchmarkName, isoCheck);

        return 0;
    } else if (program == "compress") {
        auto arguments = parsedOptions["c"].as<std::vector<std::string>>();
        auto isoCheck = parsedOptions["isoCheck"].as<bool>();
        auto printDbg = parsedOptions["dbg"].as<bool>();

        if (arguments.empty()) {
            printProgramHelp();
            exit(1);
        }

        if (printDbg) {
            loguru::g_stderr_verbosity = 1;
        }

        auto graph = IO::read_graph(arguments[0], true);
        LOG_F(INFO, "Graph read from: %s", arguments[0].c_str());
        auto compressor = arguments[1];

        if (compressor == "szip") {
            auto szipeStatic = Graph::Compression::SzipE(false, true, printDbg);
            auto encodedGraph = szipeStatic.encode(graph);
            if (isoCheck) {
                auto decodedGraph = szipeStatic.decode(encodedGraph);
                LOG_F(INFO, "Performed iso check: %d",
                      Nautytoolkit::check_isomorphic_nauty(graph, decodedGraph));
            }
            auto sizeStatic = 4 * 64 + encodedGraph.B1.size() + encodedGraph.B2.size();
            LOG_F(INFO, "Encoded size: %d", sizeStatic);
        } else if (compressor == "lms") {
            if (printDbg) {
                TIntStrH name;
                for (auto i = graph->BegNI(); i < graph->EndNI(); i++) {
                    name.AddDat(i.GetId()) = (std::to_string(i.GetId())).data();
                }
                TSnap::DrawGViz(graph, gvlDot, "LMS.png", "", name);
            }
            auto lms = Graph::Compression::LMS(-1, printDbg);
            auto encodedLms = lms.encode(::Utils::undirToDir(graph));

            if (isoCheck) {
                auto decodedGraph = ::Utils::dirToUndir(lms.decode(encodedLms));
                LOG_F(INFO, "Performed iso check: %d",
                      Nautytoolkit::check_isomorphic_nauty(graph, decodedGraph));
            }
            auto sizeLms =
                    2 * 64 + encodedLms.graphCode.size() + encodedLms.isStarting.size() + encodedLms.Bj.code.size() +
                    encodedLms.Bj.symbolToCodelen.size() * (encodedLms.Bj.codelenBits + encodedLms.Bj.symbolBits);
            LOG_F(INFO, "Encoded size: %d", sizeLms);
        }

        return 0;
    } else {
        printProgramHelp();

        exit(1);
    }

    const auto &isIsomorphic = Nautytoolkit::check_isomorphic_nauty;
    auto compressor = Graph::Compression::LMS(-1);




//    for (int noNodes = 75; noNodes < 1000; ++noNodes) {
//
//        for (int noEdges = ((noNodes * (noNodes - 1)) / 8); noEdges <= ((noNodes * (noNodes - 1)) / 8) + 10; ++noEdges) {
//            auto graph = Graph::Generators::GenerateER(noNodes, noEdges);
//            auto compressedGraph = compressor.encode(Utils::undirToDir(graph));
//            LOG_F(INFO, "Compressed");
//
//            auto decodedGraph = Utils::dirToUndir(compressor.decode(compressedGraph));
//            LOG_F(INFO, "Decoded");
//
//            //            if(!isIsomorphic(graph, decodedGraph)) {
////                graph->Dump();
////                decodedGraph->Dump();
////                TSnap::DrawGViz<PUNGraph>(decodedGraph, gvlDot, "gviz_plot_out.png", "", "decoded");
////                TSnap::DrawGViz<PUNGraph>(graph, gvlDot, "gviz_plot_in.png", "", "input");
////            }
//            auto isIso = isIsomorphic(graph, decodedGraph);
//            LOG_F(INFO, "Done: (%d, %d), %d", noNodes, noEdges, isIso);
//            assert(isIso);
//        }
//    }

//    return 0;
//    auto erGraph = getTestGraph();
//
//    erGraph->Dump();
//
//    auto vec = Utils::extractBits(7, 0, 2);
//    for(char s : vec) {
//        std::cout << s << " ";
//    }
//    std::cout << std::endl;
//
//    auto szipnCompressor = Graph::Compression::SzipN();
//    auto result = szipnCompressor.encode(erGraph);
//
//    std::cout << "N: " << result.n << std::endl << "B1: ";
//    for(short c : result.B1) {
//        std::cout << c << " ";
//    }
//    std::cout << std::endl << "B2: ";
//    for(short c : result.B2) {
//        std::cout << c << " ";
//    }
//    std::cout << std::endl;
//
//    auto graph = szipnCompressor.decode(result);
//
//    auto converted = TSnap::ConvertGraph<PNGraph, PUNGraph>(graph, false);
//    auto inputConverted = TSnap::ConvertGraph<PNGraph, PUNGraph>(erGraph, false);
//    graph->Dump();
//    converted->Dump();
//
//    TGHash<TInt> GHash(false);
//    TInt::Rnd.PutSeed(time(nullptr));
//    GHash.AddDat(converted)++;
//    GHash.AddDat(inputConverted)++;
//    std::cout << "number of graphs in hash table: " << GHash.Len() << std::endl;
//
//    auto szipEtestGraph = Graph::Generators::GenerateER(6, 6);
//    szipEtestGraph->Dump();
//    auto szipE = Graph::Compression::SzipE();
//    auto result2 = szipE.encode(getTestGraph());
////    auto result2 = szipE.encode(getTestGraph());
//    std::cout << "N: " << result2.n << std::endl << "B1: ";
//    for(short c : result2.B1) {
//        std::cout << c << " ";
//    }
//    std::cout << std::endl << "B2: ";
//    for(short c : result2.B2) {
//        std::cout << c << " ";
//    }
//    std::cout << std::endl;
//    szipE.decode(result2)->Dump();

//    auto peGraph = TSnap::LoadEdgeList<PNEGraph>("PETestGraph.txt");
//    // duplicate vert 0
//    int newNodeId = peGraph->GetNodes();
//    peGraph->AddNode(newNodeId);
//    auto nodeI = peGraph->GetNI(0);
//    for(auto i = 0; i < peGraph->GetNI(0).GetOutDeg(); ++i) {
//        auto edgeId = nodeI.GetOutEId(i);
//        auto edge = peGraph->GetEI(edgeId);
//        auto dstId = edge.GetDstNId();
//
//        if(dstId != 0) { // copying self-loops
//            peGraph->AddEdge(newNodeId, dstId);
//            peGraph->AddEdge(dstId, newNodeId);
//        }
//    }
//
//    while(true) {
//        auto selfloopEI = peGraph->GetEId(0, 0);
//        if(selfloopEI == -1) break;
//        peGraph->DelEdge(selfloopEI);
//    }
//
//    peGraph->Dump();
//    TSnap::DrawGViz<PNEGraph>(peGraph, gvlDot, "gviz_plot_input.png", "");
//    Graph::Compression::LMS lms(0);
//
//    auto result = lms.encode(peGraph);
//
//
//    auto decodedGraph = lms.decode(result);
//    TSnap::DrawGViz<PNEGraph>(decodedGraph, gvlDot, "gviz_plot_output.png", "");
//
//    decodedGraph->Dump();
//
//    bool isIso = Utils::checkIsomorphic<PNEGraph>(peGraph, decodedGraph);
//    std::cout << isIso << std::endl;

//    return 0;

//    auto randomGraph = Graph::Generators::GenerateER(1000, 50000);
//
//    TSnap::SaveEdgeList<PUNGraph>(randomGraph, "Test_ER_large.txt");
//    auto szipeCompressor = Graph::Compression::SzipE();
//    auto compressedGraph = szipeCompressor.encode(randomGraph);
//    LOG_F(INFO, "Compressed using %ld bits and structural entropy is: %d",
//          2 * 64 + compressedGraph.B1.size() + compressedGraph.B2.size(),
//          Nautytoolkit::get_structural_entropy(randomGraph));
//
//    auto decodedGraph2 = szipeCompressor.decode(compressedGraph);
//
//    std::cout << "NAUTY IS ISO: " << Nautytoolkit::check_isomorphic_nauty(randomGraph, decodedGraph2) << std::endl;

//
    std::vector<int> sizes = {1000, 2000, 3000, 4000, 5000, 6000};
//    , 7000, 8000, 9000, 10000, 12000, 15000, 20000};

//    Graph::Benchmark::modelGraphsBench1(sizes, false);
//
//    std::vector<int> sizes2(120);
//    std::generate(sizes2.begin(), sizes2.end(), [n = 1000]() mutable {
//        if(n < 10000) n += 100;
//        else n+= 1000;
//        LOG_F(INFO, "%d", n);
//        return n;
//    });
//    Graph::Benchmark::modelGraphsBench1(sizes2, false);

//    Graph::Benchmark::modelGraphsBench2(sizes2, false);
//    Graph::Benchmark::modelGraphsBench3(sizes);
//    Graph::Benchmark::modelGraphsBench5(sizes);
//    auto testGraph = Graph::Generators::GeneratePE(10, 3, true);
//    TSnap::DrawGViz<PNEGraph>(testGraph, gvlDot,
//                              "gviz_plot_output1.png", "");
//    TSnap::DrawGViz<PUNGraph>(TSnap::ConvertGraph<PUNGraph>(testGraph), gvlDot,
//                              "gviz_plot_output2.png", "");
//
//    Graph::Benchmark::modelGraphsBench5(sizes);
//    Graph::Benchmark::realWorldGraphs();
//
//    return 0;
//
//    compressor = Graph::Compression::LMS(0);
//
//
//    for (
//            int noNodes = 3;
//            noNodes < 11; ++noNodes) {
//
//        for (
//                int i = 0;
//                i < 200; ++i) {
//            for (
//                    int noEdges = 3;
//                    noEdges <= 5; ++noEdges) {
//
//                auto graph = Graph::Generators::GeneratePE(noNodes, noEdges);
//                auto compressedGraph = compressor.encode(graph);
//                auto decodedGraph = compressor.decode(compressedGraph);
//
//                std::cerr << "it: " << i << " N: " << noNodes << " E: " << noEdges << " done compressing ";
//                bool isIso = Utils::checkIsomorphic(graph, decodedGraph);
//                std::cerr << " done checking " <<
//                          std::endl;
//                if (!isIso) {
//                    TSnap::DrawGViz<PNEGraph>(graph, gvlDot,
//                                              "gviz_plot_input.png", "");
//                    TSnap::DrawGViz<PNEGraph>(decodedGraph, gvlDot,
//                                              "gviz_plot_output.png", "");
//                    graph->
//
//                            Dump();
//
//                    decodedGraph->
//
//                            Dump();
//
//                    return 0;
//                }
//            }
//        }
//    }
//    return 0;
}
