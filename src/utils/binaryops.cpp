//
// Created by pawel on 5/9/20.
//

#include "binaryops.h"
#include <algorithm>

std::vector<char> Utils::extractBits(int number, int begin, int end) {
    std::vector<char> result;
    for(int i = begin; i < end; ++i) {
        result.push_back((number & 1 << i) >> i);
    }
    std::reverse(result.begin(), result.end());
    return result;
}

int Utils::decodeBits(std::vector<char>::const_iterator begin, std::vector<char>::const_iterator end) {
    int result = 0;

    if(begin == end) return 0;

    for(auto it = begin; it < end; ++it) {
        result |= *it;
        result = result << 1;
    }
    return result >> 1;
}



