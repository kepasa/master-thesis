//
// Created by pawel on 5/9/20.
//

#ifndef MAGISTERKA_BINARYOPS_H
#define MAGISTERKA_BINARYOPS_H

#include <vector>
#include <cassert>

namespace Utils {
    // Extracts bits [begin, end) from number
    std::vector<char> extractBits(int number, int begin, int end);

    // Transforms bits from range to number
    int decodeBits(std::vector<char>::const_iterator begin, std::vector<char>::const_iterator end);

    inline static int log2i_floor(int x) {
        assert(x > 0);
        return sizeof(int) * 8 - __builtin_clz(x) - 1;
    }

    inline static int log2i_ceil(int x) {
        assert(x > 1);
        return log2i_floor(x-1)+1;
    }
}
#endif //MAGISTERKA_BINARYOPS_H
