//
// Created by pawel on 6/13/20.
//

#ifndef MAGISTERKA_GRAPH_H
#define MAGISTERKA_GRAPH_H

#include <Snap.h>
#include <vector>
#include <numeric>
#include <algorithm>

namespace Utils {

    // Called by 'checkIsomorphic'. Checks isomorphicity for a single mapping defined by 'vecMapping'.
    // Maps vertices of g1 using 'vecMapping'
    template<typename T>
    bool checkIsomorphicForMap(const T &g1, const T &g2, std::vector<int> &vecMapping) {
        for (auto it = g1->BegEI(); it < g1->EndEI(); it++) {
            int src = it.GetSrcNId();
            int dst = it.GetDstNId();
            int mappedSrc = vecMapping[src];
            int mappedDst = vecMapping[dst];
            if (!(g2->IsEdge(mappedSrc, mappedDst) && g2->IsEdge(mappedDst, mappedSrc))) return false;
        }
        return true;
    }

    // Naively check every permutation of vertices
    template<typename T>
    bool checkIsomorphic(const T &g1, const T &g2) {
        if (g1->GetNodes() != g2->GetNodes()) return false;
        if (g1->GetEdges() != g2->GetEdges()) return false;
        int noNodes = g1->GetNodes();
        std::vector<int> vecMapping(noNodes);
        std::iota(vecMapping.begin(), vecMapping.end(), 0);
        do {
            if (checkIsomorphicForMap(g1, g2, vecMapping)) return true;
        } while (std::next_permutation(vecMapping.begin(), vecMapping.end()));
        return false;
    }

    // Check vertices but group vertices by degree
    bool checkIsomorphic_V2(const PUNGraph &g1, const PUNGraph &g2);


    template<typename PTGraph>
    PTGraph copyGraph(const PTGraph &graph) {
        PTGraph result = PTGraph::New();
        for (auto i = graph->BegNI(); i < graph->EndNI(); i++) {
            result->AddNode(i.GetId());
        }

        for (auto i = graph->BegEI(); i < graph->EndEI(); i++) {
            result->AddEdge(i);
        }

        return result;
    }

    PNEGraph undirToDir(const PUNGraph &graph);

    PUNGraph dirToUndir(const PNEGraph &graph);
    
    std::vector<char> toAdjacencyMatrix(const PUNGraph& graph, bool packBits = false);

    PUNGraph fromAdjacencyMatrix(const std::vector<char>& adjMatrix, int n, bool packBits = false);
}
#endif //MAGISTERKA_GRAPH_H
