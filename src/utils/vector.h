//
// Created by pawel on 6/5/20.
//

#ifndef MAGISTERKA_VECTOR_H
#define MAGISTERKA_VECTOR_H

#include <vector>
#include <numeric>

namespace Utils {
    // taken from https://stackoverflow.com/questions/6404856/generic-function-to-flatten-a-container-of-containers
    template<typename T>
    std::vector<T> flatten_vec_of_vec(std::vector<std::vector<T>> &v_of_v) {
        std::vector<T> result;

        for(auto& vec : v_of_v) {
            for(auto& entry : vec) {
                result.push_back(entry);
            }
        }

        return result;

//        return std::accumulate(
//                v_of_v.begin(), v_of_v.end(),
//                std::vector<int>(),
//                [](std::vector<int> a, std::vector<int> b) {
//                    a.insert(a.end(), b.begin(), b.end());
//                    return a;
//                });
    }
}
#endif //MAGISTERKA_VECTOR_H
