//
// Created by pawel on 6/13/20.
//

#ifndef MAGISTERKA_TYPEDEFS_H
#define MAGISTERKA_TYPEDEFS_H

#include <utility>

typedef std::pair<int, int> pii;
typedef std::pair<double, std::string> pds;

typedef unsigned long long ull;
#endif //MAGISTERKA_TYPEDEFS_H
