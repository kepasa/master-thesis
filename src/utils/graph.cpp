//
// Created by pawel on 6/13/20.
//

#include <numeric>
#include <algorithm>
#include <iostream>
#include "graph.h"

bool checkIsomorphicForMap_V2(
        int n,
        const PUNGraph &g1,
        const PUNGraph &g2,
        std::vector<std::unordered_map<int, int>> &nodesByDeg,
        std::vector<std::vector<int>> &nodesByDegMap
) {
    for (auto it = g1->BegEI(); it < g1->EndEI(); it++) {
        int src = it.GetSrcNId();
        int dst = it.GetDstNId();
        int srcPos = nodesByDeg[g1->GetNI(src).GetDeg()][src];
        int dstPos = nodesByDeg[g1->GetNI(dst).GetDeg()][dst];
        int mappedSrc = nodesByDegMap[g1->GetNI(src).GetDeg()][srcPos];
        int mappedDst = nodesByDegMap[g1->GetNI(dst).GetDeg()][dstPos];
        if (!(g2->IsEdge(mappedSrc, mappedDst) && g2->IsEdge(mappedDst, mappedSrc))) return false;
    }
    return true;
}

bool checkIsomorphicBacktrack_V2(
        int i,
        int n,
        const PUNGraph &g1,
        const PUNGraph &g2,
        std::vector<std::unordered_map<int, int>> &nodesByDeg,
        std::vector<std::vector<int>> &nodesByDegMap) {
    if (i == n) return checkIsomorphicForMap_V2(n, g1, g2, nodesByDeg, nodesByDegMap);

    do {
        if (checkIsomorphicBacktrack_V2(i + 1, n, g1, g2, nodesByDeg, nodesByDegMap)) {
            return true;
        }
    } while (std::next_permutation(nodesByDegMap[i].begin(), nodesByDegMap[i].end()));

    return false;
}


bool Utils::checkIsomorphic_V2(const PUNGraph &g1, const PUNGraph &g2) {
    const int n = g1->GetNodes();
    if (n != g2->GetNodes()) return false;
    if (g2->GetEdges() != g1->GetEdges()) return false;
    std::vector<std::unordered_map<int, int>> nodesByDegree(n, std::unordered_map<int, int>());
    std::vector<std::vector<int>> nodesMapByDegree(n, std::vector<int>());
    // Split nodes by deg
    for (int i = 0; i < n; ++i) {
        auto currentVertex = g1->GetNI(i);
        nodesByDegree[currentVertex.GetDeg()].insert({i, nodesByDegree[currentVertex.GetDeg()].size()});
    }
    for (int i = 0; i < n; ++i) {
        nodesMapByDegree[g2->GetNI(i).GetDeg()].push_back(i);
    }

    return checkIsomorphicBacktrack_V2(0, n, g1, g2, nodesByDegree, nodesMapByDegree);
}

PNEGraph Utils::undirToDir(const PUNGraph &graph) {
    PNEGraph result = PNEGraph::New();
    for (auto i = graph->BegNI(); i < graph->EndNI(); i++) {
        result->AddNode(i.GetId());
    }

    for (auto i = graph->BegEI(); i < graph->EndEI(); i++) {
        auto src = i.GetSrcNId();
        auto dst = i.GetDstNId();
        result->AddEdge(src, dst);
        result->AddEdge(dst, src);
    }
    return result;
}

PUNGraph Utils::dirToUndir(const PNEGraph &graph) {
    PUNGraph result = PUNGraph::New();
    for (auto i = graph->BegNI(); i < graph->EndNI(); i++) {
        result->AddNode(i.GetId());
    }

    for (auto i = graph->BegEI(); i < graph->EndEI(); i++) {
        auto src = i.GetSrcNId();
        auto dst = i.GetDstNId();
        result->AddEdge(src, dst);
    }
    return result;
}

std::vector<char> Utils::toAdjacencyMatrix(const PUNGraph &graph, bool packBits) {
    std::vector<char> adjMatrix;
    int n = graph->GetNodes();

    for (int i = 0; i < n; ++i) {
        for (int j = i + 1; j < n; ++j) {
            adjMatrix.push_back(graph->IsEdge(i, j));
        }
    }

    if (packBits) {
        std::vector<char> result;
        while (adjMatrix.size() % 8 != 0) {
            adjMatrix.push_back(0);
        }
        for (int i = 0; i < adjMatrix.size(); i += 8) {
            unsigned char current = 0;
            for (int j = 7; j >= 0; j--) {
                current |= (unsigned char) (adjMatrix[i + 7 - j] << j);
            }
            result.push_back(current);
        }
        return result;
    }


    return adjMatrix;
}

PUNGraph Utils::fromAdjacencyMatrix(const std::vector<char> &matrix, int n, bool packBits) {
    PUNGraph result = PUNGraph::New();

    std::vector<char> adjMatrix;

    if (packBits) {
        for (char c : matrix) {
            for (int j = 7; j >= 0; j--) {
                adjMatrix.push_back(((unsigned char)c >> j) & 1);
            }
        }
    } else {
        adjMatrix = matrix;
    }

    if (adjMatrix.empty()) {
        result->AddNode(0);
        return result;
    }

//    int n = (std::sqrt(8*adjMatrix.size() + 1) + 1)/2;
    for (int i = 0; i < n; ++i) {
        result->AddNode(i);
    }
    int idx = 0;
    for (int i = 0; i < n; ++i) {
        for (int j = i + 1; j < n; ++j) {
            if (adjMatrix[idx] == 1) {
                result->AddEdge(i, j);
                result->AddEdge(j, i);
            }
            ++idx;
        }
    }
    return result;
}
