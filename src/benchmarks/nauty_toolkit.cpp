//
// Created by pawel on 8/14/20.
//

#include "nauty_toolkit.h"

/*
 * Based on https://github.com/krzysztof-turowski/duplication-divergence/blob/master/src/dd_automorphisms.h
 * Returns log of automorphism group size for undirected graph G.
 */
double Nautytoolkit::get_log_automorphisms(const PUNGraph &G) {
    LOG_F(INFO, "Calling nauty!");
    statsblk stats;
    static DEFAULTOPTIONS_GRAPH(options);

    int n = G->GetNodes(), word = SETWORDSNEEDED(n);
    nauty_check(WORDSIZE, word, n, NAUTYVERSIONID);

    std::vector<graph> g(word * n);
    std::vector<int> lab(n), ptn(n), orbits(n);
    EMPTYGRAPH(&g[0], word, n);
    for (auto edgeIt = G->BegEI(); edgeIt < G->EndEI(); edgeIt++) {
        ADDONEEDGE(&g[0], edgeIt.GetSrcNId(), edgeIt.GetDstNId(), word);
    }
    densenauty(&g[0], &lab[0], &ptn[0], &orbits[0], &options, &stats, word, n, NULL);
    auto result = log(stats.grpsize1) + log(10) * stats.grpsize2;
    LOG_F(INFO, "Size of the automorphism group: %lf", result);
    return result;
}

std::vector<graph> Nautytoolkit::get_canonical_label(const PUNGraph &G1) {
    statsblk stats;
    static DEFAULTOPTIONS_GRAPH(options);
    options.getcanon = true;

    int n = G1->GetNodes(), word = SETWORDSNEEDED(n);
    nauty_check(WORDSIZE, word, n, NAUTYVERSIONID);

    std::vector<graph> g1(word * n);
    std::vector<int> lab(n), ptn(n), orbits(n);
    EMPTYGRAPH(&g1[0], word, n);
    for (auto edgeIt = G1->BegEI(); edgeIt < G1->EndEI(); edgeIt++) {
        ADDONEEDGE(&g1[0], edgeIt.GetSrcNId(), edgeIt.GetDstNId(), word);
    }

    std::vector<graph> cg1(word * n);

    densenauty(&g1[0], &lab[0], &ptn[0], &orbits[0], &options, &stats, word, n, &cg1[0]);
    auto result = log(stats.grpsize1) + log(10) * stats.grpsize2;
    LOG_F(INFO, "Size of the automorphism group: %lf", result);

    return cg1;
}

bool Nautytoolkit::check_isomorphic_nauty(const PUNGraph &G1, PUNGraph &G2) {
    auto cg1 = get_canonical_label(G1);
    auto cg2 = get_canonical_label(G2);
    // todo (add assert on size and no of edges)


    int n = G1->GetNodes(), word = SETWORDSNEEDED(n);

    for (int k = 0; k < word * n; ++k) {
        if (cg1[k] != cg2[k]) return false;
    }
    return true;
}

int Nautytoolkit::get_structural_entropy(const PUNGraph &G) {
    int n = G->GetNodes();
    auto nchoose2 = double(n * (n - 1)) / double(2);

    auto p = (double) G->GetEdges() / nchoose2;
    assert(p <= 1.0);
    auto hp = -p * std::log2(p) - (1 - p) * std::log2(1.0 - p);
    auto HS = nchoose2 * hp - n * std::log2(n) + n * M_LOG2E - std::log2(n) / 2 - std::log2(M_PI) / 2;

    return HS;
}

int Nautytoolkit::get_graph_entropy(const PUNGraph &G) {
    int n = G->GetNodes();
    auto nchoose2 = double(n * (n - 1)) / double(2);
    auto p = (double) G->GetEdges() / nchoose2;
    auto hp = -p * std::log2(p) - (1 - p) * std::log2(1.0 - p);

    return nchoose2*hp;
}