//
// Created by pawel on 8/13/20.
//

#ifndef MAGISTERKA_GRAPH_BENCHMARKS_H
#define MAGISTERKA_GRAPH_BENCHMARKS_H

#include <vector>

namespace Graph::Benchmark {
    /*
     * Perform compression using Szip.
     * For each value in input vector `graphSizes` it generates ER graph and plots asymptoticality and compressed size
     */
    void modelGraphsBench1(std::vector<int> &graphSizes, bool nautyIso, int samples = 1);

    /*
    * Perform compression using Szip.
    * Constant n and run through p it generates ER graph and plots compressed size
    */
    void modelGraphsBench2(bool nautyIso, int samples = 1);


    /*
     * Perform compression using LMS.
     * For each value in input vector `graphSizes` it generates PA graph and generates asymptoticity plots
    */
    void modelGraphsBench3(std::vector<int> &graphSizes, bool nautyIso, int samples = 1);

    /*
     * Perform compression using LMS.
     * Constant n and run through several m parameters, it generates PA graph and generates asymptoticity plots
    */
    void modelGraphsBench4(bool nautyIso, int samples = 1);

    /*
     * Perform compression using all algorithms.
     * For each value in input vector `graphSizes` it generates ER graph and plots compressed size
     */
    void modelGraphsBench5(std::vector<int> &graphSizes, bool nautyIso, int samples,
                           const std::function<PUNGraph(int)> &generator, const std::string &testName);

    /*
     * Perform compression using all algorithms.
     * For each value in input vector `graphSizes` it generates PA graph and plots compressed size
     */
    void modelGraphsBench6(std::vector<int> &graphSizes, bool nautyIso);

    /*
     * Perform compression using all algorithms.
     * Generate graphs from DD paper using seed graphs in files/dd_seed_graphs
     */
    void modelGraphsBench7(bool nautyIso);

    /*
     * Perform compression using all algorithms.
     * Generate graphs from DD paper using seed graphs in files/dd_seed_graphs
     */
    void realWorldGraphs();

    void realWorldGraphsGiant();

    void testErrBars(std::vector<int> &sizes, int samples);


}

#endif //MAGISTERKA_GRAPH_BENCHMARKS_H
