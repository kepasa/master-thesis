//
// Created by pawel on 8/14/20.
//

#ifndef MAGISTERKA_NAUTY_TOOLKIT_H
#define MAGISTERKA_NAUTY_TOOLKIT_H

#include <cassert>
#include <vector>
#include <generators/generators.h>
#include "loguru.hpp"

extern "C" {
#include <nauty.h>
#include <nausparse.h>
#include <traces.h>
}

namespace Nautytoolkit {

    /*
     * Based on https://github.com/krzysztof-turowski/duplication-divergence/blob/master/src/dd_automorphisms.h
     * Returns log of automorphism group size for undirected graph G.
     */
    double get_log_automorphisms(const PUNGraph &G);

    std::vector<graph> get_canonical_label(const PUNGraph &G1);

    bool check_isomorphic_nauty(const PUNGraph &G1, PUNGraph &G2);


    int get_structural_entropy(const PUNGraph &G);

    int get_graph_entropy(const PUNGraph &G);
}

#endif //MAGISTERKA_NAUTY_TOOLKIT_H
