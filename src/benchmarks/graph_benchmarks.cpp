//
// Created by pawel on 8/13/20.
//

#include <generators/generators.h>
#include <compression/szip/szipe/szipe.h>
#include <io/graph_io.h>
#include <numeric>
#include <random>
#include "compression/lms/lms.h"
#include "graph_benchmarks.h"
#include "utils/typedefs.h"
#include "benchmarks/nauty_toolkit.h"
#include "algorithm"
#include "compression/arith_coder/graph_coder.h"
#include "compression/lz77/zstd_graph_coder.h"
#include "compression/lz78/lz78_graph_coder.h"

using Graph::Generators::GenerateER;
using Graph::Generators::GeneratePE;
using Graph::Generators::GenerateDD;
using ArithmeticCoding::GraphCoder;
using LZ77::ZSTDGraphCoder;
using LZ78::LZ78GraphCoder;
using Graph::Compression::SzipE;
using Graph::Compression::LMS;


PUNGraph getCachedOrGenerateER(int nodes, int edges) {
    std::string fname = "files/generated_graphs/ER_" + std::to_string(nodes) + "_" + std::to_string(edges);
    std::ifstream graphFile(fname);
    if (graphFile.good()) {
        LOG_F(INFO, "Reading graph from file %s", fname.c_str());
        return IO::read_graph(fname, true);
    }
    LOG_F(INFO, "Graph not found, generating");
    auto graph = GenerateER(nodes, edges);

    IO::write_graph(fname, graph);

    return graph;
}

PUNGraph getCachedOrGenerateDD(int nodes, double p, double r, const std::string &seedName, const PUNGraph &G0) {
    std::string fname = "files/generated_graphs/DD_" + std::to_string(nodes) + "_" + seedName;
    std::ifstream graphFile(fname);
    if (graphFile.good()) {
        LOG_F(INFO, "Reading graph from file %s", fname.c_str());
        return IO::read_graph(fname, true);
    }
    LOG_F(INFO, "Graph not found, generating");
    auto graph = GenerateDD(nodes, p, r, G0);

    IO::write_graph(fname, graph);

    return graph;
}

PNEGraph getCachedOrGeneratePA(int nodes, int m) {
    std::string fname = "files/generated_graphs/PA_" + std::to_string(nodes) + "_" + std::to_string(m);
    std::ifstream graphFile(fname);
    if (graphFile.good()) {
        LOG_F(INFO, "Reading graph from file %s", fname.c_str());
        return IO::read_dir_graph(fname);
    }
    LOG_F(INFO, "Graph not found, generating");
    auto graph = GeneratePE(nodes, m, true);

    IO::write_dir_graph(fname, graph);

    return graph;
}

void Graph::Benchmark::modelGraphsBench1(std::vector<int> &graphSizes, bool nautyIso, int samples) {
    loguru::add_file("benchmark1.log", loguru::Truncate, loguru::Verbosity_INFO);

    auto szipeStatic = SzipE(false, true);
    TVec<TPair<TFlt, TFlt> > StaticSzipE_PLT,
            GraphEntropy_PLT,
            LinearFactor_PLT,
            SzipMin_PLT, SzipMax_PLT,
            LinearFactorMin_PLT, LinearFactorMax_PLT;
    TVec<TPair<TFlt, TFlt> > N_PLT, NLGN_PLT;
    std::unordered_map<std::string, TVec<TPair<TFlt, TFlt> >> testNameToCompressionLengthPlot;
    std::unordered_map<std::string, TVec<TPair<TFlt, TFlt> >> testNameToLinearFactorPlot;

    for (int n : graphSizes) {
        double smallP = 0.01;
        double largerP = 0.1;

        std::vector<pds> ER_params{
                pds(smallP, "FromPaper"),
        };

        for (const auto &probWithName : ER_params) {

            auto sampleMinCompressed = std::numeric_limits<double>::max();
            auto sampleMaxCompressed = std::numeric_limits<double>::min();
            double totalCompressed = 0.0;
            auto sampleMinLinearFact = std::numeric_limits<double>::max();
            auto sampleMaxLinearFact = std::numeric_limits<double>::min();
            double totalLinearFact = 0.0;
            for (int sampleN = 0; sampleN < samples; ++sampleN) {

                LOG_F(INFO, "Generating graph %d sample %d / %d", n, sampleN, samples);
                int noEdges = int(double((ull) n * ((ull) n - 1)) / (double) 2) * probWithName.first;
                auto graph = samples > 1 ? GenerateER(n, noEdges) : getCachedOrGenerateER(n, noEdges);
                LOG_F(INFO, "Graph generated. Nodes %d edges: %d", n, noEdges);

                auto encodedStatic = szipeStatic.encode(graph);
                LOG_F(INFO, "Encoded szipe static");

                if (nautyIso) {
                    auto decodedStatic = szipeStatic.decode(encodedStatic);
                    LOG_F(INFO, "Decoded szipe static");
                    bool areIso2 =
                            graph->GetNodes() == decodedStatic->GetNodes() &&
                            graph->GetEdges() == decodedStatic->GetEdges();
                    areIso2 = areIso2 & Nautytoolkit::check_isomorphic_nauty(graph, decodedStatic);
                    LOG_IF_F(ERROR, !areIso2, "Graphs aren't isomorphic for: %s, p: %lf", probWithName.second.c_str(),
                             probWithName.first);
                }
                auto sizeStatic = 4 * 64 + encodedStatic.B1.size() + encodedStatic.B2.size();
                auto graphEntropy = Nautytoolkit::get_graph_entropy(graph);
                auto oComponent = ((double) sizeStatic - (double) graphEntropy + n * std::log2(n)) / (double) n;

                LOG_F(INFO,
                      "ER Graph %s %d - CompressedStatic: %ld - Gain: %lf - Entropy: %d - Graph entropy: %d",
                      probWithName.second.c_str(), n, sizeStatic, (graphEntropy - sizeStatic) + 0.0,
                      Nautytoolkit::get_structural_entropy(graph), Nautytoolkit::get_graph_entropy(graph));

                totalCompressed += (double(graphEntropy) - double(sizeStatic)) / samples;
                totalLinearFact += oComponent / samples;
                sampleMinCompressed = std::min(sampleMinCompressed, (double(graphEntropy) - double(sizeStatic)));
                sampleMaxCompressed = std::max(sampleMaxCompressed, (double(graphEntropy) - double(sizeStatic)));
                sampleMinLinearFact = std::min(sampleMinLinearFact, oComponent);
                sampleMaxLinearFact = std::max(sampleMaxLinearFact, oComponent);
            }

            StaticSzipE_PLT.Add({n + 0.0, totalCompressed});
            LinearFactor_PLT.Add({n + 0.0, totalLinearFact});
            SzipMin_PLT.Add({n + 0.0, sampleMinCompressed});
            SzipMax_PLT.Add({n + 0.0, sampleMaxCompressed});
            LinearFactorMin_PLT.Add({n + 0.0, sampleMinLinearFact});
            LinearFactorMax_PLT.Add({n + 0.0, sampleMaxLinearFact});
        }

        N_PLT.Add({n + 0.0, n});
        NLGN_PLT.Add({n + 0.0, n * std::log2(n)});


    }

    TGnuPlot Gp("files/output/PLOT_benchmark1_compression_size_samples", "Gain in compression over graph entropy");
    Gp.SetScale(gpsAuto);
    Gp.AddPlot(StaticSzipE_PLT, gpwPoints, "Szip gain");
    Gp.AddPlot(SzipMin_PLT, gpwLinesPoints, "Szip min");
    Gp.AddPlot(SzipMax_PLT, gpwLinesPoints, "Szip max");
    Gp.AddPlot(NLGN_PLT, gpwLinesPoints, "n log n");
    Gp.SetXYLabel("Number of vertices", "Gain over graph entropy");

    Gp.SaveEps();
    Gp.SavePng();

    TGnuPlot Gp2("files/output/PLOT_benchmark1_factor_samples", "Linear factor");
    Gp2.SetScale(gpsAuto);
    Gp2.AddPlot(LinearFactor_PLT, gpwPoints, "Linear factor");
    Gp2.AddPlot(LinearFactorMin_PLT, gpwLinesPoints, "Linear factor min");
    Gp2.AddPlot(LinearFactorMax_PLT, gpwLinesPoints, "Linear factor max");
    Gp2.SetXYLabel("Number of vertices", "Linear factor");
    Gp2.SaveEps();
    Gp2.SavePng();

    loguru::remove_callback("benchmark1.log");
}

void Graph::Benchmark::modelGraphsBench2(bool nautyIso, int samples) {
    loguru::add_file("benchmark2.log", loguru::Truncate, loguru::Verbosity_INFO);
    auto szipeStatic = SzipE(false, true);
    TVec<TPair<TFlt, TFlt> > N_PLT, NLGN_PLT;

    TVec<TPair<TFlt, TFlt> > StaticSzipE_PLT, GraphEntropy_PLT, SzipMin_PLT, SzipMax_PLT;
    double p = 0.01;

    while (p < 0.99) {
        int n = 3000;

        auto sampleMinCompressed = std::numeric_limits<double>::max();
        auto sampleMaxCompressed = std::numeric_limits<double>::min();
        double totalCompressed = 0.0;
        for (int sampleN = 0; sampleN < samples; ++sampleN) {
            LOG_F(INFO, "Generating graph %lf sample %d / %d", p, sampleN, samples);
            int noEdges = int(double((ull) n * ((ull) n - 1)) / (double) 2) * p;
            auto graph = samples > 1
                         ? GenerateER(n, noEdges)
                         : getCachedOrGenerateER(n, noEdges);
            LOG_F(INFO, "Graph generated. Nodes %d edges: %d", n, noEdges);

            auto encodedStatic = szipeStatic.encode(graph);
            LOG_F(INFO, "Encoded szipe static");

            if (nautyIso) {
                auto decodedStatic = szipeStatic.decode(encodedStatic);
                LOG_F(INFO, "Decoded szipe static");
                bool areIso2 =
                        graph->GetNodes() == decodedStatic->GetNodes() &&
                        graph->GetEdges() == decodedStatic->GetEdges();
                areIso2 = areIso2 & Nautytoolkit::check_isomorphic_nauty(graph, decodedStatic);
                LOG_IF_F(ERROR, !areIso2, "Graphs aren't isomorphic for: p: %lf",
                         p);
            }
            auto sizeStatic = 4 * 64 + encodedStatic.B1.size() + encodedStatic.B2.size();
            auto graphEntropy = Nautytoolkit::get_graph_entropy(graph);

            LOG_F(INFO,
                  "ER Graph %d - CompressedStatic: %ld - Gain: %lf - Entropy: %d - Graph entropy: %d",
                  n, sizeStatic, (graphEntropy - sizeStatic) + 0.0,
                  Nautytoolkit::get_structural_entropy(graph), Nautytoolkit::get_graph_entropy(graph));
            totalCompressed += (double(graphEntropy) - double(sizeStatic)) / samples;
            sampleMinCompressed = std::min(sampleMinCompressed, double(graphEntropy) - double(sizeStatic));
            sampleMaxCompressed = std::min(sampleMaxCompressed, double(graphEntropy) - double(sizeStatic));
        }
        StaticSzipE_PLT.Add({p, totalCompressed});
        SzipMin_PLT.Add({p, sampleMinCompressed});
        SzipMax_PLT.Add({p, sampleMaxCompressed});
        NLGN_PLT.Add({p, n * std::log2(n)});

        p += 0.05;
    }

    TGnuPlot Gp("files/output/PLOT_benchmark2_compression_size_n_const_samples",
                "Gain in compression over graph entropy");
    Gp.SetScale(gpsAuto);
    Gp.AddPlot(StaticSzipE_PLT, gpwPoints, "Szip gain");
    Gp.AddPlot(NLGN_PLT, gpwLines, "n log n");
    Gp.AddPlot(SzipMin_PLT, gpwLinesPoints, "Szip min");
    Gp.AddPlot(SzipMax_PLT, gpwLinesPoints, "Szip max");
    Gp.SetXYLabel("Edge probability", "Gain over graph entropy");

    Gp.SaveEps();
    Gp.SavePng();

    loguru::remove_callback("benchmark2.log");
}

void Graph::Benchmark::modelGraphsBench3(std::vector<int> &graphSizes, bool nautyIso, int samples) {
    loguru::add_file("benchmark3.log", loguru::Truncate, loguru::Verbosity_INFO);

    auto lms = LMS(-1);
    TVec<TPair<TFlt, TFlt> > LMS_PA_PLT,
            NLGLGN_PA_PLT,
            NLGN_PA_PLT,
            LGN_PA_PLT,
            BigOFactor_PLT,
            TreeDepth_PLT,
            LMSMin_PLT, LMSMax_PLT,
            BigOMin_PLT, BigOMax_PLT,
            TreeDepthMin_PLT, TreeDepthMax_PLT;

    for (int n : graphSizes) {
        std::vector<int> PA_param = {5};

        for (int &m : PA_param) {
            auto sampleMinCompressed = std::numeric_limits<double>::max();
            auto sampleMaxCompressed = std::numeric_limits<double>::min();
            double totalCompressed = 0.0;
            auto sampleMinBigO = std::numeric_limits<double>::max();
            auto sampleMaxBigO = std::numeric_limits<double>::min();
            double totalBigO = 0.0;
            auto sampleMinTDepth = std::numeric_limits<double>::max();
            auto sampleMaxTDepth = std::numeric_limits<double>::min();
            double totalTDepth = 0.0;
            for (int sampleN = 0; sampleN < samples; ++sampleN) {
                LOG_F(INFO, "Graph %d %d sample %d/%d", n, m, sampleN, samples);
                auto graph = samples > 1 ? GeneratePE(n, m, true) : getCachedOrGeneratePA(n, m);
                auto encodedLms = lms.encode(graph);
                auto decodedLms = lms.decode(encodedLms);

                auto sizeLms =
                        2 * 64 + encodedLms.graphCode.size() + encodedLms.isStarting.size() +
                        encodedLms.Bj.code.size() +
                        encodedLms.Bj.symbolToCodelen.size() * (encodedLms.Bj.codelenBits + encodedLms.Bj.symbolBits);

                bool areIso =
                        graph->GetNodes() == decodedLms->GetNodes() && graph->GetEdges() == decodedLms->GetEdges();

                if (nautyIso) {
                    auto convertedGraph = TSnap::ConvertGraph<PUNGraph>(graph);
                    auto convertedDecoded = TSnap::ConvertGraph<PUNGraph>(decodedLms);
                    areIso &= ::Nautytoolkit::check_isomorphic_nauty(convertedGraph, convertedDecoded);
                }

                LOG_IF_F(ERROR, !areIso, "PA Graphs aren't isomorphic for: %d %d", n, m);

                auto bigOSize = sizeLms - (m - 1) * n * (::Utils::log2i_ceil(n));
                auto bigOFactor = (double) bigOSize / (double) (n * std::log2(std::log2(n)));
                double TDepth = encodedLms.Bj.maxSymbol;
                LOG_F(INFO,
                      "PA Graph %d %d - Compressed: %ld - BigO: %ld - nlgn %lf - nlglgn %lf", n, m, sizeLms, bigOSize,
                      n * std::log2(n), n * std::log2(std::log2(n)));

                totalCompressed += double(bigOSize) / samples;
                sampleMinCompressed = std::min(sampleMinCompressed, (double) bigOSize);
                sampleMaxCompressed = std::max(sampleMaxCompressed, (double) bigOSize);
                totalBigO += bigOFactor / samples;
                sampleMinBigO = std::min(sampleMinBigO, bigOFactor);
                sampleMaxBigO = std::max(sampleMaxBigO, bigOFactor);
                totalTDepth += TDepth / samples;
                sampleMinTDepth = std::min(sampleMinTDepth, TDepth);
                sampleMaxTDepth = std::max(sampleMaxTDepth, TDepth);
            }
            LMS_PA_PLT.Add({n + 0.0, totalCompressed});
            LMSMin_PLT.Add({n + 0.0, sampleMinCompressed});
            LMSMax_PLT.Add({n + 0.0, sampleMaxCompressed});

            BigOFactor_PLT.Add({n + 0.0, totalBigO});
            BigOMin_PLT.Add({n + 0.0, sampleMinBigO});
            BigOMax_PLT.Add({n + 0.0, sampleMaxBigO});

            TreeDepth_PLT.Add({n + 0.0, totalTDepth});
            TreeDepthMin_PLT.Add({n + 0.0, sampleMinTDepth});
            TreeDepthMax_PLT.Add({n + 0.0, sampleMaxTDepth});
        }
        LGN_PA_PLT.Add({n + 0.0, std::log2(n)});
        NLGN_PA_PLT.Add({n + 0.0, n * std::log2(n)});
        NLGLGN_PA_PLT.Add({n + 0.0, n * std::log2(std::log2(n))});
    }

    TGnuPlot Gp("files/output/PLOT_benchmark3_compression_size_samples", "Bits in big O");
    Gp.AddPlot(LMS_PA_PLT, gpwLinesPoints, "Bits in big O");
    Gp.AddPlot(LMSMin_PLT, gpwLinesPoints, "Min BigO");
    Gp.AddPlot(LMSMax_PLT, gpwLinesPoints, "Max BigO");
    Gp.AddPlot(NLGLGN_PA_PLT, gpwLinesPoints, "n log log n");
    Gp.AddPlot(NLGN_PA_PLT, gpwLinesPoints, "n log n");

    Gp.SetXYLabel("Number of vertices", "Bits in big O");
    Gp.SaveEps();
    Gp.SavePng();

    TGnuPlot Gp2("files/output/PLOT_benchmark3_factor_samples", "Big O factor");
    Gp2.SetScale(gpsAuto);
    Gp2.AddPlot(BigOFactor_PLT, gpwPoints, "Big O factor");
    Gp2.AddPlot(BigOMin_PLT, gpwLinesPoints, "Big O min");
    Gp2.AddPlot(BigOMax_PLT, gpwLinesPoints, "Big O max");
    Gp2.SetXYLabel("Number of vertices", "Big O factor");
    Gp2.SaveEps();
    Gp2.SavePng();

    TGnuPlot Gp3("files/output/PLOT_benchmark3_TDepth_samples", "DAG depth");
    Gp3.SetScale(gpsAuto);
    Gp3.AddPlot(LGN_PA_PLT, gpwLinesPoints, "log n");
    Gp3.AddPlot(TreeDepth_PLT, gpwPoints, "DAG depth");
    Gp3.AddPlot(TreeDepthMin_PLT, gpwLinesPoints, "DAG depth min");
    Gp3.AddPlot(TreeDepthMax_PLT, gpwLinesPoints, "DAG depth max");
    Gp3.SetXYLabel("Number of vertices", "Tree depth");
    Gp3.SaveEps();
    Gp3.SavePng();

    loguru::remove_callback("benchmark3.log");
}

void Graph::Benchmark::modelGraphsBench4(bool nautyIso, int samples) {
    loguru::add_file("benchmark4.log", loguru::Truncate, loguru::Verbosity_INFO);

    auto lms = LMS(-1);
    TVec<TPair<TFlt, TFlt> > LMS_PA_PLT, NLGLGN_PA_PLT, NLGN_PA_PLT, BigOFactor_PLT,
            LMSMin_PLT, LMSMax_PLT;

    int n = 3000;
    std::vector<int> PA_param(30);
    std::iota(PA_param.begin(), PA_param.end(), 1);

    for (int &m : PA_param) {
        auto sampleMinCompressed = std::numeric_limits<double>::max();
        auto sampleMaxCompressed = std::numeric_limits<double>::min();
        double totalCompressed = 0.0;
        for (int sampleN = 0; sampleN < samples; ++sampleN) {
            LOG_F(INFO, "Graph %d %d sample %d/%d", n, m, sampleN, samples);
            auto graph = samples > 1 ? GeneratePE(n, m, true) : getCachedOrGeneratePA(n, m);
            auto encodedLms = lms.encode(graph);
            auto decodedLms = lms.decode(encodedLms);

            auto sizeLms =
                    2 * 64 + encodedLms.graphCode.size() + encodedLms.isStarting.size() + encodedLms.Bj.code.size() +
                    encodedLms.Bj.symbolToCodelen.size() * (encodedLms.Bj.codelenBits + encodedLms.Bj.symbolBits);

            bool areIso = graph->GetNodes() == decodedLms->GetNodes() && graph->GetEdges() == decodedLms->GetEdges();

            if (nautyIso) {
                auto convertedGraph = TSnap::ConvertGraph<PUNGraph>(graph);
                auto convertedDecoded = TSnap::ConvertGraph<PUNGraph>(decodedLms);
                areIso &= ::Nautytoolkit::check_isomorphic_nauty(convertedGraph, convertedDecoded);
            }
            LOG_IF_F(ERROR, !areIso, "PA Graphs aren't isomorphic for: %d %d", n, m);

            auto bigOSize = sizeLms - (m - 1) * n * (::Utils::log2i_ceil(n));
            auto bigOFactor = (double) bigOSize / (double) (n * std::log2(std::log2(n)));
            LOG_F(INFO,
                  "PA Graph %d %d - Compressed: %ld - BigO: %ld - nlgn %lf - nlglgn %lf", n, m, sizeLms, bigOSize,
                  n * std::log2(n), n * std::log2(std::log2(n)));
            totalCompressed += double(bigOSize) / samples;
            sampleMinCompressed = std::min(sampleMinCompressed, (double) bigOSize);
            sampleMaxCompressed = std::max(sampleMaxCompressed, (double) bigOSize);
        }
        LMS_PA_PLT.Add({m + 0.0, totalCompressed});
        LMSMin_PLT.Add({m + 0.0, sampleMinCompressed});
        LMSMax_PLT.Add({m + 0.0, sampleMaxCompressed});

        NLGN_PA_PLT.Add({m + 0.0, n * std::log2(n)});
        NLGLGN_PA_PLT.Add({m + 0.0, n * std::log2(std::log2(n))});
    }


    TGnuPlot Gp("files/output/PLOT_benchmark4_compression_size_samples", "Bits in big O");
    Gp.AddPlot(LMS_PA_PLT, gpwLinesPoints, "Bits in big O");
    Gp.AddPlot(LMSMin_PLT, gpwLinesPoints, "Min bits");
    Gp.AddPlot(LMSMax_PLT, gpwLinesPoints, "Max bits");
    Gp.AddPlot(NLGLGN_PA_PLT, gpwLinesPoints, "n log log n");
    Gp.AddPlot(NLGN_PA_PLT, gpwLinesPoints, "n log n");

    Gp.SetXYLabel("m parameter", "Bits in big O");
    Gp.SaveEps();
    Gp.SavePng();

    loguru::remove_callback("benchmark4.log");
}

void Graph::Benchmark::modelGraphsBench5(std::vector<int> &graphSizes, bool nautyIso, int samples,
                                         const std::function<PUNGraph(int)> &generator, const std::string &testName) {
    loguru::add_file("benchmark5.log", loguru::Truncate, loguru::Verbosity_INFO);

    auto lms = LMS(-1);
    auto szipeStatic = SzipE(false, false);
    auto acGraphCoder = GraphCoder();
    auto lz77GraphCoder = ZSTDGraphCoder();
    auto lz78GraphCoder = LZ78GraphCoder();
    TVec<TPair<TFlt, TFlt> > StaticSzipE_PLT, LMS_PLT, AC_PLT, LZ77_PLT, LZ78_PLT,
            SzipMin_PLT, SzipMax_PLT,
            LMSMin_PLT, LMSMax_PLT,
            ACMin_PLT, ACMax_PLT,
            LZ77Min_PLT, LZ77Max_PLT,
            LZ78Min_PLT, LZ78Max_PLT;

    TVec<TPair<TFlt, TFlt> > TimeStaticSzipE_PLT, TimeLMS_PLT, TimeAC_PLT, TimeLZ77_PLT, TimeLZ78_PLT,
            TimeSzipMin_PLT, TimeSzipMax_PLT,
            TimeLMSMin_PLT, TimeLMSMax_PLT,
            TimeACMin_PLT, TimeACMax_PLT,
            TimeLZ77Min_PLT, TimeLZ77Max_PLT,
            TimeLZ78Min_PLT, TimeLZ78Max_PLT;

    IO::LatexTable lt;
    lt.headers = {
            "# Nodes",
            "# Edges",
            "SzipE",
            "LMS",
            "Arithmetic coder",
            "ZSTD",
            "LZ78",
            "adj. mat.",
            "adj. list",
            "kmax",
            "max Bj"
    };

    for (int n : graphSizes) {
//        double smallP = 0.01;

        double szipAvg = 0.0;
        double szipMin = std::numeric_limits<double>::max();
        double szipMax = std::numeric_limits<double>::min();

        double timeSzipAvg = 0.0;
        double timeSzipMin = std::numeric_limits<double>::max();
        double timeSzipMax = std::numeric_limits<double>::min();

        double lmsAvg = 0.0;
        double lmsMin = std::numeric_limits<double>::max();
        double lmsMax = std::numeric_limits<double>::min();

        double timeLmsAvg = 0.0;
        double timeLmsMin = std::numeric_limits<double>::max();
        double timeLmsMax = std::numeric_limits<double>::min();

        double acAvg = 0.0;
        double acMin = std::numeric_limits<double>::max();
        double acMax = std::numeric_limits<double>::min();

        double timeAcAvg = 0.0;
        double timeAcMin = std::numeric_limits<double>::max();
        double timeAcMax = std::numeric_limits<double>::min();

        double lz77Avg = 0.0;
        double lz77Min = std::numeric_limits<double>::max();
        double lz77Max = std::numeric_limits<double>::min();

        double timelz77Avg = 0.0;
        double timelz77Min = std::numeric_limits<double>::max();
        double timelz77Max = std::numeric_limits<double>::min();

        double lz78Avg = 0.0;
        double lz78Min = std::numeric_limits<double>::max();
        double lz78Max = std::numeric_limits<double>::min();

        double timelz78Avg = 0.0;
        double timelz78Min = std::numeric_limits<double>::max();
        double timelz78Max = std::numeric_limits<double>::min();

        double edgesAvg = 0.0;

        for (int sampleN = 0; sampleN < samples; sampleN++) {
//            int noEdges = int((double((ull) n * ((ull) n - 1)) / (double) 2) * smallP);
            auto graph = generator(n);
            LOG_F(INFO, "Generated graph %d sample %d / %d n: %d, e: %d,  name: %s", n, sampleN, samples, n,
                  graph->GetEdges(), testName.c_str());
            edgesAvg += (double) graph->GetEdges() / samples;

            auto beginT = clock();
            auto encodedStatic = szipeStatic.encode(graph);
            auto endT = clock();
            auto timedSzip = double(endT - beginT) / CLOCKS_PER_SEC;
            timeSzipAvg += timedSzip / samples;
            timeSzipMin = std::min(timeSzipMin, timedSzip);
            timeSzipMax = std::max(timeSzipMax, timedSzip);
            LOG_F(INFO, "Encoded szipe static %lf", timedSzip);

            beginT = clock();
            auto encodedLms = lms.encode(::Utils::undirToDir(graph));
            endT = clock();
            auto timedLms = double(endT - beginT) / CLOCKS_PER_SEC;
            timeLmsAvg += timedLms / samples;
            timeLmsMin = std::min(timeLmsMin, timedLms);
            timeLmsMax = std::max(timeLmsMax, timedLms);
            LOG_F(INFO, "Encoded lms %lf", timedLms);

            beginT = clock();
            auto encodedAC = acGraphCoder.encode(graph);
            endT = clock();
            auto timedAc = double(endT - beginT) / CLOCKS_PER_SEC;
            timeAcAvg += timedAc / samples;
            timeAcMin = std::min(timeAcMin, timedAc);
            timeAcMax = std::max(timeAcMax, timedAc);
            LOG_F(INFO, "Encoded AC %lf", timedAc);

            beginT = clock();
            auto encodedLZ77 = lz77GraphCoder.encode(graph);
            endT = clock();
            auto timedlz77 = double(endT - beginT) / CLOCKS_PER_SEC;
            timelz77Avg += timedlz77 / samples;
            timelz77Min = std::min(timelz77Min, timedlz77);
            timelz77Max = std::max(timelz77Max, timedlz77);
            LOG_F(INFO, "Encoded LZ77 %lf", timedlz77);

            beginT = clock();
            auto encodedLZ78 = lz78GraphCoder.encode(graph);
            endT = clock();
            auto timedlz78 = double(endT - beginT) / CLOCKS_PER_SEC;
            timelz78Avg += timedlz78 / samples;
            timelz78Min = std::min(timelz78Min, timedlz78);
            timelz78Max = std::max(timelz78Max, timedlz78);
            LOG_F(INFO, "Encoded LZ78 %lf", timedlz78);

            if (nautyIso) {
                auto decodedStatic = szipeStatic.decode(encodedStatic);
                auto decodedLms = ::Utils::dirToUndir(lms.decode(encodedLms));

                bool areIso = ::Nautytoolkit::check_isomorphic_nauty(graph, decodedStatic);
                bool areIso2 = ::Nautytoolkit::check_isomorphic_nauty(graph, decodedLms);

                LOG_IF_F(ERROR, !areIso, "ER Graphs aren't isomorphic (Szip) for: %d %s", n, testName.c_str());
                LOG_IF_F(ERROR, !areIso2, "ER Graphs aren't isomorphic (LMS) for: %d %s", n, testName.c_str());
            }

            auto sizeStatic = 4 * 64 + encodedStatic.B1.size() + encodedStatic.B2.size();
            auto sizeLms =
                    2 * 64 + encodedLms.graphCode.size() + encodedLms.isStarting.size() + encodedLms.Bj.code.size() +
                    encodedLms.Bj.symbolToCodelen.size() * (encodedLms.Bj.codelenBits + encodedLms.Bj.symbolBits);
            auto sizeAC = encodedAC.code.size() + 2 * 64;
            auto sizeLZ77 = encodedLZ77.code.size() * 8 + 64;
            auto sizeLZ78 = encodedLZ78.code.size() * 8 + 64;

            szipAvg += (double) sizeStatic / samples;
            szipMin = std::min(szipMin, (double) sizeStatic);
            szipMax = std::max(szipMax, (double) sizeStatic);

            lmsAvg += (double) sizeLms / samples;
            lmsMin = std::min(lmsMin, (double) sizeLms);
            lmsMax = std::max(szipMax, (double) sizeLms);

            acAvg += (double) sizeAC / samples;
            acMin = std::min(acMin, (double) sizeAC);
            acMax = std::max(acMax, (double) sizeAC);

            lz77Avg += (double) sizeLZ77 / samples;
            lz77Min = std::min(lz77Min, (double) sizeLZ77);
            lz77Max = std::max(lz77Max, (double) sizeLZ77);

            lz78Avg += (double) sizeLZ78 / samples;
            lz78Min = std::min(lz78Min, (double) sizeLZ78);
            lz78Max = std::max(lz78Max, (double) sizeLZ78);
        }
        StaticSzipE_PLT.Add({n + 0.0, szipAvg});
        SzipMin_PLT.Add({n + 0.0, szipMin});
        SzipMax_PLT.Add({n + 0.0, szipMax});

        TimeStaticSzipE_PLT.Add({n + 0.0, timeSzipAvg});
        TimeSzipMin_PLT.Add({n + 0.0, timeSzipMin});
        TimeSzipMax_PLT.Add({n + 0.0, timeSzipMax});

        LMS_PLT.Add({n + 0.0, lmsAvg});
        LMSMin_PLT.Add({n + 0.0, lmsMin});
        LMSMax_PLT.Add({n + 0.0, lmsMax});

        TimeLMS_PLT.Add({n + 0.0, timeLmsAvg});
        TimeLMSMin_PLT.Add({n + 0.0, timeLmsMin});
        TimeLMSMax_PLT.Add({n + 0.0, timeLmsMax});

        AC_PLT.Add({n + 0.0, acAvg});
        ACMin_PLT.Add({n + 0.0, acMin});
        ACMax_PLT.Add({n + 0.0, acMax});

        TimeAC_PLT.Add({n + 0.0, timeAcAvg});
        TimeACMin_PLT.Add({n + 0.0, timeAcMin});
        TimeACMax_PLT.Add({n + 0.0, timeAcMax});

        LZ77_PLT.Add({n + 0.0, lz77Avg});
        LZ77Min_PLT.Add({n + 0.0, lz77Min});
        LZ77Max_PLT.Add({n + 0.0, lz77Max});

        TimeLZ77_PLT.Add({n + 0.0, timelz77Avg});
        TimeLZ77Min_PLT.Add({n + 0.0, timelz77Min});
        TimeLZ77Max_PLT.Add({n + 0.0, timelz77Max});

        LZ78_PLT.Add({n + 0.0, lz78Avg});
        LZ78Min_PLT.Add({n + 0.0, lz78Min});
        LZ78Max_PLT.Add({n + 0.0, lz78Max});

        TimeLZ78_PLT.Add({n + 0.0, timelz78Avg});
        TimeLZ78Min_PLT.Add({n + 0.0, timelz78Min});
        TimeLZ78Max_PLT.Add({n + 0.0, timelz78Max});

        std::vector<std::string> row =
                {
                        std::to_string(n),
                        testName,
                        std::to_string(szipAvg),
                        std::to_string(lmsAvg),
                        std::to_string(acAvg),
                        std::to_string(lz77Avg),
                        std::to_string(lz78Avg),
                        std::to_string(((double) (n * (n - 1)) / (double) 2)),
                        std::to_string((unsigned long long) ::Utils::log2i_ceil(n) *
                                       (unsigned long long) edgesAvg)
//                        std::to_string(encodedLms.k),
//                        std::to_string(encodedLms.Bj.maxSymbol)
                };
        lt.rows.push_back(row);
    }

    TGnuPlot Gp(("files/output/PLOT_All_Compressors_samples_" + testName).c_str(), "Compressed sizes");
    Gp.SetScale(gpsAuto);
    Gp.AddPlot(StaticSzipE_PLT, gpwLinesPoints, "SzipE");
    Gp.AddPlot(SzipMin_PLT, gpwLinesPoints, "Szip Min");
    Gp.AddPlot(SzipMax_PLT, gpwLinesPoints, "Szip Max");

    Gp.AddPlot(LMS_PLT, gpwLinesPoints, "LMS");
    Gp.AddPlot(LMSMin_PLT, gpwLinesPoints, "LMS Min");
    Gp.AddPlot(LMSMax_PLT, gpwLinesPoints, "LMS Max");

    Gp.AddPlot(AC_PLT, gpwLinesPoints, "Arithmetic coding");
    Gp.AddPlot(ACMin_PLT, gpwLinesPoints, "Arithmetic coding min");
    Gp.AddPlot(ACMax_PLT, gpwLinesPoints, "Arithmetic coding max");

    Gp.AddPlot(LZ77_PLT, gpwLinesPoints, "ZSTD");
    Gp.AddPlot(LZ77Min_PLT, gpwLinesPoints, "ZSTD min");
    Gp.AddPlot(LZ77Max_PLT, gpwLinesPoints, "ZSTD max");

    Gp.AddPlot(LZ78_PLT, gpwLinesPoints, "LZ 78");
    Gp.AddPlot(LZ78Min_PLT, gpwLinesPoints, "LZ 78");
    Gp.AddPlot(LZ78Max_PLT, gpwLinesPoints, "LZ 78");

    Gp.SetXYLabel("Number of nodes", "Compressed size");
    Gp.SaveEps();
    Gp.SavePng();

    TGnuPlot Gp2(("files/output/PLOT_All_Compressors_TIMES_samples_" + testName).c_str(), "Elapsed time");
    Gp2.SetScale(gpsAuto);
    Gp2.AddPlot(TimeStaticSzipE_PLT, gpwLinesPoints, "SzipE");
    Gp2.AddPlot(TimeSzipMin_PLT, gpwLinesPoints, "Szip Min");
    Gp2.AddPlot(TimeSzipMax_PLT, gpwLinesPoints, "Szip Max");

    Gp2.AddPlot(TimeLMS_PLT, gpwLinesPoints, "LMS");
    Gp2.AddPlot(TimeLMSMin_PLT, gpwLinesPoints, "LMS Min");
    Gp2.AddPlot(TimeLMSMax_PLT, gpwLinesPoints, "LMS Max");

    Gp2.AddPlot(TimeAC_PLT, gpwLinesPoints, "Arithmetic coding");
    Gp2.AddPlot(TimeACMin_PLT, gpwLinesPoints, "Arithmetic coding min");
    Gp2.AddPlot(TimeACMax_PLT, gpwLinesPoints, "Arithmetic coding max");

    Gp2.AddPlot(TimeLZ77_PLT, gpwLinesPoints, "ZSTD");
    Gp2.AddPlot(TimeLZ77Min_PLT, gpwLinesPoints, "ZSTD min");
    Gp2.AddPlot(TimeLZ77Max_PLT, gpwLinesPoints, "ZSTD max");

    Gp2.AddPlot(TimeLZ78_PLT, gpwLinesPoints, "LZ 78");
    Gp2.AddPlot(TimeLZ78Min_PLT, gpwLinesPoints, "LZ 78");
    Gp2.AddPlot(TimeLZ78Max_PLT, gpwLinesPoints, "LZ 78");

    Gp2.SetXYLabel("Number of nodes", "Compressed size");
    Gp2.SaveEps();
    Gp2.SavePng();

    IO::write_latex_table("files/output/All_Compressors_samples_" + testName + ".txt", lt);

    loguru::remove_callback("benchmark5.log");
}


void Graph::Benchmark::testErrBars(std::vector<int> &sizes, int samples) {
    TVec<TPair<TFlt, TFlt> > NLGN_PLT;
    std::random_device rd;
    TVec<TPair<TFlt, TFlt>> Y_HIGH;
    TVec<TPair<TFlt, TFlt>> Y_LOW;

    for (int n : sizes) {
        auto sampleMin = std::numeric_limits<double>::max();
        auto sampleMax = std::numeric_limits<double>::min();
        double ttl = 0.0;
        for (int i = 0; i < samples; ++i) {
            std::uniform_real_distribution<double> dist(-(n / 4.0), n / 4.0);
            auto sampledValue = n + dist(rd);
            sampleMin = std::min(sampleMin, sampledValue);
            sampleMax = std::max(sampleMax, sampledValue);
            ttl += sampledValue / samples;
        }
        NLGN_PLT.Add({n + 0.0, ttl});
        Y_HIGH.Add({n + 0.0, sampleMax});
        Y_LOW.Add({n + 0.0, sampleMin});
    }

    TGnuPlot Gp("files/output/testErrBars", "ErrBars test");
    Gp.SetScale(gpsAuto);
    Gp.AddPlot(NLGN_PLT, gpwLinesPoints, "n lg n");
    Gp.AddPlot(Y_HIGH, gpwDots, "high");
    Gp.AddPlot(Y_LOW, gpwDots, "low");
    Gp.SavePng();

}

void Graph::Benchmark::modelGraphsBench6(std::vector<int> &graphSizes, bool nautyIso) {
    loguru::add_file("benchmark6.log", loguru::Truncate, loguru::Verbosity_INFO);

    auto lms = LMS(-1);
    auto szipeStatic = SzipE(false, true);
    auto szipe = SzipE(false, false);
    auto acGraphCoder = GraphCoder();
    auto lz77GraphCoder = ZSTDGraphCoder();
    auto lz78GraphCoder = LZ78GraphCoder();
    TVec<TPair<TFlt, TFlt> > SzipE_PLT, StaticSzipE_PLT, LMS_PLT, AC_PLT, LZ77_PLT, LZ78_PLT;

    IO::LatexTable lt;
    lt.headers = {
            "# Nodes",
            "# Edges",
            "SzipEAdaptive",
            "SzipE",
            "LMS",
            "Arithmetic coder",
            "ZSTD",
            "LZ78",
            "adj. mat.",
            "adj. list",
            "kmax",
            "max Bj"
    };

    for (int n : graphSizes) {
        int m = 3;
        auto graph = TSnap::ConvertGraph<PUNGraph>(GeneratePE(n, m, true));
        LOG_F(INFO, "Generated graph %d", n);

        auto encodedSzipe = szipe.encode(graph);
        LOG_F(INFO, "Encoded szipe adaptive");
        auto encodedStatic = szipeStatic.encode(graph);
        LOG_F(INFO, "Encoded szipe static");
        auto encodedLms = lms.encode(::Utils::undirToDir(graph));
        LOG_F(INFO, "Encoded lms");
        auto encodedAC = acGraphCoder.encode(graph);
        LOG_F(INFO, "Encoded AC");
        auto encodedLZ77 = lz77GraphCoder.encode(graph);
        LOG_F(INFO, "Encoded LZ77");
        auto encodedLZ78 = lz78GraphCoder.encode(graph);
        LOG_F(INFO, "Encoded LZ78");

        bool isIsoZstd = ::Nautytoolkit::check_isomorphic_nauty(lz77GraphCoder.decode(encodedLZ77), graph);
        VLOG_IF_F(0, isIsoZstd == 1, "GRAPHS ARE ISOMORPHIC");
        VLOG_IF_F(-2, isIsoZstd == 0, "GRAPHS ARE NOT ISOMORPHIC!");

        if (nautyIso) {
            auto decodedStatic = szipeStatic.decode(encodedStatic);
            auto decodedLms = ::Utils::dirToUndir(lms.decode(encodedLms));

            bool areIso = ::Nautytoolkit::check_isomorphic_nauty(graph, decodedStatic);
            bool areIso2 = ::Nautytoolkit::check_isomorphic_nauty(graph, decodedLms);

            LOG_IF_F(ERROR, !areIso, "PA Graphs aren't isomorphic (Szip) for: %d %d", n, m);
            LOG_IF_F(ERROR, !areIso2, "PA Graphs aren't isomorphic (LMS) for: %d %d", n, m);
        }

        auto sizeAdaptive = 4 * 64 + encodedSzipe.B1.size() + encodedSzipe.B2.size();
        auto sizeStatic = 4 * 64 + encodedStatic.B1.size() + encodedStatic.B2.size();
        auto sizeLms =
                2 * 64 + encodedLms.graphCode.size() + encodedLms.isStarting.size() + encodedLms.Bj.code.size() +
                encodedLms.Bj.symbolToCodelen.size() * (encodedLms.Bj.codelenBits + encodedLms.Bj.symbolBits);
        auto sizeAC = encodedAC.code.size() + 2 * 64;
        auto sizeLZ77 = encodedLZ77.code.size() * 8 + 64;
        auto sizeLZ78 = encodedLZ78.code.size() * 8 + 64;

        StaticSzipE_PLT.Add({n + 0.0, sizeStatic + 0.0});
        SzipE_PLT.Add({n + 0.0, sizeAdaptive + 0.0});
        LMS_PLT.Add({n + 0.0, sizeLms + 0.0});
        AC_PLT.Add({n + 0.0, sizeAC + 0.0});
        LZ77_PLT.Add({n + 0.0, sizeLZ77 + 0.0});
        LZ78_PLT.Add({n + 0.0, sizeLZ78 + 0.0});

        std::vector<std::string> row =
                {
                        std::to_string(graph->GetNodes()),
                        std::to_string(graph->GetEdges()),
                        std::to_string(sizeAdaptive),
                        std::to_string(sizeStatic),
                        std::to_string(sizeLms),
                        std::to_string(sizeAC),
                        std::to_string(sizeLZ77),
                        std::to_string(sizeLZ78),
                        std::to_string(((double) (n * (n - 1)) / (double) 2)),
                        std::to_string((unsigned long long) ::Utils::log2i_ceil(graph->GetNodes()) *
                                       (unsigned long long) graph->GetEdges()),
                        std::to_string(encodedLms.k),
                        std::to_string(encodedLms.Bj.maxSymbol)
                };
        lt.rows.push_back(row);
    }

    TGnuPlot Gp("files/output/PLOT_PA_All_Compressors", "Compressed sizes");
    Gp.SetScale(gpsAuto);
    Gp.AddPlot(StaticSzipE_PLT, gpwLinesPoints, "SzipE (Static)");
    Gp.AddPlot(SzipE_PLT, gpwLinesPoints, "SzipE (Adaptive)");
    Gp.AddPlot(LMS_PLT, gpwLinesPoints, "LMS");
    Gp.AddPlot(AC_PLT, gpwLinesPoints, "Arithmetic coding");
    Gp.AddPlot(LZ77_PLT, gpwLinesPoints, "ZSTD");
    Gp.AddPlot(LZ78_PLT, gpwLinesPoints, "LZ 78");

    Gp.SetXYLabel("Number of nodes", "Compressed size");
    Gp.SaveEps();
    Gp.SavePng();
    IO::write_latex_table("files/output/PA_All_Compressors.txt", lt);

    loguru::remove_callback("benchmark6.log");
}

void Graph::Benchmark::modelGraphsBench7(bool nautyIso) {
    loguru::add_file("benchmark7.log", loguru::Truncate, loguru::Verbosity_INFO);

    std::vector<std::tuple<std::string, std::string, int, double, double>> testParams = {
            {"Baker's yeast",   "G0-bakers-yeast",    6152,  0.98,  0.35},
            {"Human",           "G0-human",           17295, 0.64,  0.49},
            {"Fruitfly",        "G0-fruitfly",        9205,  0.53,  0.92},
            {"Fission yeast",   "G0-fission-yeast",   4177,  0.983, 0.85},
            {"Mouse-ear cress", "G0-mouse-ear-cress", 9388,  0.98,  0.49},
            {"Mouse",           "G0-mouse",           6849,  0.96,  0.32},
            {"Roundworm",       "G0-roundworm",       3869,  0.85,  0.35}
    };
    auto lms = LMS(-1);
    auto szipeStatic = SzipE(false, true);
    auto szipe = SzipE(false, false);

    auto acGraphCoder = GraphCoder();
    auto lz77GraphCoder = ZSTDGraphCoder();
    auto lz78GraphCoder = LZ78GraphCoder();

    IO::LatexTable lt;
    lt.headers = {
            "Modeled organism",
            "# Nodes",
            "# Edges",
            "Automorphism group size",
            "SzipE static",
            "SzipE",
            "LMS",
            "Arithmetic coder",
            "ZSTD",
            "LZ78",
            "adj. mat.",
            "adj. list",
            "kmax",
            "max Bj"
    };

    for (auto &parameters : testParams) {
        auto seedGraph = IO::read_graph("files/dd_seed_graphs/" + std::get<1>(parameters) + ".txt");
        int n = std::get<2>(parameters);
        double p = std::get<3>(parameters);
        double r = std::get<4>(parameters);

        auto graph = getCachedOrGenerateDD(n, p, r, std::get<1>(parameters), seedGraph);
        LOG_F(INFO, "Generated graph %d %d", n, graph->GetEdges());

        auto encodedSzipe = szipe.encode(graph);
        LOG_F(INFO, "Szipe static");
        auto encodedStatic = szipeStatic.encode(graph);
        LOG_F(INFO, "Encoded szipe static");
        auto encodedLms = lms.encode(::Utils::undirToDir(graph));
        LOG_F(INFO, "Encoded lms");
        auto encodedAC = acGraphCoder.encode(graph);
        LOG_F(INFO, "Encoded AC");
        auto encodedLZ77 = lz77GraphCoder.encode(graph);
        LOG_F(INFO, "Encoded LZ77");
        auto encodedLZ78 = lz78GraphCoder.encode(graph);
        LOG_F(INFO, "Encoded LZ78");

        if (nautyIso) {
            auto decodedStatic = szipeStatic.decode(encodedStatic);
            auto decodedLms = ::Utils::dirToUndir(lms.decode(encodedLms));

            bool areIso = ::Nautytoolkit::check_isomorphic_nauty(graph, decodedStatic);
            bool areIso2 = ::Nautytoolkit::check_isomorphic_nauty(graph, decodedLms);

            LOG_IF_F(ERROR, !areIso, "DD Graphs aren't isomorphic (Szip) for: %s", std::get<1>(parameters).c_str());
            LOG_IF_F(ERROR, !areIso2, "DD Graphs aren't isomorphic (LMS) for: %s", std::get<1>(parameters).c_str());
        }

        auto sizeSzipe = 4 * 64 + encodedSzipe.B1.size() + encodedSzipe.B2.size();
        auto sizeStatic = 4 * 64 + encodedStatic.B1.size() + encodedStatic.B2.size();
        auto sizeLms =
                2 * 64 + encodedLms.graphCode.size() + encodedLms.isStarting.size() + encodedLms.Bj.code.size() +
                encodedLms.Bj.symbolToCodelen.size() * (encodedLms.Bj.codelenBits + encodedLms.Bj.symbolBits);
        auto sizeAC = encodedAC.code.size() + 2 * 64;
        auto sizeLZ77 = encodedLZ77.code.size() * 8 + 64;
        auto sizeLZ78 = encodedLZ78.code.size() * 8 + 64;

        std::vector<std::string> row =
                {
                        std::get<0>(parameters),
                        std::to_string(graph->GetNodes()),
                        std::to_string(graph->GetEdges()),
                        std::to_string(::Nautytoolkit::get_log_automorphisms(graph)),
                        std::to_string(sizeStatic),
                        std::to_string(sizeSzipe),
                        std::to_string(sizeLms),
                        std::to_string(sizeAC),
                        std::to_string(sizeLZ77),
                        std::to_string(sizeLZ78),
                        std::to_string(((double) (n * (n - 1)) / (double) 2)),
                        std::to_string((unsigned long long) ::Utils::log2i_ceil(graph->GetNodes()) *
                                       (unsigned long long) graph->GetEdges()),
                        std::to_string(encodedLms.k),
                        std::to_string(encodedLms.Bj.maxSymbol)
                };

        lt.rows.push_back(row);
    }

    IO::write_latex_table("files/output/dd_benchmark_all.txt", lt);
    loguru::remove_callback("benchmark7.log");
}

void Graph::Benchmark::realWorldGraphsGiant() {
    loguru::add_file("benchmarkRealWorldGiant.log", loguru::Truncate, loguru::Verbosity_INFO);

    std::vector<std::tuple<std::string, std::string>> testParams = {
            {"Internet topology graph",              "as-skitter.txt"},
            {"Amazon product co-purchasing network", "com-amazon.ungraph.txt"},
            {"Youtube social network",               "com-youtube.ungraph.txt"},
            {"Road network of California",           "roadNet-CA.txt"}
    };

    auto lms = LMS(-1);
    IO::LatexTable lt;
    lt.headers = {
            "Network",
            "# Nodes",
            "# Edges",
            "LMS",
            "adj. mat.",
            "adj. list",
            "kmax",
            "max Bj"
    };

    for (auto &parameters : testParams) {
        auto graph = IO::read_graph("files/real_world_graphs/" + std::get<1>(parameters), true);
        LOG_F(INFO, "Read graph %d %d", graph->GetNodes(), graph->GetEdges());
        int n = graph->GetNodes();

        auto encodedLms = lms.encode(::Utils::undirToDir(graph));
        LOG_F(INFO, "Encoded lms");

        auto sizeLms =
                2 * 64 + encodedLms.graphCode.size() + encodedLms.isStarting.size() + encodedLms.Bj.code.size() +
                encodedLms.Bj.symbolToCodelen.size() * (encodedLms.Bj.codelenBits + encodedLms.Bj.symbolBits);

        std::vector<std::string> row =
                {
                        std::get<0>(parameters),
                        std::to_string(graph->GetNodes()),
                        std::to_string(graph->GetEdges()),
                        std::to_string(sizeLms),
                        std::to_string(((double) (n * (n - 1)) / (double) 2)),
                        std::to_string((unsigned long long) ::Utils::log2i_ceil(graph->GetNodes()) *
                                       (unsigned long long) graph->GetEdges()),
                        std::to_string(encodedLms.k),
                        std::to_string(encodedLms.Bj.maxSymbol)
                };
        lt.rows.push_back(row);
    }
    IO::write_latex_table("files/output/real_world_benchmark_giant_all.txt", lt);
    loguru::remove_callback("benchmarkRealWorldGiant.log");
}

void Graph::Benchmark::realWorldGraphs() {


    loguru::add_file("benchmarkRealWorldSmall.log", loguru::Truncate, loguru::Verbosity_INFO);

    std::vector<std::tuple<std::string, std::string, bool>> testParams = {
            {"Collaboration network",                   "ca-AstroPh.txt",           false},
            {"Social circles from Facebook",            "facebook_combined.txt",    false},
            {"Twitch social network DE users",          "musae_DE_edges.csv",       true},
            {"Twitch social network PTBT users",        "musae_PTBR_edges.csv",     true},
            {"GitHub social network",                   "musae_git_edges.csv",      true},
            {"Internet routers 1",                      "as20000102.txt",           false},
            {"Internet routers 2",                      "oregon2_010526.txt",       false},
            {"Network of Wikipedia pages on squirrels", "musae_squirrel_edges.csv", true}
    };

    auto lms = LMS(-1);
//    auto szipeStatic = SzipE(false, true);
    auto szipe = SzipE(false, false);
    auto acGraphCoder = GraphCoder();
    auto lz77GraphCoder = ZSTDGraphCoder();
    auto lz78GraphCoder = LZ78GraphCoder();
    IO::LatexTable lt;
    lt.headers = {
            "Network",
            "# Nodes",
            "# Edges",
//            "log Aut(G)",
            "SzipE",
            "LMS",
            "Arithmetic coder",
            "ZSTD",
            "LZ78",
            "adj. mat.",
            "adj. list",
            "kmax",
            "max Bj"
    };

    for (auto &parameters : testParams) {
        PUNGraph graph;
        if (std::get<2>(parameters)) {
            graph = IO::read_csv_graph("files/real_world_graphs/" + std::get<1>(parameters));
        } else {
            graph = IO::read_graph("files/real_world_graphs/" + std::get<1>(parameters), true);
        }
        LOG_F(INFO, "Read graph %d %d", graph->GetNodes(), graph->GetEdges());
        int n = graph->GetNodes();

        auto encodedSzipe = szipe.encode(graph);
        LOG_F(INFO, "Szipe static");
        auto encodedLms = lms.encode(::Utils::undirToDir(graph));
        LOG_F(INFO, "Encoded lms");
        auto encodedAC = acGraphCoder.encode(graph);
        LOG_F(INFO, "Encoded AC");
        auto encodedLZ77 = lz77GraphCoder.encode(graph);
        LOG_F(INFO, "Encoded LZ77");
        auto encodedLZ78 = lz78GraphCoder.encode(graph);
        LOG_F(INFO, "Encoded LZ78");

        auto sizeSzipe = 4 * 64 + encodedSzipe.B1.size() + encodedSzipe.B2.size();
        auto sizeLms =
                2 * 64 + encodedLms.graphCode.size() + encodedLms.isStarting.size() + encodedLms.Bj.code.size() +
                encodedLms.Bj.symbolToCodelen.size() * (encodedLms.Bj.codelenBits + encodedLms.Bj.symbolBits);
        auto sizeAC = encodedAC.code.size() + 2 * 64;
        auto sizeLZ77 = encodedLZ77.code.size() * 8 + 64;
        auto sizeLZ78 = encodedLZ78.code.size() * 8 + 64;

        std::vector<std::string> row =
                {
                        std::get<0>(parameters),
                        std::to_string(graph->GetNodes()),
                        std::to_string(graph->GetEdges()),
//                        std::to_string(::Nautytoolkit::get_log_automorphisms(graph)),
                        std::to_string(sizeSzipe),
                        std::to_string(sizeLms),
                        std::to_string(sizeAC),
                        std::to_string(sizeLZ77),
                        std::to_string(sizeLZ78),
                        std::to_string(((double) (n * (n - 1)) / (double) 2)),
                        std::to_string((unsigned long long) ::Utils::log2i_ceil(graph->GetNodes()) *
                                       (unsigned long long) graph->GetEdges()),
                        std::to_string(encodedLms.k),
                        std::to_string(encodedLms.Bj.maxSymbol)
                };
        lt.rows.push_back(row);
    }
    IO::write_latex_table("files/output/real_world_benchmark_small_all.txt", lt);
    loguru::remove_callback("benchmarkRealWorldSmall.log");
}
