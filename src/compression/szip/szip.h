//
// Created by pawel on 8/5/20.
//

#ifndef MAGISTERKA_SZIP_H
#define MAGISTERKA_SZIP_H

#include <Snap.h>
#include <loguru.hpp>
#include <compression/szip/szipn/orderedpartition.h>
#include "utils/binaryops.h"
#include <compression/arith_coder/static_integer_coder.h>
#include "compression/compressor.h"

namespace Graph::Compression {
    using ArithmeticCoding::StaticIntegerCoder;
    // Base clas for SzipN and SzipE
    template<typename B2_TYPE>
    class Szip : public Compressor<const PUNGraph, CompressedGraph> {
    protected:
        static std::shared_ptr<Utils::PartitionSubset> createInitialPartition(int graphSize) {
            auto vecRange = std::vector<int>(graphSize);
            std::iota(vecRange.begin(), vecRange.end(), 0);

            return std::make_shared<Utils::PartitionSubset>(vecRange);
        }

        virtual std::vector<B2_TYPE> decodeB2(CompressedGraph &encodedGraph) = 0;
        virtual void initializeDecode(CompressedGraph &encodedGraph) = 0;
        virtual int getNeighbourCount(std::vector<B2_TYPE>& B2) = 0;
    public:
        virtual CompressedGraph encode(const PUNGraph &toEncode) = 0;

        const PUNGraph decode(CompressedGraph &encodedGraph) override {
            LOG_SCOPE_FUNCTION(1);
            initializeDecode(encodedGraph);
            auto currentParition = new Graph::Utils::OrderedPartition();
            currentParition->addSubset(createInitialPartition(encodedGraph.n));
            PUNGraph graph = PUNGraph::New();
            for(int i=0; i < encodedGraph.n; ++i) {
                graph->AddNode(i);
            }

            std::unordered_map<char, int> b1Freqs;
            b1Freqs[0] = encodedGraph.zerosB1;
            b1Freqs[1] = encodedGraph.onesB1;
            auto B1_coder = StaticIntegerCoder<char>(b1Freqs, 1);

            auto B1 = B1_coder.decode(encodedGraph.B1);
            auto B2 = decodeB2(encodedGraph);

            int currentB1Idx = 0;
            while(!currentParition->isEmpty()) {
                auto nextPartition = new Graph::Utils::OrderedPartition();
                int currentNode = currentParition->getSubset(0)->extractFirstElement();

                for (const auto& subset : currentParition->items()) {
                    if(subset->getSubsetSize() == 0) continue;
                    int subsetSize = subset->getSubsetSize();
                    int neighboursCount;
                    if(subsetSize == 1) {
                        neighboursCount = getNeighbourCount(B2);
                    } else {
                        int bitsCnt = ::Utils::log2i_ceil(subset->getSubsetSize() + 1);
                        neighboursCount = ::Utils::decodeBits(
                                B1.begin()+currentB1Idx,
                                B1.begin()+currentB1Idx+bitsCnt
                        );
                        currentB1Idx += bitsCnt;
                    }
//                    VLOG_F(1, "Extracted %d bits", neighboursCount);
                    auto it = subset->items().begin();
                    for(int i = 0; i < neighboursCount; ++i) {
                        int neighbor = *it;
                        std::advance(it, 1);
                        graph->AddEdge2(currentNode, neighbor);
                        graph->AddEdge2(neighbor, currentNode);
                    }
                    auto splitSubset = subset->splitSubset(currentNode, graph);
                    if(!splitSubset.first->isEmpty()) {
                        nextPartition->addSubset(splitSubset.first);
                    }
                    if(!splitSubset.second->isEmpty()) {
                        nextPartition->addSubset(splitSubset.second);
                    }
                }
                std::swap(nextPartition, currentParition);
                delete nextPartition;
            }
            delete currentParition;
            return graph;
        }
    };
}

#endif //MAGISTERKA_SZIP_H
