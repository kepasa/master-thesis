//
// Created by pawel on 5/20/20.
//

#ifndef MAGISTERKA_SZIPE_H
#define MAGISTERKA_SZIPE_H

#include <Snap.h>
#include <memory>
#include <numeric>
#include <utils/binaryops.h>
#include <iostream>
#include <cfenv>
#include <cmath>
#include <utils/vector.h>
#include "utils/typedefs.h"
#include "compression/szip/szip.h"
#include <compression/szip/szipn/szipn.h>
#include <sstream>
#include <iterator>
#include "compression/arith_coder/adaptive_integer_coder.h"
#include "compression/arith_coder/static_integer_coder.h"
#include "loguru.hpp"

namespace Graph::Compression {
    using ArithmeticCoding::AdaptiveIntegerCoder;
    using ArithmeticCoding::StaticIntegerCoder;

    struct PartitionArrays {
        std::vector<int> pos;
        std::vector<int> rank;
        std::vector<int> vertex;
        std::vector<int> partSize;
        std::vector<int> next;
    };

    class SzipE : public Szip<int> {
    protected:
        std::unordered_map<int, int> precomputeZeroRunFreqs(double p) {
            auto multipl = (1 << 24);
            double totalProb = 0.0;
            long long totalCnt = 0;
            std::unordered_map<int, int> freqs;

//            auto kmaxSeries = p == 0.0 ? 1.0 : 1.0/(1 - std::pow(1.0 - p, kmax));
//            auto kmaxPow = std::pow(1.0 - p, kmax);
            auto kmaxSeries = 1.0;
            freqs[0] = std::max(1, (int) ((p * kmaxSeries) * multipl));
            totalProb += p * kmaxSeries;

            for (int i = 1; i < kmax; ++i) {
                double currentProb = std::pow(1.0 - p, i) * p * kmaxSeries;
                freqs[i] = currentProb * multipl;
                totalCnt += freqs[i];
                totalProb += currentProb;
            }

            freqs[kmax] = std::max(1, int((1.0 - totalProb) * multipl));

//            for (int i = 0; i <= kmax; ++i) {
//                LOG_F(INFO, "PRECOMPUTED FREQ OF: %d IS: %lf", i, ((double) freqs[i] / (double) multipl));
//            }

            return freqs;
        }

        std::vector<int> decodeB2(CompressedGraph &encodedGraph) override {
            std::unique_ptr<Compressor<std::vector<int>, std::vector<char>>> B2_coder;
            auto p = encodedGraph.p;

            if (encodedGraph.B2.empty()) {
                return std::vector<int>();
            }

            if (useStaticArithCoder) {
                B2_coder = std::make_unique<StaticIntegerCoder<int>>(precomputeZeroRunFreqs(p),
                                                                     kmax);
            } else {
                B2_coder = std::make_unique<AdaptiveIntegerCoder<int>>(
                        kmax); // TODO(pawelp): the probability distiburion of codewords is know, change this possibly.
            }
            auto decodedB2 = B2_coder->decode(encodedGraph.B2);

            std::vector<int> B2;

            bool processingKmax = false;
            int currentCount = 0;
            // stitch together all kmax
            for (int i : decodedB2) {
                if (processingKmax) {
                    currentCount += i;
                    if (i < kmax) {
                        B2.push_back(currentCount);
                        currentCount = 0;
                        processingKmax = false;
                    }
                } else if (i == kmax) {
                    processingKmax = true;
                    currentCount = kmax;
                } else if (i < kmax) {
                    B2.push_back(i);
                }
            }

            return B2;
        }

        void initializeDecode(CompressedGraph &encodedGraph) override {
            int n = encodedGraph.n;
            double p = encodedGraph.p;

            if (p == 0.0) this->kmax = 1;
            else this->kmax = std::ceil(1.0 / p);
            B2idx = 0;
        }

        int getNeighbourCount(std::vector<int> &B2) override {
            if (B2[B2idx] == 0) {
                ++B2idx;
                return 1;
            } else {
                B2[B2idx]--;
                return 0;
            }
        }

    private:
        PartitionArrays P;
        bool reconstructB2;
        bool useStaticArithCoder;
        const bool printDbg = false;
        int kmax;
        double prob; // Probability set in encodeB2
        int B2idx;

        void initPartition(const PUNGraph &graph) {
            int n = graph->GetNodes();

            P.pos.resize(n);
            std::iota(std::rbegin(P.pos), std::rend(P.pos), 0);

            P.rank.resize(n);
            std::fill(P.rank.begin(), P.rank.end(), 0);

            P.vertex.resize(n);
            std::iota(std::rbegin(P.vertex), std::rend(P.vertex), 0);

            P.partSize.resize(n);
            std::fill(P.partSize.begin(), P.partSize.end(), 0);
            P.partSize[0] = n;

            P.next.resize(n);
            std::fill(P.next.begin(), P.next.end(), -1);
        }

        int getCurrentVertex(int &head, int n, int i) {
            // select vertex with highest position
            int toReturn = P.vertex[n - i];
            int pos = P.pos[toReturn];
            int r = P.rank[toReturn];
            P.partSize[r] -= 1;

            // mark as processed
            P.pos[toReturn] = -1;
            P.rank[toReturn] = -1;
            P.vertex[pos] = -1;
            if (head >= 0 && head < n && P.partSize[head] <= 1) {
                int tmpHead = head;
                head = P.next[head];
                P.next[tmpHead] = -1;
            }
            return toReturn;
        }

        // step 2
        void processNeighbours(int currentV,
                               const PUNGraph &graph,
                               std::vector<int> &count,
                               std::vector<std::vector<int>> &B,
                               int i) {
            auto nodeI = graph->GetNI(currentV);
            int v_deg = nodeI.GetDeg();
            for (int nId = 0; nId < v_deg; ++nId) {
                int neighbor = nodeI.GetNbrNId(nId);
                if (P.pos[neighbor] == -1) continue; // we already processed the neighbor
                // step 2.1
                int r = P.rank[neighbor];

                if (P.partSize[r] > 1) { // step 2.2
                    count[r] += 1;
                } else { // step 2.3
                    B[r].push_back(i);
                }
            }
        }

        // step 3.4
        // Returns the ids of paritions that will be used as 'previousU' and 'currentU' in next iteration
        pii splitSubpartition(int &head, int U, int prevU, std::vector<int> &count) {
            int r = U;
            int newRank = r + P.partSize[r] - count[r];
            P.partSize[newRank] = count[r];
            P.partSize[r] = P.partSize[r] - count[r];
            pii partitionIds(U, P.next[U]);
            if (count[r] > 1) {
                /*                                                  P.next[prevU]      P.next[U] (somewhere)
                 *                                                        \/            \/
                 * We're in a situation where we split a partition e.g. / a b c d e / ... in two smaller subparitions and
                 * the first subpartition is larger than 1 element. We need to consider two cases.
                 * (1): second subparition contains only 1 element.
                 *       e.g. / a c d e / b /
                 *      - update P.next[prevU] to 'newRank' (if prevU != -1).
                 *      - update P.next[U]=-1 because P.partSize[U] drops to 1. This is why we're returning next parition from this function instead of traversing the list in for() in 'encodeVertex' function
                 *      - P.next[newRank] = P.next[U]
                 *      - update head if necessary
                 * (2): second subpartition contains more than one element.
                 *      e.g. / c b e / a d / (...)
                 *      - update P.next[prevU] to 'newRank' (if prevU != -1)
                 *      - P.next[U] stays the same
                 *      - P.next[newRank] = U
                 *      - update head if necessary
                 */
                P.next[newRank] = P.partSize[r] > 1 ? r : P.next[r];
                if (P.partSize[r] == 1) {
                    partitionIds.first = newRank;
                    P.next[r] = -1;
                }

                if (prevU != -1)
                    P.next[prevU] = newRank;

                if (newRank > head)
                    head = newRank;
            } else if (P.partSize[U] == 1) {
                /*                                                  P.next[prevU]      P.next[U] (somewhere)
                 *                                                        \/            \/
                 * We're in a situation where we split a partition e.g. / a b  / ... in two subpartitions of size 1.
                 * In this case we have to:
                 * - set P.next[prevU] = P.next[U]
                 * - update head if head==U
                 * - set P.next[U] = -1;
                 */
                if (head == U) {
                    head = P.next[U];
                }
                if (prevU != -1) {
                    P.next[prevU] = P.next[U];
                }
                P.next[U] = -1;

                partitionIds.first = prevU;
            }

            return partitionIds;
        }

        // step 3
        void encodeVertex(int &head,
                          std::vector<int> &count,
                          std::vector<char> &B1,
                          std::vector<std::vector<int>> &B,
                          int i) {
//            std::fesetround(FE_UPWARD); // set rounding to ceil
            int previousU = -1;
            for (int currentU = head; currentU != -1;) {
                // step 3.1
                int r = currentU;
                if (P.partSize[r] <= 1) continue;

                // step 3.2
//                int bitsCnt = std::lrint(std::log2(P.partSize[r] + 1));
                int bitsCnt = ::Utils::log2i_ceil(P.partSize[r] + 1);
                auto bitsToEncode = ::Utils::extractBits(count[r], 0, bitsCnt);
                B1.insert(B1.end(), bitsToEncode.begin(), bitsToEncode.end());

                // step 3.3
                B[r].push_back(-i);
                B[r + P.partSize[r] - 1].push_back(-i);

                // step 3.4
                if (count[r] > 0 && count[r] < P.partSize[r]) {
                    // update size, next, head
                    auto paritionIds = splitSubpartition(head, currentU, previousU, count);
                    previousU = paritionIds.first;
                    currentU = paritionIds.second;
                } else {
                    count[r] = 0;
                    previousU = currentU;
                    currentU = P.next[currentU];
                }
            }
        }

        // step 4
        void moveVertices(int currentV,
                          const PUNGraph &graph,
                          std::vector<int> &count) {
            auto nodeI = graph->GetNI(currentV);
            int v_deg = nodeI.GetDeg();
            for (int nId = 0; nId < v_deg; ++nId) {
                int neighbor = nodeI.GetNbrNId(nId);
                if (P.pos[neighbor] == -1) continue; // we already processed the neighbor
                if (count[P.rank[neighbor]] == 0) continue;
                // step 4.1
                int r = P.rank[neighbor];
                // step 4.2
                count[r] -= 1;

                // step 4.3
                int idxToSwap = r + P.partSize[r] + count[r];
                int vertexToSwap = P.vertex[idxToSwap];
                std::swap(P.pos[neighbor], P.pos[vertexToSwap]);
                std::swap(P.vertex[P.pos[neighbor]], P.vertex[P.pos[vertexToSwap]]);
                // step 4.4
                P.rank[neighbor] += P.partSize[r];
            }
        }

        void printDbgInfo(int head, int currentV, int n, std::vector<int> &count, std::vector<char> &B1) {
            LOG_SCOPE_FUNCTION(1);
            VLOG_F(1, "CurrentV: %d, head: %d", currentV, head);

            std::stringstream s;
            copy(P.pos.begin(), P.pos.end(), std::ostream_iterator<int>(s, ", "));
            VLOG_F(1, "Pos: \t %s", s.str().c_str());
            s.str("");
            s.clear();

            copy(P.rank.begin(), P.rank.end(), std::ostream_iterator<int>(s, ", "));
            VLOG_F(1, "Rank: \t %s", s.str().c_str());
            s.str("");
            s.clear();

            copy(P.vertex.begin(), P.vertex.end(), std::ostream_iterator<int>(s, ", "));
            VLOG_F(1, "Vertex: \t %s", s.str().c_str());
            s.str("");
            s.clear();

            copy(P.partSize.begin(), P.partSize.end(), std::ostream_iterator<int>(s, ", "));
            VLOG_F(1, "Size: \t %s", s.str().c_str());
            s.str("");
            s.clear();

            copy(P.next.begin(), P.next.end(), std::ostream_iterator<int>(s, ", "));
            VLOG_F(1, "Next: \t %s", s.str().c_str());
            s.str("");
            s.clear();

            copy(count.begin(), count.end(), std::ostream_iterator<int>(s, ", "));
            VLOG_F(1, "Count: \t %s", s.str().c_str());
            s.str("");
            s.clear();

            copy(B1.begin(), B1.end(), std::ostream_iterator<int>(s, ", "));
            VLOG_F(1, "B1: \t %s", s.str().c_str());
            s.str("");
            s.clear();
        }

        std::vector<char> encodeB2(
                const std::vector<int> &B2
        ) {
            std::vector<int> processedB2;

            if (B2.empty()) {
                return std::vector<char>();
            }

            long long zeroCnt = std::count(B2.begin(), B2.end(), 0);

            this->prob = ((double) zeroCnt / (double) B2.size());

            if (this->prob == 0.0) kmax = 1;
            else kmax = std::ceil(1.0 / this->prob);
//            LOG_F(INFO, "Using prob: %lf zeroCnt: %ld kmax: %d", this->prob, zeroCnt, kmax);


            std::unordered_map<int, int> freqs;
            for (int i = 0; i <= kmax; ++i) {
                freqs[i] = 0;
            }

            // split values > kmax into consecutive run of kmax's
            for (int val : B2) {
                while (val >= kmax) {
                    freqs[kmax] += 1;
                    processedB2.push_back(kmax);
                    val -= kmax;
                }
                // when decoding we'll read kmax until we encounter a first value < kmax. It may be 0.
                freqs[val] += 1;
                processedB2.push_back(val);
            }

//            double ttlPRob = 0.0;
//            for (int i = 0; i <= kmax; ++i) {
//                ttlPRob += ((double) freqs[i] / (double) processedB2.size());
////                LOG_F(INFO, "FREQ OF: %d IS: %d - %lf", i, freqs[i], ((double) freqs[i] / (double) processedB2.size()));
//            }



            std::unique_ptr<Compressor<std::vector<int>, std::vector<char>>> B2_coder;

            if (useStaticArithCoder) {
                B2_coder = std::make_unique<StaticIntegerCoder<int>>(precomputeZeroRunFreqs(this->prob),
                                                                     kmax);
            } else {
                B2_coder = std::make_unique<AdaptiveIntegerCoder<int>>(
                        kmax); // TODO(pawelp): the probability distiburion of codewords is know, change this possibly.
            }


            auto encodedB2 = B2_coder->encode(processedB2);

            auto beforeBits = int(std::ceil(std::log2(kmax))) * processedB2.size();

            LOG_F(INFO, "Before B2: %d after: %d", beforeBits, encodedB2.size());

            return encodedB2;
        }

    public:

        explicit SzipE(bool reconstructB2_ = false, bool useStaticArithCoder_ = true, bool printDbg_ = false)
                : reconstructB2(reconstructB2_),
                  useStaticArithCoder(
                          useStaticArithCoder_),
                  printDbg(printDbg_) {

        }

        CompressedGraph encode(const PUNGraph &graph) override {
            LOG_SCOPE_FUNCTION(1);
            int n = graph->GetNodes();

            initPartition(graph);
            std::vector<int> count(n, 0);
            std::vector<char> B1;
            std::vector<std::vector<int>> B(n); // will be used to retrieve B2 later
            int head = 0;
            if (printDbg) {
                printDbgInfo(head, -1, n, count, B1);
            }
            LOG_F(INFO, "Starting encode main loop");
            for (int i = 1; i <= graph->GetNodes(); ++i) {
                if (i % 1000 == 0) LOG_F(INFO, "%d / %d", i, graph->GetNodes());

                // step 1
                int currentV = getCurrentVertex(head, n, i);
                if (printDbg) {
                    VLOG_IF_F(1, printDbg, "Iteration %d: After substep 1", i);
                    printDbgInfo(head, currentV, n, count, B1);
                }

                // step 2
                processNeighbours(currentV, graph, count, B, i);
                if (printDbg) {
                    VLOG_F(1, "Iteration %d: After substep 2", i);
                    printDbgInfo(head, currentV, n, count, B1);
                }

                // step 3
                encodeVertex(head, count, B1, B, i);
                if (printDbg) {
                    VLOG_F(1, "Iteration %d: After substep 3", i);
                    printDbgInfo(head, currentV, n, count, B1);
                }

                // step 4
                moveVertices(currentV, graph, count);
                if (printDbg) {
                    VLOG_F(1, "Iteration %d: After substep 4", i);
                    printDbgInfo(head, currentV, n, count, B1);
                }
            }

            // decode B into B2
            LOG_F(INFO, "Starting B to B2 translation");

            // describes the last position of a '1' or ending of a section
            std::vector<int> last(n, -1);
            std::iota(std::begin(last), std::end(last), 0);
            // determines whether the currently processed step is a section
            std::vector<bool> isSection(n, false);
            // counts how many '0' since the last '1'
            std::vector<int> gapCount(n, 0);
            std::vector<std::vector<int>> r_i(n);

            // create r_i
            for (int i = n - 2; i >= 0; --i) {
                while (!B[i].empty()) {
                    processBEntry(n - i, B[i].back(), last, isSection, gapCount, r_i);
                    B[i].pop_back();
                }
            }
            LOG_F(INFO, "Processed B Entries");
            for (int i = 1; i < n; ++i) {
                gapCount[i] += (n - last[i]);
                r_i[i].push_back(gapCount[i]);
            }
            // process r_i by performing carry-ons
            for (int i = 1; i < n - 1; ++i) {
                r_i[i + 1][0] += r_i[i].back();
                r_i[i].pop_back();
            }
            LOG_F(INFO, "Processed R_i carry-ons");

            std::vector<int> B2 = ::Utils::flatten_vec_of_vec(r_i);
            LOG_F(INFO, "Flattened vecs");

            std::vector<char> B2_converted;
            if (this->reconstructB2) {
                // https://stackoverflow.com/questions/30998444/add-same-value-multiple-times-to-stdvector-repeat
                // temporarily reconstruct B2 from 'run of zeroes' format
                bool first_processed = false;
                for (int i : B2) {
                    if (first_processed) {
                        B2_converted.push_back(1);
                    } else {
                        first_processed = true;
                    }
                    if (i > 0) B2_converted.insert(B2_converted.end(), i, 0);
                }
            }
            LOG_F(INFO, "Arithmetic codings");


            std::unordered_map<char, int> b1Freqs;
            b1Freqs[0] = std::count(B1.begin(), B1.end(), 0);
            b1Freqs[1] = std::count(B1.begin(), B1.end(), 1);
            StaticIntegerCoder<char> B1_coder(b1Freqs, 1);

            auto encodedB1 = B1_coder.encode(B1);
            LOG_F(INFO, "Before B1: %ld after: %ld", B1.size(), encodedB1.size());

            auto encodedB2 = encodeB2(B2);

            return CompressedGraph{n, this->prob, b1Freqs[0], b1Freqs[1], encodedB1, encodedB2};
        }

        static void processBEntry(
                int idx,
                int entry,
                std::vector<int> &last,
                std::vector<bool> &isSection,
                std::vector<int> &gapCount,
                std::vector<std::vector<int>> &r_i) {
            if (entry < 0) { // either we've finished the -1 section of we're staring one
                if (!isSection[-entry]) {
                    gapCount[-entry] += (idx - last[-entry] - 1);
                }
                isSection[-entry] = !isSection[-entry];
                last[-entry] = idx;
            } else if (entry > 0) {
                assert(!isSection[entry]);
                gapCount[entry] += (idx - last[entry] - 1);
                r_i[entry].push_back(gapCount[entry]);
                last[entry] = idx;
                gapCount[entry] = 0;
            }
        }
    };

}
#endif //MAGISTERKA_SZIPE_H
