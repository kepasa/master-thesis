//
// Created by pawel on 5/9/20.
//

#include "orderedpartition.h"

#include <utility>

int Graph::Utils::PartitionSubset::extractFirstElement() {
    auto toReturn = *(this->container.begin());
    this->container.erase(this->container.begin());
    return toReturn;
}

void Graph::Utils::PartitionSubset::subtractElement(int element) {
    this->container.erase(element);
}

int Graph::Utils::PartitionSubset::getNeighbourCount(int currentVertex, const PUNGraph& graphPtr) {
    int count = 0;
    for(int node : this->container) {
        count += graphPtr->IsEdge(currentVertex, node);
    }
    return count;
}

Graph::Utils::PartitionSubset::SubsetPair
Graph::Utils::PartitionSubset::splitSubset(int currentVertex, const PUNGraph& graphPtr) {
    auto U1 = std::unordered_set<int>();
    auto U2 = std::unordered_set<int>();

    for(int node : this->container) {
        if(graphPtr->IsEdge(currentVertex, node)) {
            U1.insert(node);
        } else {
            U2.insert(node);
        }
    }

    auto PS_U1 = std::make_shared<PartitionSubset>(U1);
    auto PS_U2 = std::make_shared<PartitionSubset>(U2);

    return std::make_pair(std::move(PS_U1), std::move(PS_U2));
}

int Graph::Utils::PartitionSubset::getSubsetSize() {
    return this->container.size();
}

Graph::Utils::PartitionSubset::PartitionSubset(std::vector<int> &vec) {
    this->container = std::unordered_set<int>(vec.begin(), vec.end());
}

Graph::Utils::PartitionSubset::PartitionSubset(std::unordered_set<int>& s) {
    this->container = std::unordered_set<int>(std::move(s));
}

bool Graph::Utils::PartitionSubset::isEmpty() {
    return this->container.empty();
}

std::unordered_set<int>& Graph::Utils::PartitionSubset::items() {
    return this->container;
}

bool Graph::Utils::OrderedPartition::isEmpty() {
    return this->subsets.empty();
}

void Graph::Utils::OrderedPartition::addSubset(std::shared_ptr<Graph::Utils::PartitionSubset> subset) {
    this->subsets.push_back(subset);
}

int Graph::Utils::OrderedPartition::getSubsetCount() {
    return this->subsets.size();
}

std::shared_ptr<Graph::Utils::PartitionSubset> Graph::Utils::OrderedPartition::getSubset(int subsetIdx) {
    return this->subsets[subsetIdx];
}

std::shared_ptr<Graph::Utils::PartitionSubset> Graph::Utils::OrderedPartition::extractFirstSubset() {
    return this->subsets[0];
}

std::vector<std::shared_ptr<Graph::Utils::PartitionSubset>> &Graph::Utils::OrderedPartition::items() {
    return this->subsets;
}

