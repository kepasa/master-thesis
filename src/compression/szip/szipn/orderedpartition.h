//
// Created by pawel on 5/9/20.
//

#ifndef MAGISTERKA_ORDEREDPARTITION_H
#define MAGISTERKA_ORDEREDPARTITION_H

#include <Snap.h>
#include <vector>
#include <list>
#include <unordered_set>
#include <memory>

// TODO(pawelp): cleanup memory management (use smart pointers everywhere)
namespace Graph::Utils {
    class PartitionSubset {
    using SubsetPair = std::pair<std::shared_ptr<PartitionSubset>,
            std::shared_ptr<PartitionSubset>>;
    private:
        std::unordered_set<int> container;
    public:
        explicit PartitionSubset(std::vector<int>& vec);
        explicit PartitionSubset(std::unordered_set<int>& s);
        ~PartitionSubset() = default;
        int extractFirstElement();
        void subtractElement(int element); // prob not needed?
        int getNeighbourCount(int currentVertex, const PUNGraph& graphPtr);
        SubsetPair splitSubset(int currentVertex, const PUNGraph& graphPtr);
        int getSubsetSize();
        bool isEmpty();
        std::unordered_set<int>& items();
    };
}

namespace Graph::Utils {
    class OrderedPartition {
    private:
        std::vector<std::shared_ptr<PartitionSubset>> subsets;
    public:
        bool isEmpty();
        void addSubset(std::shared_ptr<PartitionSubset> subset);
        int getSubsetCount();
        std::shared_ptr<PartitionSubset> extractFirstSubset();
        std::shared_ptr<PartitionSubset> getSubset(int subsetIdx);
        std::vector<std::shared_ptr<PartitionSubset>>& items();
    };
}

#endif //MAGISTERKA_ORDEREDPARTITION_H
