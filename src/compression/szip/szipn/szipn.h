//
// Created by pawel on 5/9/20.
//
#ifndef MAGISTERKA_SZIPN_H
#define MAGISTERKA_SZIPN_H

#include <Snap.h>
#include "algorithm"
#include <numeric>
#include "compression/arith_coder/adaptive_integer_coder.h"
#include "utils/binaryops.h"
#include "orderedpartition.h"
#include <cfenv>
#include <cmath>
#include <loguru.hpp>
#include <compression/szip/szip.h>


namespace Graph::Compression {
    using ArithmeticCoding::AdaptiveIntegerCoder;
    using Graph::Utils::PartitionSubset;

    class SzipN : public Szip<char> {
    protected:
        std::vector<char> decodeB2(CompressedGraph &encodedGraph) override {
            auto B2_coder = AdaptiveIntegerCoder<char>(1);
            return B2_coder.decode(encodedGraph.B2);
        }

        int currentB2Idx;

        void initializeDecode(CompressedGraph &encodedGraph) override {
            this->currentB2Idx = 0;
        }

        int getNeighbourCount(std::vector<char> &B2) override {
            auto neighboursCount = ::Utils::decodeBits(
                    B2.begin() + currentB2Idx,
                    B2.begin() + currentB2Idx + 1);
            ++currentB2Idx;
            return neighboursCount;
        }

    public:
        CompressedGraph encode(const PUNGraph &graphPtr) override {
            std::vector<char> B1;
            std::vector<char> B2;
            auto B2_coder = std::make_unique<AdaptiveIntegerCoder<char>>(1);

            auto currentParition = new Graph::Utils::OrderedPartition();
            int graphSize = graphPtr->GetNodes();
            int e = graphPtr->GetEdges();
            currentParition->addSubset(createInitialPartition(graphSize));

            while (!currentParition->isEmpty()) {
                auto nextPartition = new Graph::Utils::OrderedPartition();
                int currentNode = currentParition->getSubset(0)->extractFirstElement();

                for (const auto &subset : currentParition->items()) {
                    if (subset->getSubsetSize() == 0) continue;
                    int neighbourCount = subset->getNeighbourCount(currentNode, graphPtr);

                    if (subset->getSubsetSize() > 1) {
                        int bitsCnt = ::Utils::log2i_ceil(subset->getSubsetSize() + 1);
                        auto bitsToEncode = ::Utils::extractBits(neighbourCount, 0, bitsCnt);
                        B1.insert(B1.end(), bitsToEncode.begin(), bitsToEncode.end());
                    } else {
                        auto bitsToEncode = ::Utils::extractBits(neighbourCount, 0, 1);
                        B2.insert(B2.end(), bitsToEncode.begin(), bitsToEncode.end());
                    }

                    auto splitSubset = subset->splitSubset(currentNode, graphPtr);
                    if (!splitSubset.first->isEmpty()) {
                        nextPartition->addSubset(splitSubset.first);
                    }
                    if (!splitSubset.second->isEmpty()) {
                        nextPartition->addSubset(splitSubset.second);
                    }
                }
                std::swap(nextPartition, currentParition);
                delete nextPartition;
            }
            delete currentParition;
            std::unordered_map<char, int> b1Freqs;
            b1Freqs[0] = std::count(B1.begin(), B1.end(), 0);
            b1Freqs[1] = std::count(B1.begin(), B1.end(), 1);
            auto B1_coder = std::make_unique<StaticIntegerCoder<char>>(b1Freqs, 1);
            auto p =  (double) e / (double((graphSize * (graphSize-1)) / double(2)));


            return CompressedGraph{graphSize, p, b1Freqs[0], b1Freqs[1], B1_coder->encode(B1), B2_coder->encode(B2)};
        }
    };
}

#endif //MAGISTERKA_SZIPN_H
