//
// Created by pawel on 9/6/20.
//

#ifndef MAGISTERKA_HUFF_CODER_H
#define MAGISTERKA_HUFF_CODER_H

#include <compression/compressor.h>
#include <unordered_map>
#include <memory>
#include <queue>
#include <algorithm>
#include <utils/binaryops.h>

namespace HuffmanCoding {
    using Graph::Compression::Compressor;

    struct HuffmanBinaryTree {
        int occurenceCount;
        int key;
        HuffmanBinaryTree *parent;
        HuffmanBinaryTree *leftChild;
        HuffmanBinaryTree *rightChild;

        ~HuffmanBinaryTree() {
            if (leftChild != nullptr) {
                delete leftChild;
            }

            if (rightChild != nullptr) {
                delete rightChild;
            }
        }
    };

    class Compare {
    public:
        bool operator()(HuffmanBinaryTree *a, HuffmanBinaryTree *b) {

            return (a->occurenceCount > b->occurenceCount);
        }
    };

    struct HuffmanCode {
        std::vector<char> code;
        std::unique_ptr<HuffmanBinaryTree> tree;
        std::unordered_map<int, int> symbolToCodelen;
        int symbolBits;
        int codelenBits;
        int maxSymbol;
    };

    class HuffmanCoder : Compressor<std::vector<int>, HuffmanCode> {
    private:
        std::vector<char> getCodeFromNode(HuffmanBinaryTree *node) {
            auto current = node;
            std::vector<char> code;
            while (current->parent != nullptr) {
                auto prnt = current->parent;
                if (prnt->leftChild == current) {
                    code.push_back(0);
                } else {
                    code.push_back(1);
                }
                current = prnt;
            }
            std::reverse(code.begin(), code.end());
            return code;
        }

    public:
        HuffmanCode encode(std::vector<int> &toEncode) override {
            std::unordered_map<int, int> freqs;
            for (auto i : toEncode) {
                if (freqs.count(i) == 0) {
                    freqs[i] = 1;
                } else {
                    freqs[i] = freqs[i] + 1;
                }
            }

            std::unordered_map<int, HuffmanBinaryTree *> elemToNode;
            std::priority_queue<HuffmanBinaryTree *, std::vector<HuffmanBinaryTree *>, Compare> Q;
            for (auto entry : freqs) {
                auto leaf = new HuffmanBinaryTree{entry.second, entry.first, nullptr, nullptr, nullptr};
                elemToNode[entry.first] = leaf;
                Q.push(leaf);
            }

            while (Q.size() > 1) {
                auto t1 = Q.top();
                Q.pop();
                auto t2 = Q.top();
                Q.pop();
                auto t = new HuffmanBinaryTree{t1->occurenceCount + t2->occurenceCount, 0, nullptr, t1, t2};
                t1->parent = t;
                t2->parent = t;
                Q.push(t);
            }

            // only 1 element in input
            if (freqs.size() == 1) {
                auto t1 = Q.top();
                Q.pop();
                auto t = new HuffmanBinaryTree{t1->occurenceCount, 0, nullptr, t1, nullptr};
                t1->parent = t;
                Q.push(t);
            }

            std::unordered_map<int, std::vector<char>> elemToCode;
            std::unordered_map<int, int> elemToCodelen;
            int symbolBits = -1;
            int codelenBits = -1;

            for (auto entry : elemToNode) {
                elemToCode[entry.first] = getCodeFromNode(entry.second);
                elemToCodelen[entry.first] = elemToCode[entry.first].size();
                symbolBits = std::max(entry.first, symbolBits);
                codelenBits = std::max(codelenBits, elemToCodelen[entry.first]);
            }

            std::vector<char> encoded;
            for (auto i : toEncode) {
                auto iCode = elemToCode[i];
                encoded.insert(encoded.end(), iCode.begin(), iCode.end());
            }

            auto tree = Q.empty() ? nullptr : Q.top();

            return {
                    encoded,
                    std::unique_ptr<HuffmanBinaryTree>(tree),
                    elemToCodelen,
                    (symbolBits > 1 ? Utils::log2i_ceil(symbolBits) : 1),
                    (codelenBits > 1 ? Utils::log2i_ceil(codelenBits) : 1),
                    symbolBits
            };
        }

        std::vector<int> decode(HuffmanCode &toDecode) override {
            auto it = toDecode.code.begin();
            auto treeIt = toDecode.tree.get();
            std::vector<int> decoded;
            if (treeIt == nullptr)
                return decoded;

            while (it != toDecode.code.end()) {
                if (treeIt->leftChild == nullptr && treeIt->rightChild == nullptr) {
                    decoded.push_back(treeIt->key);
                    treeIt = toDecode.tree.get();
                } else {
                    if (*it == 0) {
                        treeIt = treeIt->leftChild;
                    } else if (*it == 1) {
                        treeIt = treeIt->rightChild;
                    }
                    ++it;
                }
            }
            decoded.push_back(treeIt->key);


            return decoded;
        }
    };
}


#endif //MAGISTERKA_HUFF_CODER_H
