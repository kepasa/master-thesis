//
// Created by pawel on 7/14/20.
//

#ifndef MAGISTERKA_ARITHMETIC_CODING_H
#define MAGISTERKA_ARITHMETIC_CODING_H

#include <vector>
#include <iostream>
#include <utils/binaryops.h>
#include <memory>

namespace ArithmeticCoding {

    constexpr static int B_BITS = 32;
    constexpr static int F_BITS = 27;
    constexpr static unsigned long long MAX_BITS_OUTSTANDING = (unsigned long long) 1 << 31;

    typedef unsigned long code_value;    /* B_BITS of precision */
    typedef unsigned long freq_value;    /* F_BITS+1 of precision */
    typedef unsigned long div_value;    /* B_BITS-F_BITS of precision */

    class Context {
    private:
        int num_of_symbols;
    public:
        explicit Context(int _num_of_symbols) : num_of_symbols(_num_of_symbols) {
        };

        virtual int install_symbol(int symbol) = 0;

        virtual void encode(int symbol) = 0;

        virtual int decode() = 0;

        virtual void purge_context() = 0;
    };

    class ArithmeticCoder {
    private:
        freq_value L;
        freq_value R;
        freq_value r;
        freq_value D;
        unsigned long long bits_outstanding;

        std::vector<char>* input;
        int input_idx = 0;
        std::vector<char>* output;

        // todo(pawelp): implement this functions. At first probably just reading/writing to vector will be ok
        void write_one_bit(bool bit) {
            output->push_back(bit);
        }

        freq_value read_one_bit() {
            return (*input)[input_idx++];
        }

        /*
         * Write the bit (0 or 1) to the output bit stream, plus any outstanding following bits,
         * which are known to be of opposite polarity
         */
        void bit_plus_follow(bool bit) {
            write_one_bit(bit);
            while (bits_outstanding > 0) {
                write_one_bit(!bit);
                --bits_outstanding;
            }
        }

    public:
        void install_input(std::vector<char> &inpt) {
            input_idx=0;
            this->input = &inpt;
        }

        void install_output(std::vector<char> &outpt) {
            this->output = &outpt;
        }

        // Initialize encoder state variables
        void start_encode() {
            L = 0;
            R = (freq_value) 1 << (B_BITS - 1);
            bits_outstanding = 0;
        }

        // Flush encoder so that all information is in the output bit stream
        void finish_encode() {
            for (int b = B_BITS - 1; b >= 0; --b) {
                bit_plus_follow(((L >> b) & 1));
            }
        }

        // Initialize decoder state variables
        void start_decode() {
            input_idx = 0;
            R = (freq_value) 1 << (B_BITS - 1);
            D = 0;
            for (int i = 0; i < B_BITS; ++i) {
                D = 2 * D + read_one_bit();
            }
        }

        // No action necessary.
        void finish_decode() {
            return;
        }


        /*
         * Arithmetically encode the range [l/t, h/t) using low-precision arithmetic.
         * The state variables R and L are modified to reflect the new range, and then
         * renormalized to restore the initial and final invariants 2^{b-2} < R <= 2^{b-1},
         * 0 <= L <= 2^b - 2^{b-2}, and L+R <= 2^b.
         */
        void arithmetic_encode(freq_value l, freq_value h, freq_value t) {
//            std::cerr << "L: " << L << " R: " << R << std::endl;
//            std::cerr << "l: " << l << " h: " << h << " t: " << t << std::endl;
            r = R / t;
            L = L + r * l;
            if (h < t) {
                R = r * (h - l);
            } else {
                R = R - r * l;
            }

            /*
             * Reestablish the invariant on R, namely that 2^{b-2} < R <= 2^{b-1}. Each doubling
             * of R corresponds to the output of one bit, either of known value, or of the value
             * opposite to the value of the next bit actually output
             */
            while (R <= ((freq_value) 1 << (B_BITS - 2))) {
                if (L + R <= ((freq_value) 1 << (B_BITS - 1))) {
                    bit_plus_follow(false);
                } else if (((freq_value) 1 << (B_BITS - 1)) <= L) {
                    bit_plus_follow(true);
                    L -= ((freq_value) 1 << (B_BITS - 1));
                } else {
                    bits_outstanding += 1;
                    L -= ((freq_value) 1 << (B_BITS - 2));
                }
                L *= 2;
                R *= 2;
            }
        }

        /*
         * Returns an integer target, 0 <= target < t that is guaranteed to lie in the range
         * [l, h) that was used at the corresponding call to arithmetic_encode()
         */
        freq_value decode_target(freq_value t) {
            r = R / t;
            return std::min(t - 1, D / r);
        }

        /*
         * Adjusts the decoder's state variables R and D to reflect the changes made in the
         * encoder during the corresponding call to arithmetic_encode(). The transformation
         * D = V - L is used. It is also assumed that r has been set by a prior call to decode_target.
         */
        void arithmetic_decode(freq_value l, freq_value h, freq_value t) {
//            std::cerr << "D: " << D << " R: " << R << std::endl;
//            std::cerr << "l: " << l << " h: " << h << " t: " << t << std::endl;
            D = D - r * l;
            if (h < t) {
                R = r * (h - l);
            } else {
                R = R - r * l;
            }

            while (R <= ((freq_value) 1 << (B_BITS - 2))) {
                R = 2*R;
                D = 2 * D + read_one_bit();
            }
        }

    };

    struct BinaryContext {
        virtual int binary_encode(int bit) = 0;

        virtual int binary_decode() = 0;
    };

}

#endif //MAGISTERKA_ARITHMETIC_CODING_H
