//
// Created by pawel on 7/16/20.
//

#ifndef MAGISTERKA_FENWICK_TREE_H
#define MAGISTERKA_FENWICK_TREE_H

#include <vector>
#include "arithmetic_coding.h"

namespace ArithmeticCoding {

    class FenwickTree {
    private:
        std::vector<freq_value> F;
        int n; // alphabet size == F.size() - 1;
        freq_value total_count;

        static int backward(int i) {
            return i & (i - 1);
        }

        static int forward(int i) {
            return i + (i & (-i));
        }

    public:
        explicit FenwickTree(int _n) : n(_n) {
            this->F = std::vector<freq_value>(n + 1, 0);

            // install symbols
            for (int i = 1; i <= n; ++i) {
                fenwick_incr_frequency(i - 1, 1);
            }

            total_count = n;
        }

        void fenwick_incr_frequency(int symbol, freq_value incr) {
            int i = symbol + 1;

            while (i <= n) {
                F[i] += incr;
                i = forward(i);
            }
            total_count += incr;
        }

        void fenwick_decr_frequency(int symbol, freq_value decr) {
            int i = symbol + 1;

            while (i <= n) {
                F[i] -= decr;
                i = forward(i);
            }
            total_count -= decr;
        }

        // symbols start from 1
        freq_value fenwick_get_frequency(int symbol) {
            int i = symbol + 1;
            freq_value hs = 0;
            while (i != 0) {
                hs += F[i];
                i = backward(i);
            }
            return hs;
        }

        freq_value fenwick_get_total() const {
            return total_count;
        }

        /*
         * Return the symbol that, if passed as argument to function
         * fenwick_get_frequency(), would return the least value that exceeds
         * the parameter target.
         */
        int fenwick_get_symbol(freq_value target) {
            int s = 0;
            int mid = 1 << (Utils::log2i_floor(n));
            while (mid > 0) {
                if (s + mid <= n && F[s + mid] <= target) {
                    target -= F[s + mid];
                    s += mid;
                }
                mid /= 2;
            }
            return s; // because F[0] = 0 and F[1] is freq of symbol 0;
        }

        void halveFreqs() {
            std::vector<freq_value> freqs;

            for (int i = 0; i < n; ++i) {
                auto cum_freq_i = fenwick_get_frequency(i);
                freqs.push_back(cum_freq_i);
            }

            for (auto &i : F) {
                i = 0;
            }
            total_count = 0;

            for (int i = freqs.size() - 1; i > 0; --i) {
                auto freq_i = freqs[i] - freqs[i-1];
                auto toIncr = std::max(freq_i/2, (freq_value)1);
                fenwick_incr_frequency(i, toIncr);
            }
            fenwick_incr_frequency(0, std::max(freqs[0]/2, (freq_value)1));
        }
    };

}

#endif //MAGISTERKA_FENWICK_TREE_H
