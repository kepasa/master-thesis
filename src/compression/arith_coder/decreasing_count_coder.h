//
// Created by pawel on 9/9/20.
//

#ifndef MAGISTERKA_DECREASING_COUNT_CODER_H
#define MAGISTERKA_DECREASING_COUNT_CODER_H
#include "arithmetic_coding.h"
#include "compression/compressor.h"
#include "decreasing_count_context.h"
#include <vector>
#include <loguru.hpp>

namespace ArithmeticCoding {
    using Graph::Compression::Compressor;

    template<typename ST>
    class DecreasingCountCoder : public Compressor<std::vector<ST>, std::vector<char>> {
    private:
        // A maximum integer that will be handled.
        int k_max;
        const int EOF_SYMBOL;
        ArithmeticCoder coder;
        std::unordered_map<ST, int> &symbolFreqs;
        std::unique_ptr<DecreasingCountContext<ST>> context;

    public:
        DecreasingCountCoder(std::unordered_map<ST, int> symbolFreqs_, int _k_max) : symbolFreqs(symbolFreqs_), k_max(_k_max),
                                                                                   EOF_SYMBOL(_k_max + 1) {

            context = std::make_unique<DecreasingCountContext<ST>>(coder, symbolFreqs, k_max + 2);
        }

        // todo(pawelp) - template for source and result
        std::vector<char> encode(std::vector<ST> &source) override {
            std::vector<char> result;

            coder.install_output(result);
            coder.start_encode();

            LOG_F(INFO, "Starting encode, source size: %d", source.size());
            int i = 0;
            for (auto &elem : source) {
                if (elem > k_max) throw std::out_of_range("Element too large");
                context->encode((int) elem);
            }

            // Encode end of message symbol
            context->encode(EOF_SYMBOL);
            coder.finish_encode();

            LOG_F(INFO, "Finished encode");

            // cleanup
            context->purge_context();

            return result;
        }

        std::vector<ST> decode(std::vector<char> &code) override {

            coder.install_input(code);
            coder.start_decode();

            std::vector<ST> result;
            ST symbol;
            do {
                symbol = (ST) context->decode();

                if (symbol != EOF_SYMBOL) {
                    result.push_back(symbol);
                }
            } while (symbol != EOF_SYMBOL);

            // cleanup
            context->purge_context();

            return result;
        }
    };

}
#endif //MAGISTERKA_DECREASING_COUNT_CODER_H
