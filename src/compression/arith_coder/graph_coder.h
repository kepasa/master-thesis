//
// Created by pawel on 9/10/20.
//

#ifndef MAGISTERKA_GRAPH_CODER_H
#define MAGISTERKA_GRAPH_CODER_H
#include "arithmetic_coding.h"
#include "compression/compressor.h"
#include "utils/graph.h"
#include <vector>
#include <loguru.hpp>
#include "static_integer_coder.h"
#include <Snap.h>
#include "algorithm"

namespace ArithmeticCoding {
    using Graph::Compression::Compressor;

    struct ArithmeticCodingGraph {
        std::vector<char> code;
        int n;
        double p;
    };

    class GraphCoder : public Compressor<const PUNGraph, ArithmeticCodingGraph> {

    public:
        GraphCoder()= default;

        ArithmeticCodingGraph encode(const PUNGraph &source) override {
            std::vector<char> adjMatrix = ::Utils::toAdjacencyMatrix(source);
            int n = source->GetNodes();
            int e = source->GetEdges();
            auto nchoose2 = double(n * (n - 1)) / double(2);
            auto p = (double) e / nchoose2;

            assert(adjMatrix.size() == (n*(n-1))/2);

            std::unordered_map<char, int> freqs;
            freqs[0] = std::max(1, int((1-p) * adjMatrix.size()));
            freqs[1] = std::max(1, int(p * adjMatrix.size()));

            auto encoded = StaticIntegerCoder<char>(freqs, 1).encode(adjMatrix);

            return {encoded, n, p};
        }

        const PUNGraph decode(ArithmeticCodingGraph &code) override {
            std::unordered_map<char, int> freqs;
            freqs[0] = std::max(1, int((1-code.p) * (code.n * (code.n-1))/2));
            freqs[1] = std::max(1, int(code.p * (code.n * (code.n-1))/2));

            auto decoded = StaticIntegerCoder<char>(freqs, 1).decode(code.code);

            return ::Utils::fromAdjacencyMatrix(decoded, code.n);
        }
    };

}

#endif //MAGISTERKA_GRAPH_CODER_H
