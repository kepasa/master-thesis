//
// Created by pawel on 9/9/20.
//

#ifndef MAGISTERKA_DECREASING_COUNT_CONTEXT_H
#define MAGISTERKA_DECREASING_COUNT_CONTEXT_H
#include <vector>
#include <memory>
#include <unordered_map>
#include <iostream>
#include "arithmetic_coding.h"
#include "fenwick_tree.h"

namespace ArithmeticCoding {
    template<typename ST>
    class DecreasingCountContext : public Context {
    private:
        ArithmeticCoder &arithmeticCoder;
        std::unique_ptr<FenwickTree> tree;
        freq_value increment;
        int num_of_symbols;
        std::unordered_map<ST, int> symbols;
    public:

        DecreasingCountContext(ArithmeticCoder &_arithmeticCoder, std::unordered_map<ST, int> symbols_, int _num_of_symbols)
                : Context(_num_of_symbols), arithmeticCoder(_arithmeticCoder),
                  num_of_symbols(_num_of_symbols), symbols(symbols_) {
            tree = std::make_unique<FenwickTree>(num_of_symbols);
            for(int i = 0; i < _num_of_symbols; ++i){
                tree->fenwick_decr_frequency(i, 1);
            }
            tree->fenwick_incr_frequency(_num_of_symbols-1, 1);
            for(auto& entry : symbols) {
                tree->fenwick_incr_frequency(entry.first, entry.second);
                while (tree->fenwick_get_total() > ((freq_value) 1 << F_BITS)) {
                    tree->halveFreqs();
                }
            }
        }

        int install_symbol(int symbol) override {
            return 0;
        }

        void encode(int symbol) override {
            auto ls = tree->fenwick_get_frequency(symbol - 1);
            auto hs = tree->fenwick_get_frequency(symbol);
            auto t = tree->fenwick_get_total();
            tree->fenwick_decr_frequency(symbol, 1);
//            std::cerr << "before arith_decode for symbol: " << symbol << " low: " << ls << " high: " << hs << std::endl;

            arithmeticCoder.arithmetic_encode(ls, hs, t);
        }

        int decode() override {
            auto total = tree->fenwick_get_total();

            auto target = arithmeticCoder.decode_target(total);

            int symbol = tree->fenwick_get_symbol(target);
            auto low = tree->fenwick_get_frequency(symbol - 1);
            auto high = tree->fenwick_get_frequency(symbol);
            tree->fenwick_decr_frequency(symbol, 1);

//            std::cerr << "before arith_decode for symbol: " << symbol << " low: " << low << " high: " << high << std::endl;
            arithmeticCoder.arithmetic_decode(low, high, total);

            return symbol;
        }

        void purge_context() override {
            tree = std::make_unique<FenwickTree>(num_of_symbols);
            for(int i = 0; i < num_of_symbols; ++i){
                tree->fenwick_decr_frequency(i, 1);
            }
            tree->fenwick_incr_frequency(num_of_symbols-1, 1);
            for(auto& entry : symbols) {
                tree->fenwick_incr_frequency(entry.first, entry.second);
                while (tree->fenwick_get_total() > ((freq_value) 1 << F_BITS)) {
                    tree->halveFreqs();
                }
            }
        }

    };

}
#endif //MAGISTERKA_DECREASING_COUNT_CONTEXT_H
