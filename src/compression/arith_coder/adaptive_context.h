//
// Created by pawel on 7/14/20.
//

#ifndef MAGISTERKA_ADAPTIVE_CONTEXT_H
#define MAGISTERKA_ADAPTIVE_CONTEXT_H

#include <vector>
#include <memory>
#include "arithmetic_coding.h"
#include "fenwick_tree.h"

namespace ArithmeticCoding {
    class AdaptiveContext : public Context {
    private:
        ArithmeticCoder &arithmeticCoder;
        std::unique_ptr<FenwickTree> tree;
        freq_value increment;
        int num_of_symbols;
    public:

        AdaptiveContext(ArithmeticCoder &_arithmeticCoder, int _num_of_symbols)
                : Context(_num_of_symbols), arithmeticCoder(_arithmeticCoder),
                  num_of_symbols(_num_of_symbols) {
            increment = (freq_value) 1 << F_BITS;
            tree = std::make_unique<FenwickTree>(num_of_symbols);
        }

        int install_symbol(int symbol) override {
            return 0;
        }

        void encode(int symbol) override {
            auto ls = tree->fenwick_get_frequency(symbol - 1);
            auto hs = tree->fenwick_get_frequency(symbol);
            auto t = tree->fenwick_get_total();
            tree->fenwick_incr_frequency(symbol, increment);

//            std::cerr << "before arith_encode for symbol: " << symbol << std::endl;
            arithmeticCoder.arithmetic_encode(ls, hs, t);

            while (tree->fenwick_get_total() > ((freq_value) 1 << F_BITS)) {
                tree->halveFreqs();
                increment = (increment + 1) / 2;
            }

        }

        int decode() override {
            auto total = tree->fenwick_get_total();

            auto target = arithmeticCoder.decode_target(total);

            int symbol = tree->fenwick_get_symbol(target);
            auto low = tree->fenwick_get_frequency(symbol - 1);
            auto high = tree->fenwick_get_frequency(symbol);
            tree->fenwick_incr_frequency(symbol, increment);

//            std::cerr << "before arith_decode for symbol: " << symbol << std::endl;
            arithmeticCoder.arithmetic_decode(low, high, total);

            while (tree->fenwick_get_total() > ((freq_value) 1 << F_BITS)) {
                tree->halveFreqs();
                increment = (increment + 1) / 2;
            }

            return symbol;
        }

        void purge_context() override {
            increment = (freq_value) 1 << F_BITS;
            tree = std::make_unique<FenwickTree>(num_of_symbols);
        }

    };

}

#endif //MAGISTERKA_ADAPTIVE_CONTEXT_H
