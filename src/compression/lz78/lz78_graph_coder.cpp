//
// Created by pawel on 9/12/20.
//

#include "lz78_graph_coder.h"

LZ78::LZ78Graph LZ78::LZ78GraphCoder::encode(const PUNGraph &graph) {
    std::vector<char> adjMatrix = ::Utils::toAdjacencyMatrix(graph);

    auto encoded = LZ78().encode(adjMatrix);

    return {encoded, graph->GetNodes()};
}

const PUNGraph LZ78::LZ78GraphCoder::decode(LZ78Graph &code) {
    auto decoded = LZ78().decode(code.code);
    return ::Utils::fromAdjacencyMatrix(decoded, code.n);
}
