//
// Created by pawel on 9/10/20.
//

#ifndef MAGISTERKA_LZ78_H
#define MAGISTERKA_LZ78_H


#include <string>
#include <unordered_map>
#include <vector>
#include <compression/compressor.h>

namespace LZ78 {
    using Graph::Compression::Compressor;

    class LZ78 : public Compressor<std::vector<char>, std::vector<unsigned char>> {
    private:
        unsigned int currentEntry{};
        int smallWordsCnt{};
        bool largeOccured{};
        const unsigned int MAX_DICT_ENTRY = (1 << 24);
        std::unordered_map<std::string, unsigned int> dict;
        std::vector<std::string> decodeDict;

        void initDict();

        void initDecodeDict();

        std::string decodeWord(int c);

        unsigned int readCode(int& idx, std::vector<unsigned char>& toDecode) const;

        void emitCode(unsigned int code, std::vector<unsigned char> &output) const;

        void initCoder();

    public:
        LZ78();

        std::vector<unsigned char> encode(std::vector<char> &toEncode) override;

        std::vector<char> decode(std::vector<unsigned char> &toDecode) override;
    };
}
#endif //MAGISTERKA_LZ78_H
