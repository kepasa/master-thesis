//
// Created by pawel on 9/11/20.
//

#ifndef MAGISTERKA_LZ78_GRAPH_CODER_H
#define MAGISTERKA_LZ78_GRAPH_CODER_H

#include <compression/compressor.h>
#include <Snap.h>
#include <utils/graph.h>
#include "lz78.h"

namespace LZ78 {
    using Graph::Compression::Compressor;

    struct LZ78Graph {
        std::vector<unsigned char> code;
        int n;
    };

    class LZ78GraphCoder : public Compressor<const PUNGraph, LZ78Graph> {
    public:
        LZ78GraphCoder() = default;

        LZ78Graph encode(const PUNGraph &graph) override;

        const PUNGraph decode(LZ78Graph &code) override;
    };
}
#endif //MAGISTERKA_LZ78_GRAPH_CODER_H
