//
// Created by pawel on 9/12/20.
//
#include "lz78.h"

namespace LZ78 {
    LZ78::LZ78() {
        initCoder();
    }

    void LZ78::initCoder() {
        this->currentEntry = 0;
        this->dict = std::unordered_map<std::string, unsigned int>();
        this->decodeDict = std::vector<std::string>();
        this->smallWordsCnt = 0;
        this->largeOccured = false;
    }

    void LZ78::initDict() {
        for (int i = 0; i < 256; ++i) {
            std::string current{(char) i};
            this->dict[current] = this->currentEntry++;
        }
        // this->currentEntry++; // 256 is reserved
    }

    void LZ78::emitCode(unsigned int code, std::vector<unsigned char> &output) const {

        auto c1 = (unsigned char) ((0xFF0000 & code) >> 16);
        auto c2 = (unsigned char) ((0x00FF00 & code) >> 8);
        auto c3 = (unsigned char) ((0x0000FF & code));
        if (this->currentEntry < ((1 << 16))) {
//            std::cout << c2 << c3;
            output.push_back(c2);
            output.push_back(c3);
        } else {
//            std::cout << c1 << c2 << c3;
            output.push_back(c1);
            output.push_back(c2);
            output.push_back(c3);
        }
    }

    std::vector<unsigned char> LZ78::encode(std::vector<char> &toEncode) {
        initCoder();
        std::vector<unsigned char> output;
        int longest_match = -1;
        int emit = 0;
        int readBytes = 0;
        this->initDict();

        std::string currentSequence;
        for (char b : toEncode) {
            std::string tmpStr = std::string{(char) b};
            if (this->dict.find(currentSequence + tmpStr) == this->dict.end()) {
                longest_match = std::max(longest_match, (int) currentSequence.length());
                emitCode(this->dict[currentSequence], output);

                if (this->currentEntry < this->MAX_DICT_ENTRY) {
                    this->dict[currentSequence + tmpStr] = ++currentEntry;
                }
                currentSequence = tmpStr;
            } else {
                currentSequence += tmpStr;
            }
        }
        if (!currentSequence.empty()) {
            emitCode(this->dict[currentSequence], output);
        }
        emitCode((unsigned int) 256, output);
        // std::cerr << "at the end we have: " << currentSequence << std::endl;

        // std::cerr << "longest match: " << longest_match << std::endl;
        // std::cerr << "size: " << this->dict.size();
        // for(auto& it : this->dict) {
        // if(it.first.length() > 5)
        // std::cerr << "key: " << it.first << " entry: " << it.second << std::endl;
        // }
        return output;
    }

    std::string LZ78::decodeWord(int c) {
        if (c == 256) return "";
        if (c >= 256 * 256) this->largeOccured = true;
        if (c < 256 * 256 && !this->largeOccured) this->smallWordsCnt++;
        return this->decodeDict[c];
    }

    unsigned int LZ78::readCode(int &idx, std::vector<unsigned char> &toDecode) const {
        auto b = toDecode[idx++];
        unsigned int code;
        if (this->currentEntry < (1 << 16) - 1) {
            code = ((unsigned int) b << 8) | ((unsigned int) toDecode[idx++]);
        } else {
            code = ((unsigned int) b << 16) | ((unsigned int) (toDecode[idx++]) << 8) |
                   ((unsigned int) toDecode[idx++]);
        }
        return code;
    }

    void LZ78::initDecodeDict() {
        this->decodeDict = std::vector<std::string>(this->MAX_DICT_ENTRY, "");
        for (int i = 0; i < 256; ++i) {
            this->decodeDict[i] = (std::string{(char) i});
        }
        this->decodeDict[256] = ("this is terminator string"); // 256 is reserved
        this->currentEntry = 256;
    }

    std::vector<char> LZ78::decode(std::vector<unsigned char> &toDecode) {
        std::vector<char> decoded;
        initCoder();
        this->initDecodeDict();
        // https://stackoverflow.com/questions/56531241/fast-and-simple-way-to-read-from-stdin-one-byte-at-a-time-in-c
        // FILE *f = std::freopen(nullptr, "rb", stdin);
        std::string prevSeq, currentSequence;
        int idx = 0;
        int pCode, code;
        pCode = this->readCode(idx, toDecode);
        int processed = 0;
        prevSeq = this->decodeWord(pCode);
        // std::cerr << "decoded: " << pCode << " seq: " << prevSeq << std::endl;
        if (!prevSeq.empty()) decoded.insert(decoded.end(), prevSeq.begin(), prevSeq.end());
        while (pCode != 256) {
            ++processed;
            code = this->readCode(idx, toDecode);
            if (code == currentEntry + 1) {
                // std::cerr << "adding at: " << code << " string: " << prevSeq + prevSeq[0] << std::endl;
                this->decodeDict[code] = prevSeq + prevSeq[0];
            }
            currentSequence = this->decodeWord(code);
            // std::cerr << "decoded: " << code << " seq: " << currentSequence << " newseq: " << prevSeq + currentSequence[0] << " address: " << currentEntry+1 << std::endl;
            decoded.insert(decoded.end(), currentSequence.begin(), currentSequence.end());
            this->decodeDict[++currentEntry] = (prevSeq + currentSequence[0]);

            prevSeq = currentSequence;
            pCode = code;
        }
//        std::cerr << "small words: " << smallWordsCnt << " pCode: " << pCode << " processed: " << processed
//                  << " entries: " << this->currentEntry << std::endl;
        return decoded;
    }
}