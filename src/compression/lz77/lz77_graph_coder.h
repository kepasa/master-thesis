//
// Created by pawel on 9/11/20.
//

#ifndef MAGISTERKA_LZ77_GRAPH_CODER_H
#define MAGISTERKA_LZ77_GRAPH_CODER_H

#include <compression/compressor.h>
#include <Snap.h>
#include <utils/graph.h>
#include "lz77.h"

namespace LZ77 {
    using Graph::Compression::Compressor;

    struct LZ77Graph {
        std::vector<symbol> code;
        int n;
    };

    class LZ77GraphCoder : public Compressor<const PUNGraph, LZ77Graph> {
    public:
        LZ77GraphCoder() = default;

        LZ77Graph encode(const PUNGraph &graph) override;

        const PUNGraph decode(LZ77Graph &code) override;
    };
}

#endif //MAGISTERKA_LZ77_GRAPH_CODER_H
