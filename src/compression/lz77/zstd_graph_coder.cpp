//
// Created by pawel on 9/12/20.
//

#include "zstd_graph_coder.h"

LZ77::ZSTDGraph LZ77::ZSTDGraphCoder::encode(const PUNGraph &graph) {
    std::vector<char> adjMatrix = ::Utils::toAdjacencyMatrix(graph, false);
    if(adjMatrix.empty()) return {graph->GetNodes(), adjMatrix};
    std::vector<char> result(ZSTD_compressBound(adjMatrix.size()));

    auto copressedSize = ZSTD_compress(
            static_cast<void *>(result.data()),
            result.size(),
            static_cast<void *>(adjMatrix.data()),
            adjMatrix.size(),
            ZSTD_maxCLevel());
    if (ZSTD_isError(copressedSize)) {
        LOG_F(ERROR, "ERROR from ZSTD");
    } else {
        result.resize(copressedSize);
    }
    return {graph->GetNodes(), result};
}

const PUNGraph LZ77::ZSTDGraphCoder::decode(ZSTDGraph &code) {
    if(code.code.empty()) {
        return ::Utils::fromAdjacencyMatrix(code.code, code.n, false);
    }


    unsigned long long const rSize = ZSTD_getFrameContentSize(
            static_cast<void *>(code.code.data()),
            code.code.size()
    );

    std::vector<char> adjMatrix(rSize);

    auto dSize = ZSTD_decompress(
            static_cast<void *>(adjMatrix.data()),
            adjMatrix.size(),
            static_cast<void *>(code.code.data()),
            code.code.size()
    );
//    if (ZSTD_isError(dSize)) {
//        LOG_F(ERROR, "ERROR from ZSTD");
//    }
    return ::Utils::fromAdjacencyMatrix(adjMatrix, code.n, false);
}
