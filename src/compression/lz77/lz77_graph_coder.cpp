//
// Created by pawel on 9/12/20.
//

#include "lz77_graph_coder.h"

LZ77::LZ77Graph LZ77::LZ77GraphCoder::encode(const PUNGraph &graph) {
    std::vector<char> adjMatrix = ::Utils::toAdjacencyMatrix(graph);
    std::vector<unsigned char> adjMatrixUnsigned;
    adjMatrixUnsigned.reserve(adjMatrix.size());
    for (auto c : adjMatrix) {
        adjMatrixUnsigned.push_back((unsigned char) c);
    }
    auto encoded = LZ77().encode(adjMatrixUnsigned);

    return {encoded, graph->GetNodes()};
}

const PUNGraph LZ77::LZ77GraphCoder::decode(LZ77Graph &code) {
    auto decoded = LZ77().decode(code.code);
    std::vector<char> adjMatrix;
    adjMatrix.reserve(decoded.size());
    for (auto c : decoded) {
        adjMatrix.push_back((char) c);
    }

    return ::Utils::fromAdjacencyMatrix(adjMatrix, code.n);
}
