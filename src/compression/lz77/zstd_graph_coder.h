//
// Created by pawel on 9/12/20.
//

#ifndef MAGISTERKA_ZSTD_GRAPH_CODER_H
#define MAGISTERKA_ZSTD_GRAPH_CODER_H

#include <compression/compressor.h>
#include <Snap.h>
#include <utils/graph.h>
#include "zstd.h"
#include "loguru.hpp"

namespace LZ77 {
    using Graph::Compression::Compressor;

    struct ZSTDGraph {
        int n;
        std::vector<char> code;
    };

    class ZSTDGraphCoder : public Compressor<const PUNGraph, ZSTDGraph> {
    public:
        ZSTDGraphCoder() = default;

        ZSTDGraph encode(const PUNGraph &graph) override;

        const PUNGraph decode(ZSTDGraph &code) override;
    };
}
#endif //MAGISTERKA_ZSTD_GRAPH_CODER_H
