//
// Created by pawel on 9/12/20.
//


#include "lz77.h"

namespace LZ77 {
    LZ77::LZ77(bool print_dbg_) : print_dbg(print_dbg_) {
        initAll();
    }

    void LZ77::initAll() {
        this->MAX_SEARCH_BUF_SIZE = 2 * 8192;
        this->MAX_LOOKAHEAD_BUF_SIZE = 250;
        this->MIN_MATCHED_CNT = 3;
        this->offset = 0;
        this->searchBuffer = std::deque<unsigned char>();
        this->lookaheadBuffer = std::deque<unsigned char>();
        this->buffer = std::vector<unsigned char>();
        this->codePositions = std::unordered_map<int, std::list<int>>();
    }

// find longest prefix of lookahead buffer in search buffer
    pii LZ77::getLongestMatch() {
        int lookaheadBufPtr = this->MIN_MATCHED_CNT - 1;

        if (lookaheadBufPtr + 1 >= lookaheadBuffer.size()) {
            return pii(0, 0);
        }

        // keep postitions of all matches
        std::list<int> matchedPos;

        int prefixCode;
        if (lookaheadBuffer.size() > 2) {
            prefixCode = (int) lookaheadBuffer[0] * 256 * 256 + lookaheadBuffer[1] * 256 + lookaheadBuffer[2];
        } else if (lookaheadBuffer.size() == 2) {
            prefixCode = (int) lookaheadBuffer[0] * 256 * 256 + lookaheadBuffer[1] * 256;
        } else {
            prefixCode = (int) lookaheadBuffer[0] * 256 * 256;
        }
        // int prefixCode = lookaheadBuffer[0]*256 + lookaheadBuffer[1];

        if (this->codePositions.count(prefixCode) != 0) {
            std::list<int> &codePositionsForPrefix = this->codePositions[prefixCode];

            codePositionsForPrefix.remove_if([this](int pos) { return pos < this->offset; });

            for (int &i : codePositionsForPrefix) {
                if (matchedPos.size() > 16) break;
                matchedPos.push_back(i - this->offset);
            }
        }

        // no matches found, go back
        if (matchedPos.empty()) {
            return pii(0, 0);
        }
        assert(lookaheadBuffer.size() <= this->MAX_LOOKAHEAD_BUF_SIZE);
        while (++lookaheadBufPtr < lookaheadBuffer.size()) {

            int firstMatchedPosition = matchedPos.front();
            // try matching current char with every memoized match in search buffer so far (find all mismatches)
            // we went too far when we tried to match           the letter we're trying to match in search buffer is different
            // possible opt - circularity
            matchedPos.remove_if(
                    [this, lookaheadBufPtr](int pos) {
                        return ((pos + lookaheadBufPtr) >= searchBuffer.size()) ||
                               ((pos + lookaheadBufPtr) < searchBuffer.size() &&
                                searchBuffer[pos + lookaheadBufPtr] != lookaheadBuffer[lookaheadBufPtr]);
                    }
            );

            // we exhausted all matches
            if (matchedPos.empty()) {
                return pii(firstMatchedPosition, lookaheadBufPtr);
            }
        }

        return pii(matchedPos.front(), lookaheadBufPtr);
    }

    void LZ77::shiftWindow(int i) {
        for (; i > 0; --i) {
            unsigned char toShift = this->lookaheadBuffer.front();
            this->lookaheadBuffer.pop_front();
            this->searchBuffer.push_back(toShift);

            if (this->searchBuffer.size() >= 3) {
                int prev = (256 * 256) * (int) searchBuffer[searchBuffer.size() - 3] +
                           256 * (int) searchBuffer[searchBuffer.size() - 2];

                if (this->codePositions.count(prev + toShift) == 0)
                    this->codePositions[prev + toShift] = std::list<int>();
                this->codePositions[prev + toShift].push_back(offset + this->searchBuffer.size() - 3);
            }
            // if(this->searchBuffer.size() >= 2) {
            //     int prev = 256* (int)searchBuffer[searchBuffer.size()-2];

            //     if(this->codePositions.count(prev+toShift) == 0) this->codePositions[prev+toShift] = std::list<int>();
            //     this->codePositions[prev + toShift].push_back(offset+this->searchBuffer.size()-2);
            // }
        }

        while (this->searchBuffer.size() > this->MAX_SEARCH_BUF_SIZE) {
            this->searchBuffer.pop_front();
            ++this->offset;
        }
    }

    void LZ77::outputChar(unsigned char c, std::vector<symbol> &output) const {
//        std::cout << (char) 0 << c;
        LOG_F(1, "CHAR (%c, %c)", 0, c);
        if (print_dbg)
            std::cerr << "(" << 0 << ", " << (int) c << ")";
        output.push_back({0, 0, c});
    }

    void LZ77::outputTriple(int distance, int length, std::vector<symbol> &output) const {
        if (print_dbg)
            std::cerr << "(" << 1 << ", " << distance << ", " << length << ")";
//        std::cout << (unsigned char) ((i & 0xFF00) >> 8) << (unsigned char) ((i & 0x00FF)) << (unsigned char) length;
        output.push_back({1, distance, (unsigned char) length});
    }

    std::vector<symbol> LZ77::encode(std::vector<unsigned char> &toEncode) {
        initAll();
        std::vector<symbol> output;
        if (toEncode.empty()) return output;
        // read file byte by byte
        int processed = 0;
        int max_dist = -1;
        int max_len = -1;
        int singles = 0;

        int idx = 0;
        unsigned int b;
        do {
            while (this->lookaheadBuffer.size() < this->MAX_LOOKAHEAD_BUF_SIZE && (idx < toEncode.size())) {
                // std::cout << (char)b << std::endl;
                this->lookaheadBuffer.push_back(toEncode[idx++]);
            }

            // std::cerr << "Search buff: ";
            // for(char c : this->searchBuffer) {
            //     std::cerr << c;
            // }
            // std::cerr << std::endl;
            // std::cerr << "Lookahead buff: ";
            // for(char c : this->lookaheadBuffer) {
            //     std::cerr << c;
            // }
            // std::cerr << std::endl;

            auto match = this->getLongestMatch();

            // output (1, d, l) and shift window
            if (match.second >= this->MIN_MATCHED_CNT) {
                // std::cerr << "matched pos: " << match.first << " length: " << match.second << std::endl;
                max_dist = std::max(max_dist, match.first);
                max_len = std::max(max_len, match.second);
                this->outputTriple(match.first, match.second, output);
                this->shiftWindow(match.second);
                processed += match.second;
            } else { // output (0, char) and shift
                // std::cerr << "not matched " << this->lookaheadBuffer.front() << std::endl;
                this->outputChar(this->lookaheadBuffer.front(), output);
                this->shiftWindow(1);
                processed++;
                ++singles;
            }
            // if(processed % 10000 == 0) std::cerr << "processed: " << processed << std::endl;

            while (this->lookaheadBuffer.size() < this->MAX_LOOKAHEAD_BUF_SIZE && (idx < toEncode.size())) {
                this->lookaheadBuffer.push_back(toEncode[idx++]);
            }
            if (print_dbg) {
                for (int i = 0; i < searchBuffer.size(); ++i) {
                    std::cerr << (int) searchBuffer[i] << ",";
                }
                std::cerr << " | ";
                for (int i = 0; i < lookaheadBuffer.size(); ++i) {
                    std::cerr << (int) lookaheadBuffer[i] << ",";
                }
                std::cerr << std::endl;
            }
        } while (!this->lookaheadBuffer.empty());
        return output;
    }


    symbol LZ77::decodeSymbol(int &idx, std::vector<symbol> &toDecode) const {
        if (idx == toDecode.size()) {
            return symbol{-1, -1, 0};
        }
        auto b = toDecode[idx++];

        if (b.status == 1) { // we got triple
            if (print_dbg)
                std::cerr << "(" << 1 << ", " << b.dist << ", " << (int) b.cnt << ") ";
            return b;
        } else {
            if (print_dbg)
                std::cerr << "(" << 0 << ", " << b.cnt << ") ";
            return b;
        }
    }

    std::vector<unsigned char> LZ77::decode(std::vector<symbol> &toDecode) {
        initAll();
        bool processing = true;
        std::vector<unsigned char> decoded;
        if (toDecode.empty()) return decoded;
        int idx = 0;

        while (processing) {
            auto currentSymbol = this->decodeSymbol(idx, toDecode);
            // std::cerr << "Search buff: ";
            // for(char c : this->searchBuffer) {
            // std::cerr << c;
            // }
            // if(currentSymbol.status == 1 && currentSymbol.cnt == 4) {
            // std::cerr << "decoded symbol: " << currentSymbol.status << " " << (unsigned short)currentSymbol.dist << " " << (int)currentSymbol.cnt << std::endl;

            // }
            if (currentSymbol.status == -1) break;
            else if (currentSymbol.status == 0) {
                this->searchBuffer.push_back(currentSymbol.cnt);
                if (this->searchBuffer.size() > this->MAX_SEARCH_BUF_SIZE) this->searchBuffer.pop_front();

//                std::cerr << currentSymbol.cnt;
                decoded.push_back(currentSymbol.cnt);
            } else {
                std::vector<char> decodedPhrase;
                for (int i = 0; i < currentSymbol.cnt; ++i) {
                    // std::cerr << "decoded symbol: " << currentSymbol.status << " " << (unsigned short)currentSymbol.dist << " " << (int)currentSymbol.cnt << std::endl;
                    char decodedChar = this->searchBuffer[int((unsigned short) currentSymbol.dist) + i];
//                    std::cerr << decodedChar;
                    decoded.push_back(decodedChar);
                    decodedPhrase.push_back(decodedChar);
                }

                for (char c : decodedPhrase) {
                    this->searchBuffer.push_back(c);
                }
                while (this->searchBuffer.size() > this->MAX_SEARCH_BUF_SIZE) this->searchBuffer.pop_front();
            }
            if (print_dbg) {
                for (unsigned char i : searchBuffer) {
                    std::cerr << (int) i << ",";
                }
                std::cerr << " | ";
                for (unsigned char i : lookaheadBuffer) {
                    std::cerr << (int) i << ",";
                }
                std::cerr << std::endl;
            }
        }
        return decoded;
    }

    int LZ77::get_encoded_bits(const std::vector<symbol> &code) {
        int bits = 0;
        for (auto &x : code) {
            bits += 1;
            if (x.status == 1) {
                bits += 24;
            } else {
                bits += 8;
            }
        }
        return bits;
    }
}
