//
// Created by pawel on 9/10/20.
//

#ifndef MAGISTERKA_LZ77_H
#define MAGISTERKA_LZ77_H

#include <deque>
#include <vector>
#include <cstdio>
#include <iostream>
#include <list>
#include <unordered_map>
#include <loguru.hpp>
#include <cassert>
#include "utils/typedefs.h"
#include "compression/compressor.h"

namespace LZ77 {
    using Graph::Compression::Compressor;
    typedef unsigned long long ull;
    struct symbol {
        int status;
        int dist;
        unsigned char cnt;
    };

    class LZ77 : public Compressor<std::vector<unsigned char>, std::vector<symbol>> {
    private:
        int MAX_SEARCH_BUF_SIZE;
        int MAX_LOOKAHEAD_BUF_SIZE;
        int MIN_MATCHED_CNT;
        int offset;
        bool print_dbg;
        std::deque<unsigned char> searchBuffer;
        std::deque<unsigned char> lookaheadBuffer;
        std::vector<unsigned char> buffer;

        pii getLongestMatch();

        void outputTriple(int distance, int length, std::vector<symbol> &output) const;

        void outputChar(unsigned char c, std::vector<symbol> &output) const;

        void shiftWindow(int i);

        symbol decodeSymbol(int &idx, std::vector<symbol> &toDecode) const;

        std::unordered_map<int, std::list<int>> codePositions; // for every char keep the list of positions

        void initAll();

    public:
        LZ77(bool print_dbg_ = false);

        std::vector<symbol> encode(std::vector<unsigned char> &toEncode) override;

        std::vector<unsigned char> decode(std::vector<symbol> &toDecode) override;

        static int get_encoded_bits(const std::vector<symbol> &code);
    };

}
#endif //MAGISTERKA_LZ77_H