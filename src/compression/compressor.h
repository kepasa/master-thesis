//
// Created by pawel on 5/9/20.
//

#ifndef MAGISTERKA_COMPRESSOR_H
#define MAGISTERKA_COMPRESSOR_H

#include <vector>

namespace Graph::Compression {
    struct CompressedGraph {
        int n;
        double p;
        int zerosB1;
        int onesB1;
        std::vector<char> B1;
        std::vector<char> B2;
    };

    template<typename T, typename U>
    class Compressor {
    public:
        // TODO(pawlep): think about using vector<bool> for space efficiency
        virtual U encode(T& toEncode) = 0;
        virtual T decode(U& toDecode) = 0;
    };
}

#endif //MAGISTERKA_COMPRESSOR_H
