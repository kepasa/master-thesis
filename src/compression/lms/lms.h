//
// Created by pawel on 7/23/20.
//

#ifndef MAGISTERKA_LMS_H
#define MAGISTERKA_LMS_H

#include <Snap.h>
#include <queue>
#include <tuple>
#include <utils/binaryops.h>
#include <stack>
#include <iostream>
#include <utils/graph.h>
#include <unordered_set>
#include <compression/huff_coder/huff_coder.h>
#include "compression/compressor.h"
#include "utils/typedefs.h"
#include "loguru.hpp"

namespace Graph::Compression {
    using HuffmanCoding::HuffmanCoder;
    using HuffmanCoding::HuffmanCode;

    struct LMSGraph {
        int n;
        int k;
        HuffmanCode Bj;
        std::vector<char> graphCode;
        std::vector<char> isStarting;
    };

    class LMS : public Compressor<const PNEGraph, LMSGraph> {
    private:
        int startingNode;
        const bool printDbg = false;
        std::vector<char> visited;
        std::vector<int> discoveryTime;
        std::vector<int> backtrackNumber;

        PNEGraph constructDmin(PNEGraph &graph) {
            LOG_SCOPE_FUNCTION(1);
            auto result = PNEGraph::New();
            std::priority_queue<pii, std::vector<pii>, std::greater<>> Q;
            for (auto nodeI = graph->BegNI(); nodeI < graph->EndNI(); nodeI++) {
                Q.push(pii{nodeI.GetOutDeg(), nodeI.GetId()});
                result->AddNode(nodeI.GetId());
            }

            int processedNodes = 0;
            while (processedNodes < graph->GetNodes()) {
                std::queue<pii> currentLayer;
                std::unordered_set<int> currentLayerIds;
                pii currentNode;

                do {
                    currentNode = Q.top();
                    Q.pop();
                    currentLayer.push(currentNode);
                } while (!Q.empty() && !visited[currentNode.second]);

                while (!Q.empty() && Q.top().first == currentNode.first) {

                    auto currentElement = Q.top();
                    if (!visited[currentElement.second]) {
                        currentLayer.push(currentElement);
                        currentLayerIds.insert(currentElement.second);
                    }
                    Q.pop();
                }

                while (!currentLayer.empty()) {
                    currentNode = currentLayer.front();
                    currentLayer.pop();
                    auto currentNodeI = graph->GetNI(currentNode.second);

                    // orient edges correctly
                    std::queue<TNEGraph::TEdgeI> edgesToProcess;

                    for (int i = 0; i < currentNodeI.GetOutDeg(); ++i) {
                        auto edgeId = currentNodeI.GetOutEId(i);
                        auto edgeIt = graph->GetEI(edgeId);
                        edgesToProcess.push(edgeIt);
                    }

                    while (!edgesToProcess.empty()) {
                        auto edgeIt = edgesToProcess.front();
                        edgesToProcess.pop();
                        auto dstV = edgeIt.GetDstNId();
                        auto dstVI = graph->GetNI(dstV);
                        if (dstV == currentNode.second) continue; // remove self-loops

                        graph->DelEdge(edgeIt.GetId());
                        int reverseEdgeId;
                        assert(graph->IsEdge(dstV, currentNode.second, reverseEdgeId));
                        graph->DelEdge(reverseEdgeId);

                        if (currentLayerIds.count(dstV) == 1) {
                            // both nodes are in current layer, orient edge from larger to smaller
                            result->AddEdge(std::max(currentNode.second, dstV), std::min(currentNode.second, dstV));
                        } else {
                            // nodes are in different layers, orient edge away from current node
                            result->AddEdge(currentNode.second, dstV);
                            VLOG_F(1, "Adding edge between layers: (%d, %d). Dst node out degree is now: %d",
                                   currentNode.second, dstV, graph->GetNI(dstV).GetOutDeg());
                            Q.push(pii{int(graph->GetNI(dstV).GetOutDeg()), dstV});
                        }
                    }
                    visited[currentNode.second] = 1;

                    ++processedNodes;
                }
            }
            return result;
        }

        void initializeArrays(const PNEGraph &graph) {
            this->visited.resize(graph->GetNodes());
            this->discoveryTime.resize(graph->GetNodes());
            this->backtrackNumber.resize(graph->GetNodes());
            for (int i = 0; i < graph->GetNodes(); ++i) {
                this->backtrackNumber[i] = 0;
            }
        }

        void cleanVisited(const PNEGraph &graph) {
            for (int i = 0; i < graph->GetNodes(); ++i) {
                this->visited[i] = 0;
            }
        }

        // Returns maximum out-degree, turning back vertex (it's discovery time) and distance from it
        std::tuple<int, int, int>
        preprocessDfs(const PNEGraph &graph, int currentNode, int &currentTime, int backtrackDist, bool isStartingNode,
                      std::vector<char> &isStarting) {
            this->visited[currentNode] = 1;
            this->discoveryTime[currentNode] = currentTime++;
            isStarting[this->discoveryTime[currentNode]] = isStartingNode;

            auto nodeI = graph->GetNI(currentNode);
            bool unvisitedNeigh = false;
            int maxDeg = nodeI.GetOutDeg();
            std::unordered_set<int> distinct_nodes;
            std::unordered_set<int> processed_nodes;
            for (int i = 0; i < nodeI.GetInDeg(); ++i) {
                auto edgeId = nodeI.GetInEId(i);
                auto edge = graph->GetEI(edgeId);
                auto srcNId = edge.GetSrcNId();
                if (!visited[srcNId]) {
                    distinct_nodes.insert(srcNId);
                }
            }
            int cnt = 0;
            // holds ID of turning back vertex and distance from it.
            std::pair<int, int> turnbackVrtx = {-1, -1};
            for (auto i = 0; i < nodeI.GetInDeg(); ++i) {
                auto edgeId = nodeI.GetInEId(i);
                auto edge = graph->GetEI(edgeId);
                auto srcNId = edge.GetSrcNId();
                if (processed_nodes.count(srcNId) == 1 || visited[srcNId]) continue;

                int backtrackD;
                if (cnt == distinct_nodes.size() - 1) {
                    backtrackD = backtrackDist + 1;
                } else {
                    backtrackD = 1;
                }
                unvisitedNeigh = true;

                if (turnbackVrtx.first != -1) {
                    // we received some information that some vertex backtracked to `currentVertex` taking path from
                    // some other neighbour of `currentVertex`. Therefore, we can safely set backtrackNumber for it here
                    backtrackNumber[discoveryTime[turnbackVrtx.first]] = turnbackVrtx.second;

                }

                auto res = preprocessDfs(graph, srcNId, currentTime, backtrackD, false, isStarting);
                maxDeg = std::max(maxDeg, std::get<0>(res));
                turnbackVrtx = {std::get<1>(res), std::get<2>(res)};

                ++cnt;
                processed_nodes.insert(srcNId);
            }

//            if (!unvisitedNeigh) {
//                backtrackNumber[discoveryTime[currentNode]] = backtrackDist;
//            } else {
//                backtrackNumber[discoveryTime[currentNode]] = 0;
//            }

            if (isStartingNode && turnbackVrtx.first != -1) {
                // This is a starting vertex. We set backtrack number for the last branch
                backtrackNumber[discoveryTime[turnbackVrtx.first]] = turnbackVrtx.second;
            }


            if (turnbackVrtx.first == -1) {
                // We didn't go into any neighbour. We're turning back point.
                turnbackVrtx.first = currentNode;
                turnbackVrtx.second = 0;
            }


            return {maxDeg, turnbackVrtx.first, turnbackVrtx.second + 1};
        }

        void encodingDfs(const PNEGraph &graph,
                         const int previousNode,
                         const int currentNode,
                         std::vector<char> &code,
                         const int maxDeg) {
            this->visited[currentNode] = true;

            auto nodeI = graph->GetNI(currentNode);

            if (previousNode != -1 && maxDeg > 1) {
                int bitCnt = ::Utils::log2i_ceil(maxDeg);
                auto bitsToEncode = ::Utils::extractBits(nodeI.GetOutDeg() - 1, 0, bitCnt);
                VLOG_F(1, "Encoding node: %d, deg: %d, discTime: %d, Bj: %d, previous: %d, previousDiscTime :%d",
                       currentNode, nodeI.GetOutDeg(), discoveryTime[currentNode],
                       backtrackNumber[discoveryTime[currentNode]],
                       previousNode, discoveryTime[previousNode]);
                code.insert(code.end(), bitsToEncode.begin(), bitsToEncode.end());

                bool skippedPrevious = false;
                bitCnt = ::Utils::log2i_ceil(graph->GetNodes());
                std::string dbgS = "";
                for (int i = 0; i < nodeI.GetOutDeg(); ++i) {
                    auto edgeId = nodeI.GetOutEId(i);
                    auto edge = graph->GetEI(edgeId);
                    auto dstNId = edge.GetDstNId();

                    if ((dstNId != previousNode) || skippedPrevious) {
                        dbgS += " [";
                        dbgS += std::to_string(i);
                        dbgS += "]: ";
                        dbgS += std::to_string(discoveryTime[dstNId]);
                        dbgS += ",";
                        auto encodedName = ::Utils::extractBits(discoveryTime[dstNId], 0, bitCnt);
                        code.insert(code.end(), encodedName.begin(), encodedName.end());
                    } else if (dstNId == previousNode) {
                        skippedPrevious = true;
                    }
                }
                VLOG_F(1, dbgS.c_str());
            }

            for (int i = 0; i < nodeI.GetInDeg(); ++i) {
                auto edgeId = nodeI.GetInEId(i);
                auto edge = graph->GetEI(edgeId);
                auto srcNId = edge.GetSrcNId();

                if (!visited[srcNId]) {
                    encodingDfs(graph, currentNode, srcNId, code, maxDeg);
                }
            }
        }


    public:

        // assume that we're compressing only preferential attachment graphs with loop for now
        LMS(int startingNode_, bool printDbg_ = false) : startingNode(startingNode_), printDbg(printDbg_) {

        }

        // We assume that toEncode is an undirected multigraph containing no self-loops
        // General note - should dfses operate on nodes or on discoveryTimes? It's critical that the sequence of
        // nodes is the same for both cases.
        LMSGraph encode(const PNEGraph &toEncode) override {
            initializeArrays(toEncode);
            cleanVisited(toEncode);
            auto workingCopy = ::Utils::copyGraph(toEncode);
            auto Dmin = constructDmin(workingCopy);

            cleanVisited(Dmin);
            int currentTime = 0;
            std::vector<int> startingNodes;

            if (startingNode != -1) {
                startingNodes.push_back(startingNode);
            }
            for (auto nodeit = Dmin->BegNI(); nodeit < Dmin->EndNI(); nodeit++) {
                if (nodeit.GetOutDeg() == 0) {
                    VLOG_F(1, "Got bottom: %d", nodeit.GetId());
                    // We have explicitly provided a starting node for the process and it's already added.
                    if (startingNode != -1 && nodeit.GetId() != startingNode) {
                        startingNodes.push_back(nodeit.GetId());
                    } else if (startingNode == -1) {
                        startingNodes.push_back(nodeit.GetId());
                    }
                }
            }

            int maxDeg = 0;
            std::vector<char> isStarting(Dmin->GetNodes());
            for (int i : startingNodes) {
                auto res = preprocessDfs(Dmin, i, currentTime, 0, true, isStarting);
                maxDeg =
                        std::max(maxDeg, std::get<0>(res));
            }
            cleanVisited(Dmin);

            if (printDbg) {
                TIntStrH name;
                for (auto i = Dmin->BegNI(); i < Dmin->EndNI(); i++) {
                    name.AddDat(i.GetId()) = (std::to_string(i.GetId()) + " - " +
                                              std::to_string(discoveryTime[i.GetId()])).data();
                }
                TSnap::DrawGViz<PNEGraph>(Dmin, gvlDot, "LMS_DMin.png", "", name);
            }

            VLOG_F(1, "Max deg: %d", maxDeg);
            auto codeBuff = std::vector<char>();
//            encodingDfs(Dmin, -1, this->startingNode, codeBuff, maxDeg);
            for (int i : startingNodes) {
                encodingDfs(Dmin, -1, i, codeBuff, maxDeg);
            }

            auto huffCoder = HuffmanCoder();
            return LMSGraph{
                    toEncode->GetNodes(),
                    maxDeg,
                    huffCoder.encode(backtrackNumber),
                    codeBuff,
                    isStarting};
        }

        const PNEGraph decode(LMSGraph &toDecode) override {
            auto result = PNEGraph::New();
            for (int i = 0; i < toDecode.n; ++i) {
                result->AddNode(i);
            }

            if (toDecode.k == 0) return result;

            auto Bj = HuffmanCoder().decode(toDecode.Bj);

            // Add self-loops
//            for (int i = 0; i < toDecode.k; ++i) {
//                result->AddEdge(0, 0);
//            }

            std::stack<int> processedVertices;
            processedVertices.push(0);
            processedVertices.push(1);

            const int logCeilMaxDeg = toDecode.k > 1 ? ::Utils::log2i_ceil(toDecode.k) : 0;
            const int logCeilGraphSize = ::Utils::log2i_ceil(toDecode.n);

            int graphCodeIt = 0;
            while (processedVertices.size() > 1) {
                int currentVertex = processedVertices.top();
                if (currentVertex == toDecode.n) break;
                if (toDecode.isStarting[currentVertex]) {
                    processedVertices.push(currentVertex + 1);
                    continue;
                }
                processedVertices.pop();
                int previousVertex = processedVertices.top();
                processedVertices.push(currentVertex);
                VLOG_F(1, "Current vertex: %d, Previous vertes: %d", currentVertex, previousVertex);
                result->AddEdge(currentVertex, previousVertex);
                result->AddEdge(previousVertex, currentVertex);

                int vDeg = ::Utils::decodeBits(toDecode.graphCode.begin() + graphCodeIt,
                                               toDecode.graphCode.begin() + graphCodeIt +
                                               logCeilMaxDeg);
                VLOG_F(1, "Adding %d vertices to vertex %d, Bj: %d, previous: %d", vDeg, currentVertex,
                       Bj[currentVertex], previousVertex);

                graphCodeIt += logCeilMaxDeg;
                std::string dbgS = "";
                for (int i = 0; i < vDeg; ++i) {
                    int vertexName = ::Utils::decodeBits(toDecode.graphCode.begin() + graphCodeIt,
                                                         toDecode.graphCode.begin() + graphCodeIt + logCeilGraphSize);
                    dbgS += " [";
                    dbgS += std::to_string(i);
                    dbgS += "]: ";
                    dbgS += std::to_string(vertexName);
                    dbgS += ",";
                    graphCodeIt += logCeilGraphSize;
                    result->AddEdge(currentVertex, vertexName);
                    result->AddEdge(vertexName, currentVertex);
                }
                VLOG_F(1, dbgS.c_str());
                for (int i = 0; i < Bj[currentVertex]; ++i) {
                    processedVertices.pop();
                }
                processedVertices.push(currentVertex + 1);
            }


            return result;
        }

    };

}
#endif //MAGISTERKA_LMS_H
