//
// Created by pawel on 9/12/20.
//

#include "graph_io.h"

PUNGraph IO::read_csv_graph(const std::string &filepath) {
    std::ifstream graph_file(filepath);
    if (graph_file.fail()) {
        throw std::invalid_argument("Invalid graph path: " + filepath);
    }
    PUNGraph result = PUNGraph::New();
    std::string useless;
    char delim;
    graph_file >> useless >> delim >> useless;

    int u, v;
    while (graph_file >> u >> delim >> v) {
        if (u == v && !result->IsNode(u)) {
            result->AddNode(u);
            continue;
        }
        if (!result->IsNode(u)) {
            result->AddNode(u);
        }
        if (!result->IsNode(v)) {
            result->AddNode(v);
        }
        result->AddEdge(u, v);
        result->AddEdge(v, u);
    }
    return TSnap::ConvertGraph<PUNGraph>(result, true);
}

PUNGraph IO::read_graph(const std::string &filepath, bool useSnap) {
    if (useSnap) {
        auto result = TSnap::ConvertGraph<PUNGraph>(
                TSnap::LoadEdgeList<PUNGraph>(filepath.c_str()),
                true
        );
        return result;
    }
    std::ifstream graph_file(filepath);
    if (graph_file.fail()) {
        throw std::invalid_argument("Invalid graph path: " + filepath);
    }
    PUNGraph result = PUNGraph::New();

    int u, v;
    while (graph_file >> u >> v) {
        if (u == v && !result->IsNode(u)) {
            result->AddNode(u);
            continue;
        }
        if (!result->IsNode(u)) {
            result->AddNode(u);
        }
        if (!result->IsNode(v)) {
            result->AddNode(v);
        }
        result->AddEdge(u, v);
    }
    return result;
}


PNEGraph IO::read_dir_graph(const std::string &filepath) {
    return TSnap::ConvertGraph<PNEGraph>(
            TSnap::LoadEdgeList<PNEGraph>(filepath.c_str()),
            true
    );
}

void IO::write_dir_graph(const std::string &filepath, const PNEGraph &graph) {
    TSnap::SaveEdgeList(graph, filepath.c_str());
}

void IO::write_graph(const std::string &filepath, const PUNGraph &graph) {
    TSnap::SaveEdgeList(graph, filepath.c_str());
}

void IO::write_latex_table(const std::string &filepath, IO::LatexTable &lt) {
    std::ofstream outputFile(filepath, std::ofstream::out | std::ofstream::trunc);

    outputFile << " \\begin{tabular}{||c c c c||} \n";
    outputFile << " \\hline \n";

    outputFile << lt.headers[0];
    for (int i = 1; i < lt.headers.size(); ++i) {
        outputFile << " & " << lt.headers[i];
    }
    outputFile << " \\\\ [0.5ex]\n";

    outputFile << " \\hline\\hline\n";

    for (int j = 0; j < lt.rows.size(); ++j) {
        const auto &row = lt.rows[j];
        for (int i = 0; i < row.size() - 1; ++i) {
            outputFile << row[i] << " & ";
        }
        outputFile << row[row.size() - 1] << " \\\\ ";
        if (j == lt.rows.size() - 1) {
            outputFile << "[1ex]";
        }
        outputFile << "\n";
    }

    outputFile << " \\hline \n";
    outputFile << " \\end{tabular}";

    outputFile.close();
}

