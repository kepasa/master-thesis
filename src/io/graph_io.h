//
// Created by pawel on 9/12/20.
//

#ifndef MAGISTERKA_GRAPH_IO_H
#define MAGISTERKA_GRAPH_IO_H

#include <fstream>
#include <vector>
#include "Snap.h"

namespace IO {
    PUNGraph read_graph(const std::string &filepath, bool useSnap = false);

    PNEGraph read_dir_graph(const std::string &filepath);


    struct LatexTable {
        std::vector<std::string> headers;
        std::vector<std::vector<std::string>> rows;
    };

    void write_latex_table(const std::string &filepath, LatexTable &lt);

    void write_graph(const std::string &filepath, const PUNGraph &graph);

    void write_dir_graph(const std::string &filepath, const PNEGraph &graph);

    PUNGraph read_csv_graph(const std::string &filepath);
}

#endif //MAGISTERKA_GRAPH_IO_H
