//
// Created by pawel on 5/6/20.
//

#ifndef MAGISTERKA_GENERATORS_H
#define MAGISTERKA_GENERATORS_H

#include "Snap.h"

namespace Graph::Generators {
    // Generates undirected Erdos-Renyi graph
    PUNGraph GenerateER(int n, int m);

    // Generates undirected Erdos-Renyi graph given edge probability p
    PUNGraph GenerateER(int n, double p);

    // Generates preferential-attachment graph (with loops) as multigraph
    PNEGraph GeneratePE(int n, int m, bool removeLoops = false);

    // Generates undirected duplication-divergence graph
    PUNGraph GenerateDD(int n, double p, double r, const PUNGraph &G0);
}

#endif //MAGISTERKA_GENERATORS_H
