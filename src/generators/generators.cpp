//
// Created by pawel on 5/6/20.
//

#include <vector>
#include <random>
#include <utils/graph.h>
#include "generators.h"
#include "Snap.h"

namespace Graph::Generators {
    // Generates undirected Erdos-Renyi graph
    PUNGraph GenerateER(int n, int m) {
        return TSnap::GenRndGnm<PUNGraph>(n, m, false);
    }

    // Generates undirected Erdos-Renyi graph given edge probability p
    PUNGraph GenerateER(int n, double p) {
        std::random_device rd;
        std::uniform_real_distribution<double> dist(0.0, 1.0);

        auto result = PUNGraph::New();

        for (int i = 0; i < n; ++i) {
            result->AddNode(i);
        }

        for (int i = 0; i < n - 1; ++i) {
            for (int j = i + 1; j < n; ++j) {
                if (i == j) continue;

                if (dist(rd) <= p) {
                    result->AddEdge(i, j);
                    result->AddEdge(j, i);
                }
            }
        }

        return result;
    }

    PNEGraph GeneratePE(int n, int m, bool removeLoops) {
        auto result = PNEGraph::New();
        for (int i = 0; i < n; ++i) {
            result->AddNode(i);
        }

        //G1
        for (int i = 0; i < m; ++i) {
            result->AddEdge(0, 0);
        }

        // TODO(pawelp): use fenwick tree for calculating cumulative freqs
        std::vector<int> degrees;
        std::vector<int> newDegrees;
        degrees.push_back(2 * m);
        newDegrees.push_back(2 * m);

        std::random_device rd;
        // Gi+1
        for (int i = 1; i < n; ++i) {
            newDegrees.push_back(m);
            std::uniform_int_distribution<int> dist(0, 2 * m * (i - 1));
            for (int j = 0; j < m; ++j) {
                auto rand_val = dist(rd);
                int sum = 0; // todo - replace with long long
                int selectedNode;
                for (selectedNode = 0; selectedNode < i; ++selectedNode) {
                    sum += degrees[selectedNode];
                    if (sum > rand_val) break;
                }

                result->AddEdge(i, selectedNode);
                result->AddEdge(selectedNode, i);
                newDegrees[selectedNode]++;
            }

            degrees = newDegrees;
        }

        if (removeLoops) {
            while (true) {
                auto selfloopEI = result->GetEId(0, 0);
                if (selfloopEI == -1) break;
                result->DelEdge(selfloopEI);
            }
        }

        return result;
    }


    // Assume that G0 vertices are numbered 0..{n_0}-1
    PUNGraph GenerateDD(int n, double p, double r, const PUNGraph &G0) {
        auto Gk = ::Utils::copyGraph(G0);

        std::random_device rd;
        std::uniform_real_distribution<double> edgeDistr(0.0, 1.0);

        int n0 = Gk->GetNodes();
        for (int i = n0; i < n; ++i) {
            std::uniform_int_distribution<int> dupNodeDistr(0, i - 1);
            int dupNodeId = dupNodeDistr(rd);

            Gk->AddNode(i);
            for (int j = 0; j < i; ++j) {
                if (Gk->IsEdge(dupNodeId, j) && edgeDistr(rd) <= p) {
                    Gk->AddEdge(i, j);
                    Gk->AddEdge(j, i);
                } else if (edgeDistr(rd) <= r / i) {
                    Gk->AddEdge(i, j);
                    Gk->AddEdge(j, i);
                }
            }
        }
        return Gk;
    }
}