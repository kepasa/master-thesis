# Master's Thesis

This project has following external dependencies:

* libzstd
* gnuplot
* GraphViz

make sure you have both gnuplot and GraphViz installed and on `$PATH` variable, as well as libzstd installed.

In order to compile the project run `./build.sh` (<mark>Warning! this takes a while</mark>) in project main directory. This will create a directory `release_build`
 which will now become "root" directory. Everything command from now on assumes that you are inside `release_build` directory.
 
 The main executable is called `Magisterka`. You can control it using following command line parameters:
 
 * program parameter `-p` which assumes values
    * `generate`
    * `compress`
    * `benchmark`
    
 * control parameter which controls given program
    * for benchmark `-b [benchName]` e.g. `-b paSmall`
    * for compress `-c [graphPath],[compressor] {--isoCheck}` where graph path is 
    relative path where graph to be compressed is located and `--isoCheck` is optional (it uses Nauty to check whether
    graph at input and graph after decompression are isomorphic). 
    * for generate `-g [ER,PA,DD],{parameters}` where ER,PA,DD are random graph models and parameters
    is a list (delimited with commas) of parameters for a given model
    
examples for running the program:

`./Magisterka -p generate -g ER,10,0.5` - generates a Erdos-Renyi random graph on 10 vertices with `p=0.5` and
saves it to `files/generated_Graphs/ER_10_22.txt`

`./Magisterka -p compress -c files/generated_graphs/ER_10_22.txt,szip --isoCheck` compresses the graph at given path using
SzipE algorithm and checks whether decompressed graph is isomorphic

`./Magisterka -p benchmark -b paSmall` runs benchmark for small preferential-attachment graphs and
saves output to `files/output`
