//
// Created by pawel on 6/13/20.
//
#include <generators/generators.h>
#include <utils/graph.h>
#include "gtest/gtest.h"

using Graph::Generators::GenerateER;
using Utils::checkIsomorphic;
using Utils::checkIsomorphic_V2;
using Utils::toAdjacencyMatrix;
using Utils::fromAdjacencyMatrix;

typedef std::pair<PUNGraph, PUNGraph> pgg;

pgg getSmallIsoGraphs() {
    auto g1 = PUNGraph::New();
    auto g2 = PUNGraph::New();
    g1->AddNode(0);
    g1->AddNode(1);
    g1->AddNode(2);
    g2->AddNode(0);
    g2->AddNode(1);
    g2->AddNode(2);

    g1->AddEdge(0, 2);
    g1->AddEdge(2, 0);

    g2->AddEdge(1, 2);
    g2->AddEdge(2, 1);

    return {g1, g2};
}

pgg getSmallDifferentGraphs() {
    auto g1 = PUNGraph::New();
    auto g2 = PUNGraph::New();
    g1->AddNode(0);
    g1->AddNode(1);
    g1->AddNode(2);
    g1->AddNode(3);
    g2->AddNode(0);
    g2->AddNode(1);
    g2->AddNode(2);
    g2->AddNode(3);

    g1->AddEdge(0, 1);
    g1->AddEdge(1, 0);

    g1->AddEdge(1, 2);
    g1->AddEdge(2, 1);

    g1->AddEdge(2, 3);
    g1->AddEdge(3, 2);

    g1->AddEdge(3, 0);
    g1->AddEdge(0, 3);


    g2->AddEdge(0, 1);
    g2->AddEdge(1, 0);

    g2->AddEdge(1, 2);
    g2->AddEdge(2, 1);

    g2->AddEdge(2, 0);
    g2->AddEdge(0, 2);

    g2->AddEdge(0, 3);
    g2->AddEdge(3, 0);

    return {g1, g2};
}

TEST(CheckIsomorphic, TrueForEqualGraphs) {
    auto graph1 = GenerateER(8, 15);
    ASSERT_TRUE(checkIsomorphic(graph1, graph1));
}

TEST(CheckIsomorphic, FalseForUnequalNoNodes) {
    auto graph1 = GenerateER(8, 15);
    auto graph2 = GenerateER(7, 10);
    ASSERT_FALSE(checkIsomorphic(graph1, graph2));
}

TEST(CheckIsomorphic, FalseForUnequalNoEdges) {
    auto graph1 = GenerateER(8, 15);
    auto graph2 = GenerateER(8, 14);
    ASSERT_FALSE(checkIsomorphic(graph1, graph2));
}

TEST(CheckIsomorphic, FalseForDifferentGraphs) {
    auto graphs = getSmallDifferentGraphs();
    ASSERT_FALSE(checkIsomorphic(graphs.first, graphs.second));
}

TEST(CheckIsomorphic_V2, TrueForEqualGraphs2) {
    pgg graphs = getSmallIsoGraphs();
    ASSERT_TRUE(checkIsomorphic_V2(graphs.first, graphs.second));
}

TEST(CheckIsomorphic_V2, TrueForEqualGraphs) {
    auto graph1 = GenerateER(8, 15);
    ASSERT_TRUE(checkIsomorphic_V2(graph1, graph1));
}

TEST(CheckIsomorphic_V2, FalseForUnequalNoNodes) {
    auto graph1 = GenerateER(8, 15);
    auto graph2 = GenerateER(7, 10);
    ASSERT_FALSE(checkIsomorphic_V2(graph1, graph2));
}

TEST(CheckIsomorphic_V2, FalseForUnequalNoEdges) {
    auto graph1 = GenerateER(8, 15);
    auto graph2 = GenerateER(8, 14);
    ASSERT_FALSE(checkIsomorphic_V2(graph1, graph2));
}

TEST(CheckIsomorphic_V2, FalseForDifferentGraphs) {
    auto graphs = getSmallDifferentGraphs();
    ASSERT_FALSE(checkIsomorphic_V2(graphs.first, graphs.second));
}

TEST(AdjMatrixConversion, ConvertsCorrectly_SmallGraphs) {
    const auto& isIsomorphic = checkIsomorphic_V2;

    for (int noNodes = 1; noNodes < 10; ++noNodes) {

        for (int noEdges = 0; noEdges <= ((noNodes * (noNodes - 1)) / 2); ++noEdges) {
            auto graph = GenerateER(noNodes, noEdges);
            auto adjMatrix = toAdjacencyMatrix(graph);
            auto decodedGraph = fromAdjacencyMatrix(adjMatrix, graph->GetNodes());
            for(int i = 0; i < noNodes; ++i) {
                for(int j = 0; j < noNodes; ++j) {
                    if(i == j) continue;
                    ASSERT_TRUE(graph->IsEdge(i, j) == decodedGraph->IsEdge(i ,j));
                }
            }
        }
    }
}

TEST(AdjMatrixConversion, ConvertsCorrectly_MediumGraphs) {
    const auto& isIsomorphic = checkIsomorphic_V2;

    for (int noNodes = 10; noNodes < 17; ++noNodes) {

        for (int noEdges = ((noNodes * (noNodes - 1)) / 8); noEdges <= ((noNodes * (noNodes - 1)) / 4); ++noEdges) {
            auto graph = GenerateER(noNodes, noEdges);
            auto adjMatrix = toAdjacencyMatrix(graph);
            auto decodedGraph = fromAdjacencyMatrix(adjMatrix, graph->GetNodes());
            for(int i = 0; i < noNodes; ++i) {
                for(int j = 0; j < noNodes; ++j) {
                    if(i == j) continue;
                    ASSERT_TRUE(graph->IsEdge(i, j) == decodedGraph->IsEdge(i ,j));
                }
            }
        }
    }
}