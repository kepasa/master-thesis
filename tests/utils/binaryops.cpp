//
// Created by pawel on 5/11/20.
//
#include "gtest/gtest.h"
#include "gmock/gmock-matchers.h"
#include "utils/binaryops.h"

using ::testing::ElementsAre;

TEST(TestExtractBits, ExtractsBitsCorrectly) {
    ASSERT_THAT(Utils::extractBits(3, 0, 4), ElementsAre(0, 0, 1, 1));
}

TEST(TestDecodeBits, DecodesBitsCorrectly) {
    std::vector<char> testvec{1, 0, 1, 0, 1, 1};
    ASSERT_EQ(Utils::decodeBits(testvec.begin(), testvec.end()), 43);
}