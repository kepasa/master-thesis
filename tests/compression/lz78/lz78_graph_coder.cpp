//
// Created by pawel on 9/11/20.
//

#include <generators/generators.h>
#include <compression/lz78/lz78_graph_coder.h>
#include "gtest/gtest.h"
#include "benchmarks/nauty_toolkit.h"

using LZ78::LZ78GraphCoder;

TEST(LZ78GraphCoder, EncodesCorrectly_SmallGraphs) {
    auto coder = LZ78GraphCoder();
    for (int noNodes = 1; noNodes < 10; ++noNodes) {

        for (int noEdges = 0; noEdges <= ((noNodes * (noNodes - 1)) / 2); ++noEdges) {
            auto graph = Graph::Generators::GenerateER(noNodes, noEdges);
            auto encoded = coder.encode(graph);
            auto decodedGraph = coder.decode(encoded);
            for (int i = 0; i < noNodes; ++i) {
                for (int j = 0; j < noNodes; ++j) {
                    if (i == j) continue;
                    ASSERT_TRUE(graph->IsEdge(i, j) == decodedGraph->IsEdge(i, j));
                }
            }
        }
    }
}

TEST(LZ78GraphCoder, ConvertsCorrectly_MediumGraphs) {
    auto coder = LZ78GraphCoder();

    for (int noNodes = 10; noNodes < 17; ++noNodes) {

        for (int noEdges = ((noNodes * (noNodes - 1)) / 8); noEdges <= ((noNodes * (noNodes - 1)) / 4); ++noEdges) {
            auto graph = Graph::Generators::GenerateER(noNodes, noEdges);
            auto encoded = coder.encode(graph);
            auto decodedGraph = coder.decode(encoded);
            for (int i = 0; i < noNodes; ++i) {
                for (int j = 0; j < noNodes; ++j) {
                    if (i == j) continue;
                    ASSERT_TRUE(graph->IsEdge(i, j) == decodedGraph->IsEdge(i, j));
                }
            }
        }
    }
}

TEST(LZ78GraphCoder, EncodesLargeGraph) {
    auto coder = LZ78GraphCoder();

    double p = 0.01;
    int n = 10000;
    int noEdges = int(double(n * (n - 1)) / (double) 2) * p;
    auto graph = Graph::Generators::GenerateER(n, noEdges);
    std::cerr << "Graph generated" << std::endl;
    auto encoded = coder.encode(graph);
    auto decodedGraph = coder.decode(encoded);
    ASSERT_TRUE(decodedGraph->GetNodes() == graph->GetNodes());
    ASSERT_TRUE(decodedGraph->GetEdges() == graph->GetEdges());
    for (auto edgeIt = graph->BegEI(); edgeIt < graph->EndEI(); edgeIt++) {
        ASSERT_TRUE(decodedGraph->IsEdge(edgeIt.GetSrcNId(), edgeIt.GetDstNId()));
    }
    std::cerr << "Encoded size: " << 64 + encoded.code.size()*8 << " Entropy: "
              << ::Nautytoolkit::get_graph_entropy(graph) << std::endl;
}