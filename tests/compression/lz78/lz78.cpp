//
// Created by pawel on 9/11/20.
//



#include "gtest/gtest.h"
#include <random>
#include "compression/lz78/lz78.h"

TEST(TestLZ78, CompressesCorrectlySmallRandom) {
    std::random_device rd;
    std::uniform_int_distribution<unsigned char> dist(0, 10);

    for (int i = 0; i < 100; ++i) {
        auto coder = LZ78::LZ78();

        auto toEncode = std::vector<char>();

        for (int j = 0; j < i * 10; ++j) {
            toEncode.push_back(dist(rd));
        }
        auto result = coder.encode(toEncode);
        std::cerr << "encoded " << i << std::endl;
        auto decoded = coder.decode(result);

        ASSERT_EQ(decoded.size(), toEncode.size());
        for (int k = 0; k < toEncode.size(); ++k) {
            ASSERT_EQ(decoded[k], toEncode[k]);
        }
    }
}


TEST(TestLZ78, CompressesLongBinarySequenceOptimally) {

    auto toEncode = std::vector<char>();
    const int vec_size = (1<<16);

    std::random_device rd;
    std::uniform_int_distribution<int> dist(0, 1);

    for (int j = 0; j < vec_size; ++j) {
        toEncode.push_back(dist(rd));
    }
    std::cerr << "SEQUENCE GENERATED: " << std::endl;

    std::cerr << std::endl;
    auto coder = LZ78::LZ78();
    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;
    std::cerr << "size of input vector: " << vec_size << "size of result: "
              << result.size()*8 << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}

TEST(TestLZ78, CompressesLongSequenceOptimally) {

    auto toEncode = std::vector<char>();
    const int vec_size = (1<<24);
    srand(time(0));

    for (int j = 0; j < vec_size; ++j) {
        toEncode.push_back(rand() % 15001);
    }
    std::cerr << "SEQUENCE GENERATED: " << std::endl;

    auto coder = LZ78::LZ78();
    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;
    std::cerr << "size of input vector: " << vec_size << "size of result: "
              << result.size()*8 << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}