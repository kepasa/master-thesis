//
// Created by pawel on 7/16/20.
//

#include <random>
#include "gtest/gtest.h"
#include "compression/arith_coder/adaptive_integer_coder.h"

using ArithmeticCoding::AdaptiveIntegerCoder;

TEST(TestAdaptiveIntegerCoder, CompressesCorrectlySmallRandom) {
    auto coder = AdaptiveIntegerCoder<int>(1);
    std::random_device rd;
    std::uniform_int_distribution<int> dist(0, 1);
    auto toEncode = std::vector<int>();

    for(int i = 0; i < 50; ++i) {

        for(int j = 0; j < i*10; ++j) {
            toEncode.push_back(dist(rd));
        }
        auto result = coder.encode(toEncode);

        std::cerr << "END OF ENCODE" << std::endl;

        auto decoded = coder.decode(result);

        ASSERT_EQ(decoded.size(), toEncode.size());
        for (int k = 0; k < toEncode.size(); ++k) {
            ASSERT_EQ(decoded[k], toEncode[k]);
        }
    }
}

TEST(TestAdaptiveIntegerCoder, CompressesCorrectly) {
    auto coder = AdaptiveIntegerCoder<int>(1);

    auto toEncode = std::vector<int>{0, 0, 1, 1, 0};
    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}

TEST(TestAdaptiveIntegerCoder, CompressEmptySequence) {
    auto coder = AdaptiveIntegerCoder<int>(1);

    auto toEncode = std::vector<int>();
    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE, encoded size: " << result.size() << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
}

TEST(TestAdaptiveIntegerCoder, CompressesCorrectly2) {
    auto coder = AdaptiveIntegerCoder<int>(4);

    auto toEncode = std::vector<int>{0, 4, 1, 3, 2, 2, 3, 1, 1, 1, 0, 3};
    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}

TEST(TestAdaptiveIntegerCoder, CompressesLongRandomSequenceCorrectly) {
    auto coder = AdaptiveIntegerCoder<int>(15000);
    srand(time(0));
    std::vector<int> toEncode;
    const int vec_size = (1 << 24);
    toEncode.reserve(vec_size);
    for (int i = 0; i < vec_size; ++i) {
        toEncode.push_back(rand() % 15001);
    }
    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;
    std::cerr << "size of input vector: " << vec_size << "size of result: "
              << result.size() << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}

TEST(TestAdaptiveIntegerCoder, CompressesPermutation) {
    auto coder = AdaptiveIntegerCoder<int>(6167);
    srand(time(0));
    std::vector<int> toEncode;
    for (int i = 0; i < 6167; ++i) {
        toEncode.push_back(i+1);
    }
    std::random_shuffle(toEncode.begin(), toEncode.end());

    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;
    std::cerr << "size of input vector (bits): " << (sizeof(int)*toEncode.size()) << " size of result(bits): "
              << result.size() << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}

TEST(TestAdaptiveIntegerCoder, CompressesCorrectlySkewedSequence) {
    auto coder = AdaptiveIntegerCoder<char>(1);

    std::vector<char> toEncode;
    const int vec_size = (1 << 24);
    toEncode.reserve(vec_size);

    std::random_device rd;
    std::uniform_int_distribution<int> dist(0, 100);

    for (int i = 0; i < vec_size; ++i) {
        (dist(rd) > 80) ? toEncode.push_back(1) : toEncode.push_back(0);
    }
    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;
    std::cerr << "size of input vector: " << vec_size << "size of result: "
              << result.size() << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}

TEST(TestAdaptiveIntegerCoder, SampleGraph) {
    auto coder = AdaptiveIntegerCoder<char>(1);

    std::vector<char> toEncode;
    const int vec_size = 499500;
    toEncode.reserve(vec_size);

    std::random_device rd;
    std::uniform_int_distribution<int> dist(0, 100);

    for (int i = 0; i < vec_size; ++i) {
        (dist(rd) > 90) ? toEncode.push_back(1) : toEncode.push_back(0);
    }
    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;
    std::cerr << "size of input vector: " << vec_size << "size of result: "
              << result.size() << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}