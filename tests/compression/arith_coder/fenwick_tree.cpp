//
// Created by pawel on 7/16/20.
//
#include "gtest/gtest.h"
#include "compression/arith_coder/fenwick_tree.h"
constexpr static int freqs[] = {1, 1, 1, 4, 3, 5, 2, 3, 6, 5, 4, 1, 1, 9};
constexpr static int expected_cum_freqs[] = {1, 2, 3, 7, 10, 15, 17, 20, 26, 31, 35, 36, 37, 46};

using ArithmeticCoding::FenwickTree;

TEST(TestFenwickTree, ReturnsCorrectCumulativeFrequencies) {
    auto tree = FenwickTree(14);

    for(int i = 0; i < 14; ++i) {
        tree.fenwick_incr_frequency(i, freqs[i]-1); // cuz fenwick tree initializes freqs to 1
    }

    for(int i = 0; i < 14; ++i) {
        ASSERT_EQ(expected_cum_freqs[i], tree.fenwick_get_frequency(i));
    }
}

TEST(TestFenwickTree, ReturnsCorrectSymbolForFrequency) {
    auto tree = FenwickTree(14);

    for(int i = 0; i < 14; ++i) {
        tree.fenwick_incr_frequency(i, freqs[i]-1); // cuz fenwick tree initializes freqs to 1
    }

    ASSERT_EQ(8, tree.fenwick_get_symbol(23));
    ASSERT_EQ(13, tree.fenwick_get_symbol(40));
    ASSERT_EQ(1, tree.fenwick_get_symbol(1));
    ASSERT_EQ(0, tree.fenwick_get_symbol(0));
    ASSERT_EQ(10, tree.fenwick_get_symbol(34));
}

TEST(TestFenwickTree, IncrementWorksCorrectly) {
    auto tree = FenwickTree(14);

    for(int i = 0; i < 14; ++i) {
        tree.fenwick_incr_frequency(i, freqs[i]-1); // cuz fenwick tree initializes freqs to 1
    }

    const int incr_value = 10;
    tree.fenwick_incr_frequency(5, incr_value);

    for(int i = 0; i < 5; ++i) {
        ASSERT_EQ(expected_cum_freqs[i], tree.fenwick_get_frequency(i));
    }
    for(int i = 5; i < 14; ++i) {
        ASSERT_EQ(expected_cum_freqs[i]+incr_value, tree.fenwick_get_frequency(i));
    }
}

TEST(TestFenwickTree, HalveFreqsWorksCorrectly) {
    auto tree = FenwickTree(14);

    for(int i = 0; i < 14; ++i) {
        tree.fenwick_incr_frequency(i, freqs[i]-1); // cuz fenwick tree initializes freqs to 1
    }

    tree.halveFreqs();

    ASSERT_EQ(std::max((freqs[0])/2, 1), tree.fenwick_get_frequency(0));
    for(int i = 1; i < 14; ++i) {
        auto curr_freq = (tree.fenwick_get_frequency(i) - tree.fenwick_get_frequency(i-1));
        ASSERT_EQ(std::max((freqs[i])/2, 1), curr_freq);
    }
}