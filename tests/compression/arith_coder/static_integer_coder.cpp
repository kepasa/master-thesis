//
// Created by pawel on 9/6/20.
//

#include <random>
#include "gtest/gtest.h"
#include "compression/arith_coder/static_integer_coder.h"

using ArithmeticCoding::StaticIntegerCoder;

TEST(TestStaticIntegerCoder, CompressesCorrectlySmallRandom) {
    std::unordered_map<int, int> symbolsFreqs;
    symbolsFreqs[0] = 100;
    symbolsFreqs[1] = 100;

    auto coder = StaticIntegerCoder<int>(symbolsFreqs, 1);
    std::random_device rd;
    std::uniform_int_distribution<int> dist(0, 1);
    auto toEncode = std::vector<int>();

    for (int i = 0; i < 50; ++i) {

        for (int j = 0; j < i * 10; ++j) {
            toEncode.push_back(dist(rd));
        }
        auto result = coder.encode(toEncode);

        std::cerr << "END OF ENCODE" << std::endl;

        auto decoded = coder.decode(result);

        ASSERT_EQ(decoded.size(), toEncode.size());
        for (int k = 0; k < toEncode.size(); ++k) {
            ASSERT_EQ(decoded[k], toEncode[k]);
        }
    }
}

TEST(TestStaticIntegerCoder, CompressesCorrectly) {
    std::unordered_map<int, int> symbolsFreqs;
    symbolsFreqs[0] = 100;
    symbolsFreqs[1] = 100;

    auto coder = StaticIntegerCoder<int>(symbolsFreqs, 1);
    auto toEncode = std::vector<int>{0, 0, 1, 1, 0};
    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}

TEST(TestStaticIntegerCoder, CompressEmptySequence) {
    std::unordered_map<int, int> symbolsFreqs;
    symbolsFreqs[0] = 100;
    symbolsFreqs[1] = 100;

    auto coder = StaticIntegerCoder<int>(symbolsFreqs, 1);
    auto toEncode = std::vector<int>();
    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE, encoded size: " << result.size() << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
}

TEST(TestStaticIntegerCoder, CompressesCorrectly2) {
    std::unordered_map<int, int> symbolsFreqs;
    symbolsFreqs[0] = 100;
    symbolsFreqs[1] = 100;
    symbolsFreqs[2] = 100;
    symbolsFreqs[3] = 100;
    symbolsFreqs[4] = 100;


    auto coder = StaticIntegerCoder<int>(symbolsFreqs, 4);
    auto toEncode = std::vector<int>{0, 4, 1, 3, 2, 2, 3, 1, 1, 1, 0, 3};
    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}

TEST(TestStaticIntegerCoder, CompressesLongRandomSequenceCorrectly) {
    std::unordered_map<int, int> symbolsFreqs;
    for (int i = 0; i < 15001; ++i) {
        symbolsFreqs[i] = 100;
    }

    auto coder = StaticIntegerCoder<int>(symbolsFreqs, 15000);
    srand(time(0));
    std::vector<int> toEncode;
    const int vec_size = (1 << 24);
    toEncode.reserve(vec_size);
    for (int i = 0; i < vec_size; ++i) {
        toEncode.push_back(rand() % 15001);
    }
    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;
    std::cerr << "size of input vector: " << vec_size << "size of result: "
              << result.size() << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}

TEST(TestStaticIntegerCoder, CompressesPermutation) {
    std::unordered_map<int, int> symbolsFreqs;
    for (int i = 0; i < 6168; ++i) {
        symbolsFreqs[i] = 100;
    }

    auto coder = StaticIntegerCoder<int>(symbolsFreqs, 6167);
    srand(time(0));
    std::vector<int> toEncode;
    for (int i = 0; i < 6167; ++i) {
        toEncode.push_back(i + 1);
    }
    std::random_shuffle(toEncode.begin(), toEncode.end());

    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;
    std::cerr << "size of input vector (bits): " << (sizeof(int) * toEncode.size()) << " size of result(bits): "
              << result.size() << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}

TEST(TestStaticIntegerCoder, CompressesCorrectlySkewedSequence) {
    std::unordered_map<char, int> symbolsFreqs;
    symbolsFreqs[0] = 800;
    symbolsFreqs[1] = 200;
    auto coder = StaticIntegerCoder<char>(symbolsFreqs, 1);

    std::vector<char> toEncode;
    const int vec_size = (1 << 24);
    toEncode.reserve(vec_size);

    std::random_device rd;
    std::uniform_int_distribution<int> dist(0, 100);

    for (int i = 0; i < vec_size; ++i) {
        (dist(rd) > 80) ? toEncode.push_back(1) : toEncode.push_back(0);
    }
    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;
    std::cerr << "size of input vector: " << vec_size << "size of result: "
              << result.size() << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}

TEST(TestStaticIntegerCoder, SampleGraph) {
    std::unordered_map<char, int> symbolsFreqs;
    symbolsFreqs[0] = 99;
    symbolsFreqs[1] = 1;
    auto coder = StaticIntegerCoder<char>(symbolsFreqs, 1);

    long long n = 40000;
    const int vec_size = int(double(n * (n - 1)) / (double) 2);
    std::cerr << "vec_size: " << vec_size << std::endl;
    std::vector<char> toEncode(vec_size, 0);


    std::random_device rd;
    std::uniform_int_distribution<int> dist(0, toEncode.size() - 1);

    for (int i = 0; i < vec_size/100; ++i) {
        int idx = dist(rd);
        if(toEncode[idx] == 0) {
            toEncode[idx] = 1;
            ++i;
        }
    }
    std::cerr << "Generated sequence" << std::endl;
    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;
    std::cerr << "size of input vector: " << vec_size << "size of result: "
              << result.size() << " bits per symbol: " << ((double) vec_size / (double) result.size()) << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}