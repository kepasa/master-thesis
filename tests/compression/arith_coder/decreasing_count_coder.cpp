//
// Created by pawel on 9/9/20.
//


#include <random>
#include <compression/arith_coder/decreasing_count_coder.h>
#include "gtest/gtest.h"

using ArithmeticCoding::DecreasingCountCoder;

TEST(TestDecreasingCountCoder, CompressesCorrectlySmallRandom) {
    std::unordered_map<int, int> symbolsFreqs;
//    symbolsFreqs[0] = 100;
//    symbolsFreqs[1] = 100;

    std::random_device rd;
    std::uniform_int_distribution<int> dist(0, 1);
    auto toEncode = std::vector<int>();

    for (int i = 0; i < 50; ++i) {

        for (int j = 0; j < i * 10; ++j) {
            toEncode.push_back(dist(rd));
        }
        symbolsFreqs[0] = std::count(toEncode.begin(), toEncode.end(), 0);
        symbolsFreqs[1] = std::count(toEncode.begin(), toEncode.end(), 1);
        auto coder = DecreasingCountCoder<int>(symbolsFreqs, 1);

        auto result = coder.encode(toEncode);

        std::cerr << "END OF ENCODE" << std::endl;

        auto decoded = coder.decode(result);

        ASSERT_EQ(decoded.size(), toEncode.size());
        for (int k = 0; k < toEncode.size(); ++k) {
            ASSERT_EQ(decoded[k], toEncode[k]);
        }
    }
}

TEST(TestDecreasingCountCoder, CompressesCorrectly) {
    std::unordered_map<int, int> symbolsFreqs;
    symbolsFreqs[0] = 3;
    symbolsFreqs[1] = 2;

    auto coder = DecreasingCountCoder<int>(symbolsFreqs, 1);
    auto toEncode = std::vector<int>{0, 0, 1, 1, 0};
    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}

TEST(TestDecreasingCountCoder, CompressEmptySequence) {
    std::unordered_map<int, int> symbolsFreqs;
    symbolsFreqs[0] = 0;
    symbolsFreqs[1] = 0;

    auto coder = DecreasingCountCoder<int>(symbolsFreqs, 1);
    auto toEncode = std::vector<int>();
    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE, encoded size: " << result.size() << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
}

TEST(TestDecreasingCountCoder, CompressesCorrectly2) {
    std::unordered_map<int, int> symbolsFreqs;


    auto toEncode = std::vector<int>{0, 4, 1, 3, 2, 2, 3, 1, 1, 1, 0, 3};

    symbolsFreqs[0] = std::count(toEncode.begin(), toEncode.end(), 0);
    symbolsFreqs[1] = std::count(toEncode.begin(), toEncode.end(), 1);
    symbolsFreqs[2] = std::count(toEncode.begin(), toEncode.end(), 2);
    symbolsFreqs[3] = std::count(toEncode.begin(), toEncode.end(), 3);
    symbolsFreqs[4] = std::count(toEncode.begin(), toEncode.end(), 4);
    auto coder = DecreasingCountCoder<int>(symbolsFreqs, 4);

    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}

TEST(TestDecreasingCountCoder, CompressesLongRandomSequenceCorrectly) {
    std::unordered_map<int, int> symbolsFreqs;
    for (int i = 0; i < 15001; ++i) {
        symbolsFreqs[i] = 0;
    }

    srand(time(0));
    std::vector<int> toEncode;
    const int vec_size = (1 << 24);
    toEncode.reserve(vec_size);
    for (int i = 0; i < vec_size; ++i) {
        auto val = rand() % 15001;
        toEncode.push_back(val);
        symbolsFreqs[val] += 1;
    }
    std::cerr << "GENERATED SEQUENCE" << std::endl;

    auto coder = DecreasingCountCoder<int>(symbolsFreqs, 15000);

    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;
    std::cerr << "size of input vector: " << vec_size << "size of result: "
              << result.size() << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}

TEST(TestDecreasingCountCoder, CompressesPermutation) {
    std::unordered_map<int, int> symbolsFreqs;
    for (int i = 0; i < 6168; ++i) {
        symbolsFreqs[i] = 1;
    }

    auto coder = DecreasingCountCoder<int>(symbolsFreqs, 6167);
    srand(time(0));
    std::vector<int> toEncode;
    toEncode.reserve(6167);
    for (int i = 0; i < 6167; ++i) {
        toEncode.push_back(i + 1);
    }
    std::shuffle(toEncode.begin(), toEncode.end(), std::mt19937(std::random_device()()));

    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;
    std::cerr << "size of input vector (bits): " << (sizeof(int) * toEncode.size()) << " size of result(bits): "
              << result.size() << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}

TEST(TestDecreasingCountCoder, CompressesCorrectlySkewedSequence) {
    std::unordered_map<char, int> symbolsFreqs;
    symbolsFreqs[0] = 0;
    symbolsFreqs[1] = 0;

    std::vector<char> toEncode;
    const int vec_size = (1 << 24);
    toEncode.reserve(vec_size);

    std::random_device rd;
    std::uniform_int_distribution<int> dist(0, 100);

    for (int i = 0; i < vec_size; ++i) {
        if (dist(rd) > 80) {
            toEncode.push_back(1);
            symbolsFreqs[1] += 1;
        } else {
            toEncode.push_back(0);
            symbolsFreqs[0] += 1;
        }
    }
    auto coder = DecreasingCountCoder<char>(symbolsFreqs, 1);

    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;
    std::cerr << "size of input vector: " << vec_size << "size of result: "
              << result.size() << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}
