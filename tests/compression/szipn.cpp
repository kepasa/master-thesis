//
// Created by pawel on 5/11/20.
//

#include <random>
#include "gtest/gtest.h"
#include "generators/generators.h"
#include "compression/szip/szipn/szipn.h"
#include "compression/szip/szipe/szipe.h"
#include "utils/graph.h"

using Graph::Compression::SzipN;
using Graph::Compression::SzipE;
using Graph::Generators::GenerateER;

// Generate larger graphs and use TGHash heuristic from SNAP to check graph isomorphism
TEST(TestSzipN, HashingCheck) {
    auto compressor = new SzipN();
    auto inGraph = GenerateER(30, 50);
    auto inConverted = TSnap::ConvertGraph<PNGraph>(inGraph);

    auto compressedGraph = compressor->encode(inGraph);
    auto outGraph = compressor->decode(compressedGraph);

    auto outConverted = TSnap::ConvertGraph<PNGraph>(outGraph);

    TGHash<TInt> GHash(false);
    TInt::Rnd.PutSeed(time(nullptr));
    GHash.AddDat(inConverted)++;
    GHash.AddDat(outConverted)++;

    ASSERT_EQ(GHash.Len(), 1);
}
