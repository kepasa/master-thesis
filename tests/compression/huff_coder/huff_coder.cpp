//
// Created by pawel on 9/6/20.
//

#include "gtest/gtest.h"
#include "compression/huff_coder/huff_coder.h"
#include <random>

using HuffmanCoding::HuffmanCoder;

TEST(TestHuffmanCoder, CompressesCorrectlySmallRandom) {
    auto coder = HuffmanCoder();
    std::random_device rd;
    std::uniform_int_distribution<int> dist(0, 10);

    for (int i = 0; i < 100; ++i) {
        auto toEncode = std::vector<int>();

        for (int j = 0; j < i * 10; ++j) {
            toEncode.push_back(dist(rd));
        }
        auto result = coder.encode(toEncode);

        auto decoded = coder.decode(result);

        ASSERT_EQ(decoded.size(), toEncode.size());
        for (int k = 0; k < toEncode.size(); ++k) {
            ASSERT_EQ(decoded[k], toEncode[k]);
        }
    }
}


TEST(TestHuffmanCoder, CompressesLongSequenceOptimally) {
    auto coder = HuffmanCoder();


    auto toEncode = std::vector<int>();
    const int vec_size = (1<<16);

    std::random_device rd;
    std::uniform_int_distribution<int> dist(0, 3);

    for (int j = 0; j < vec_size; ++j) {
        toEncode.push_back(dist(rd));
    }
    auto result = coder.encode(toEncode);

    std::cerr << "END OF ENCODE" << std::endl;
    std::cerr << "size of input vector: " << vec_size << "size of result: "
              << result.code.size() << std::endl;

    auto decoded = coder.decode(result);

    ASSERT_EQ(decoded.size(), toEncode.size());
    for (int i = 0; i < toEncode.size(); ++i) {
        ASSERT_EQ(decoded[i], toEncode[i]);
    }
}

TEST(TestHuffmanCoder, CompressesSingleSymbol) {
    auto coder = HuffmanCoder();
    std::random_device rd;

    int symbol = 0;

    for (int i = 0; i < 100; ++i) {
        auto toEncode = std::vector<int>();

        for (int j = 0; j < i * 10; ++j) {
            toEncode.push_back(symbol);
        }
        auto result = coder.encode(toEncode);

        auto decoded = coder.decode(result);

        ASSERT_EQ(decoded.size(), toEncode.size());
        for (int k = 0; k < toEncode.size(); ++k) {
            ASSERT_EQ(decoded[k], toEncode[k]);
        }
    }
}