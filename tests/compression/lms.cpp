//
// Created by pawel on 7/27/20.
//

#include <generators/generators.h>
#include <compression/lms/lms.h>
#include <utils/graph.h>
#include "gtest/gtest.h"


using Graph::Generators::GeneratePE;
using Graph::Generators::GenerateER;
using Graph::Compression::LMS;
using Utils::checkIsomorphic;
using Utils::checkIsomorphic_V2;
using Utils::undirToDir;
using Utils::dirToUndir;

TEST(TestLMS, BasicTest) {
    for (int noNodes = 3; noNodes < 10; ++noNodes) {

        for (int i = 0; i < 200; ++i) {
            for (int noEdges = 3; noEdges <= 5; ++noEdges) {
                LMS compressor = LMS(0);

                auto graph = GeneratePE(noNodes, noEdges, true);
                auto compressedGraph = compressor.encode(graph);
                auto decodedGraph = compressor.decode(compressedGraph);
                std::cerr << "N: " << noNodes << " E: " << noEdges << " done compressing ";
                bool isIso = checkIsomorphic(graph, decodedGraph);
                std::cerr << " done checking " << std::endl;
                if (!isIso) {
                    TSnap::DrawGViz<PNEGraph>(graph, gvlDot, "gviz_plot_input.png", "");
                    TSnap::DrawGViz<PNEGraph>(decodedGraph, gvlDot, "gviz_plot_output.png", "");

                    graph->Dump();
                    decodedGraph->Dump();
                }
                ASSERT_TRUE(isIso);
            }
        }
    }
}

// Generate number of small graphs (up to 9 nodes) and check
TEST(TestLMS, CheckIsomorphism_SmallGraph) {
    const auto& isIsomorphic = checkIsomorphic<PUNGraph>;
    LMS compressor = LMS(-1);

    for (int noNodes = 1; noNodes < 10; ++noNodes) {

        for (int noEdges = 0; noEdges <= ((noNodes * (noNodes - 1)) / 2); ++noEdges) {
            auto graph = GenerateER(noNodes, noEdges);
            auto compressedGraph = compressor.encode(undirToDir(graph));
            auto decodedGraph = dirToUndir(compressor.decode(compressedGraph));
            if(!isIsomorphic(graph, decodedGraph)) {
                graph->Dump();
                decodedGraph->Dump();
                TSnap::DrawGViz<PUNGraph>(decodedGraph, gvlDot, "gviz_plot_out.png", "", "decoded");
                TSnap::DrawGViz<PUNGraph>(graph, gvlDot, "gviz_plot_in.png", "", "input");
            }
            ASSERT_TRUE(isIsomorphic(graph, decodedGraph));
        }
    }
};


TEST(TestLMS, CheckIsomorphism_MediumGraph) {
    const auto& isIsomorphic = checkIsomorphic_V2;
    LMS compressor = LMS(-1);

    for (int noNodes = 10; noNodes < 17; ++noNodes) {

        for (int noEdges = ((noNodes * (noNodes - 1)) / 8); noEdges <= ((noNodes * (noNodes - 1)) / 4); ++noEdges) {
            auto graph = GenerateER(noNodes, noEdges);
            auto compressedGraph = compressor.encode(undirToDir(graph));
            auto decodedGraph = dirToUndir(compressor.decode(compressedGraph));
            if(!isIsomorphic(graph, decodedGraph)) {
                graph->Dump();
                decodedGraph->Dump();
                TSnap::DrawGViz<PUNGraph>(decodedGraph, gvlDot, "gviz_plot_out.png", "", "decoded");
                TSnap::DrawGViz<PUNGraph>(graph, gvlDot, "gviz_plot_in.png", "", "input");
            }
            std::cerr << "Done : " << noNodes << ", " << noEdges << std::endl;
            ASSERT_TRUE(isIsomorphic(graph, decodedGraph));
        }
    }
};


