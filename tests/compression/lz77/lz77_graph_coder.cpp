//
// Created by pawel on 9/11/20.
//

#include <generators/generators.h>
#include <compression/lz77/lz77_graph_coder.h>
#include "gtest/gtest.h"
#include "benchmarks/nauty_toolkit.h"

using LZ77::LZ77GraphCoder;

TEST(LZ77GraphCoder, EncodesCorrectly_SmallGraphs) {
    auto coder = LZ77GraphCoder();
    for (int noNodes = 1; noNodes < 10; ++noNodes) {

        for (int noEdges = 0; noEdges <= ((noNodes * (noNodes - 1)) / 2); ++noEdges) {
            auto graph = Graph::Generators::GenerateER(noNodes, noEdges);
            auto encoded = coder.encode(graph);
            auto decodedGraph = coder.decode(encoded);
            for (int i = 0; i < noNodes; ++i) {
                for (int j = 0; j < noNodes; ++j) {
                    if (i == j) continue;
                    ASSERT_TRUE(graph->IsEdge(i, j) == decodedGraph->IsEdge(i, j));
                }
            }
        }
    }
}

TEST(LZ77GraphCoder, ConvertsCorrectly_MediumGraphs) {
    auto coder = LZ77GraphCoder();

    for (int noNodes = 10; noNodes < 17; ++noNodes) {

        for (int noEdges = ((noNodes * (noNodes - 1)) / 8); noEdges <= ((noNodes * (noNodes - 1)) / 4); ++noEdges) {
            auto graph = Graph::Generators::GenerateER(noNodes, noEdges);
            auto encoded = coder.encode(graph);
            auto decodedGraph = coder.decode(encoded);
            for (int i = 0; i < noNodes; ++i) {
                for (int j = 0; j < noNodes; ++j) {
                    if (i == j) continue;
                    ASSERT_TRUE(graph->IsEdge(i, j) == decodedGraph->IsEdge(i, j));
                }
            }
        }
    }
}

TEST(LZ77GraphCoder, EncodesLargeGraph) {
    auto coder = LZ77GraphCoder();

    double p = 0.01;
    int n = 10000;
    int noEdges = int(double(n * (n - 1)) / (double) 2) * p;
    auto graph = Graph::Generators::GenerateER(n, noEdges);
    std::cerr << "Graph generated" << std::endl;
    auto encoded = coder.encode(graph);
    auto decodedGraph = coder.decode(encoded);
    ASSERT_TRUE(decodedGraph->GetNodes() == graph->GetNodes());
    ASSERT_TRUE(decodedGraph->GetEdges() == graph->GetEdges());
    for (auto edgeIt = graph->BegEI(); edgeIt < graph->EndEI(); edgeIt++) {
        ASSERT_TRUE(decodedGraph->IsEdge(edgeIt.GetSrcNId(), edgeIt.GetDstNId()));
    }
    std::cerr << "Encoded size: " << 64 + LZ77::LZ77().get_encoded_bits(encoded.code) << " Entropy: "
              << ::Nautytoolkit::get_graph_entropy(graph) << std::endl;
}