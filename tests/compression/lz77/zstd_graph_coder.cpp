//
// Created by pawel on 9/12/20.
//

#include <generators/generators.h>
#include <compression/lz77/zstd_graph_coder.h>
#include <benchmarks/nauty_toolkit.h>
#include "gtest/gtest.h"

using LZ77::ZSTDGraphCoder;

TEST(ZSTDGraphCoder, EncodesCorrectly_SmallGraphs) {
    auto coder = ZSTDGraphCoder();
    for (int noNodes = 1; noNodes < 10; ++noNodes) {

        for (int noEdges = 0; noEdges <= ((noNodes * (noNodes - 1)) / 2); ++noEdges) {
            auto graph = Graph::Generators::GenerateER(noNodes, noEdges);
            auto encoded = coder.encode(graph);
            auto decodedGraph = coder.decode(encoded);

            for (int i = 0; i < noNodes; ++i) {
                for (int j = 0; j < noNodes; ++j) {
                    if (i == j) continue;
                    ASSERT_TRUE(graph->IsEdge(i, j) == decodedGraph->IsEdge(i, j));
                }
            }
        }
    }
}

TEST(ZSTDGraphCoder, ConvertsCorrectly_MediumGraphs) {
    auto coder = ZSTDGraphCoder();

    for (int noNodes = 10; noNodes < 17; ++noNodes) {

        for (int noEdges = ((noNodes * (noNodes - 1)) / 8); noEdges <= ((noNodes * (noNodes - 1)) / 4); ++noEdges) {
            auto graph = Graph::Generators::GenerateER(noNodes, noEdges);
            auto encoded = coder.encode(graph);
            auto decodedGraph = coder.decode(encoded);
            for (int i = 0; i < noNodes; ++i) {
                for (int j = 0; j < noNodes; ++j) {
                    if (i == j) continue;
                    ASSERT_TRUE(graph->IsEdge(i, j) == decodedGraph->IsEdge(i, j));
                }
            }
        }
    }
}

TEST(ZSTDGraphCoder, EncodesLargeGraph) {
    auto coder = ZSTDGraphCoder();

    double p = 0.01;
    int n = 10000;
    int noEdges = int(double(n * (n - 1)) / (double) 2) * p;
    auto graph = Graph::Generators::GenerateER(n, noEdges);
    std::cerr << "Graph generated" << std::endl;
    auto encoded = coder.encode(graph);
    auto decodedGraph = coder.decode(encoded);
    ASSERT_TRUE(decodedGraph->GetNodes() == graph->GetNodes());
    ASSERT_TRUE(decodedGraph->GetEdges() == graph->GetEdges());
    for (auto edgeIt = graph->BegEI(); edgeIt < graph->EndEI(); edgeIt++) {
        ASSERT_TRUE(decodedGraph->IsEdge(edgeIt.GetSrcNId(), edgeIt.GetDstNId()));
    }
    std::cerr << "Encoded size: " << 64 + encoded.code.size() << " Entropy: "
              << ::Nautytoolkit::get_graph_entropy(graph) << std::endl;
}