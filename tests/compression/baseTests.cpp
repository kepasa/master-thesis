//
// Created by pawel on 6/13/20.
//

#ifndef MAGISTERKA_BASETESTS_H
#define MAGISTERKA_BASETESTS_H
#include <gtest/gtest.h>
#include <Snap.h>
#include <compression/compressor.h>
#include <compression/szip/szipn/szipn.h>
#include <compression/szip/szipe/szipe.h>
#include <compression/lms/lms.h>
#include <generators/generators.h>
#include <utils/graph.h>

using Graph::Compression::Compressor;
using Graph::Compression::CompressedGraph;
using Graph::Compression::SzipN;
using Graph::Compression::SzipE;
using Graph::Compression::LMS;
using Graph::Generators::GenerateER;
using Utils::checkIsomorphic;
using Utils::checkIsomorphic_V2;

typedef std::tuple<
        std::function<bool(const PUNGraph&, const PUNGraph&)>,
        Compressor<const PUNGraph, CompressedGraph>&,
        std::string
> ParamTestType;

//http://sandordargo.com/blog/2019/04/24/parameterized-testing-with-gtest
class ERGraphsCompressionTest_SmallGraph_Suite :public ::testing::TestWithParam<ParamTestType> {
public:
    struct PrintToStringParamName {
        template <class ParamType>
        std::string operator()( const testing::TestParamInfo<ParamType>& info ) const
        {
            auto params = static_cast<ParamTestType>(info.param);
            return std::get<2>(params);
        }
    };
};

class ERGraphsCompressionTest_MediumGraph_Suite : public ERGraphsCompressionTest_SmallGraph_Suite {

};

// Generate number of small graphs (up to 9 nodes) and check
TEST_P(ERGraphsCompressionTest_SmallGraph_Suite, CheckIsomorphism_SmallGraph) {
    const auto& isIsomorphic = std::get<0>(GetParam());
    auto& compressor = std::get<1>(GetParam());

    for (int noNodes = 1; noNodes < 10; ++noNodes) {

        for (int noEdges = 0; noEdges <= ((noNodes * (noNodes - 1)) / 2); ++noEdges) {
            auto graph = GenerateER(noNodes, noEdges);
            auto compressedGraph = compressor.encode(graph);
            auto decodedGraph = compressor.decode(compressedGraph);
            if(!isIsomorphic(graph, decodedGraph)) {
                graph->Dump();
                decodedGraph->Dump();
            }
            ASSERT_TRUE(isIsomorphic(graph, decodedGraph));
        }
    }
};

// Generate number of medium graphs and check
TEST_P(ERGraphsCompressionTest_MediumGraph_Suite, CheckIsomorphism_MediumGraph) {
    const auto& isIsomorphic = std::get<0>(GetParam());
    auto& compressor = std::get<1>(GetParam());

    for (int noNodes = 10; noNodes < 17; ++noNodes) {

        for (int noEdges = ((noNodes * (noNodes - 1)) / 8); noEdges <= ((noNodes * (noNodes - 1)) / 4); ++noEdges) {
            auto graph = GenerateER(noNodes, noEdges);
            auto compressedGraph = compressor.encode(graph);
            auto decodedGraph = compressor.decode(compressedGraph);
            if(!isIsomorphic(graph, decodedGraph)) {
                graph->Dump();
                decodedGraph->Dump();
            }
            ASSERT_TRUE(isIsomorphic(graph, decodedGraph));
        }
    }
};

static SzipN szipN = SzipN();
static SzipE szipE = SzipE();
static LMS lms = LMS(-1);

INSTANTIATE_TEST_SUITE_P(
        CompressorTests,
        ERGraphsCompressionTest_SmallGraph_Suite,
        ::testing::Values(
            std::make_tuple(checkIsomorphic<PUNGraph>, std::ref(szipN), "Basic_iso_SzipN"),
            std::make_tuple(checkIsomorphic<PUNGraph>, std::ref(szipE), "Basic_iso_SzipE"),
            std::make_tuple(checkIsomorphic_V2, std::ref(szipN), "Group_by_deg_iso_SzipN"),
            std::make_tuple(checkIsomorphic_V2, std::ref(szipE), "Group_by_deg_iso_SzipE")
        ),
        ERGraphsCompressionTest_SmallGraph_Suite::PrintToStringParamName()
);

INSTANTIATE_TEST_SUITE_P(
        CompressorTests,
        ERGraphsCompressionTest_MediumGraph_Suite,
        ::testing::Values(
                std::make_tuple(checkIsomorphic_V2, std::ref(szipN), "Group_by_deg_iso_SzipN"),
                std::make_tuple(checkIsomorphic_V2, std::ref(szipE), "Group_by_deg_iso_SzipE")
        ),
        ERGraphsCompressionTest_MediumGraph_Suite::PrintToStringParamName()
);

#endif //MAGISTERKA_BASETESTS_H