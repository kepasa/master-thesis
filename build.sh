#!/bin/bash

if ! ldconfig -p | grep libzstd &> /dev/null
then
  echo "libzstd not found!"
  echo "Please visit: https://github.com/facebook/zstd and make sure you have libzstd installed on your system"
  exit 1
fi

if ! command -v dot &> /dev/null
then
  echo "GraphViz not found!"
  echo "Please visit: https://www.graphviz.org/ and make sure you have GraphViz installed on your system"
  exit 1
fi

if ! command -v gnuplot &> /dev/null
then
  echo "gnuplot not found!"
  echo "Please visit: http://www.gnuplot.info/ and make sure you have gnuplot installed on your system"
  exit 1
fi

if ! command -v cmake &> /dev/null
then
  echo "CMake not found!"
  echo "Please visit: https://cmake.org/ and make sure you have CMake installed on your system"
  exit 1
fi


echo "All dependences met. Building CMake project..."


mkdir release_build
cd release_build
cmake ../
cmake --build .
