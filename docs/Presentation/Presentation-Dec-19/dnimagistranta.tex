\documentclass[10pt]{beamer}

\usetheme{Madrid}
\usepackage[T1]{fontenc}
\usepackage[polish]{babel}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{pgf,tikz,pgfplots}
\usepackage{bm}
\usepackage{wasysym}
\usepackage{gensymb}
\usepackage{amsmath}
\usepackage{mathtools,amsthm}
\usepackage{listings}
\usepackage[labelformat=empty]{caption}
\pgfplotsset{compat=1.15}
\usepackage{mathrsfs}
\usetikzlibrary{arrows}
\graphicspath{ {./graphics/} }
\usepackage{ulem}
\usepackage{hyperref}
\usepackage{nameref}
\DeclareRobustCommand{\hsout}[1]{\texorpdfstring{\sout{#1}}{#1}}

\theoremstyle{definition}
\newtheoremstyle{case}{}{}{}{}{}{:}{ }{}
\theoremstyle{case}
\newtheorem{case}{Case}

\title{
Random graph compression algorithms
}
\author{Paweł Palenica}
\institute{Jagiellonian University}
\date{December 18, 2019}

\definecolor{wrwrwr}{rgb}{0.3803921568627451,0.3803921568627451,0.3803921568627451}
\definecolor{qqwuqq}{rgb}{0,0.39215686274509803,0}
\definecolor{rvwvcq}{rgb}{0.08235294117647059,0.396078431372549,0.7529411764705882}
\definecolor{mGreen}{rgb}{0,0.6,0}
\definecolor{mGray}{rgb}{0.5,0.5,0.5}
\definecolor{mPurple}{rgb}{0.58,0,0.82}
\definecolor{backgroundColour}{rgb}{0.95,0.95,0.92}
\lstdefinestyle{CStyle}{
    backgroundcolor=\color{backgroundColour},   
    commentstyle=\color{mGreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{mGray},
    stringstyle=\color{mPurple},
    basicstyle=\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2,
    language=C
}
 
\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}{Introduction}
Motivation for graph compression
\begin{itemize}
\item<2-> Classic compression algorithms do not take into consideration the information embodied in data structure
\item<3-> There is increasing amount of multidimensional and context dependent data
\item<4-> This data consists of information conveyed by the structure of data itself and the data labels implanted in the structure
\item<5-> We'll mainly focus on the structural information
\end{itemize}
\end{frame}

\note{
Examples gene regulation, protein interactions, metabolic pathways, topographical maps, medical data, volumetric data, social networks, Internet
}

\begin{frame}{Introduction - Erdős–Rényi model}
A graph $\mathcal{G}_{n,p}$ is a graph over $n$ vertices in which edges are added randomly with probability $p$.
\end{frame}

\begin{frame}{Introduction - Preferential Attachment model}
A graph $\mathcal{PA}_{m,n}$ for integer parameter $m \geq 1$ is a graph over $n$ vertices. \pause It is defined inductively on $n$ in the following way:
\begin{itemize}
\item<3-> The graph $G_1 \sim \mathcal{PA}_{m, 1}$ is a single vertex with $m$ self-edges
\item<4-> Inductively, to obtain graph $G_{n+1} \sim \mathcal{PA}_{m, n+1}$ from $G_n$ we add a vertex and make $m$ random choices of neighbours $v_1, \ldots, v_m$ in $G_n$  as follows: for each vertex $w \leq n$ in $G_n$, $\mathbb{P}(v_i = w | G_n, v_1, \ldots, v_{i-1}) = \frac{deg_n(w)}{2mn}$ where $deg_n(w)$ is the degree of vertex $w$ in graph $G_n$.
\end{itemize}
\end{frame}


\begin{frame}{Shortcomings of traditional approach}
Sequence compression struggle to take advantage of the internal symmetries of graph structures
\includegraphics[scale=0.33]{graph1.png}
\end{frame}

\begin{frame}{Graph entropy}
Given a probability distribution on graphs produced by the random graph model $\mathcal{G}$ we define graph entropy \[ H_{\mathcal{G}} = \mathbb{E}[-log P(G)] = -\sum_{G \in \mathcal{G}} P(G)log P(G), \]
where $P(G)$ is probability of a graph $G$.
\end{frame}

\begin{frame}{Structural entropy}
For given structure $S \in \mathcal{S}$, the probability of S can is defined as:
\[ P(S) = \sum_{G \cong S, G \in \mathcal{G}} P(G) \]
where $G \cong S$ means that $S$ is isomorphic to $G$. \pause \vspace{3mm} \\
The structural entropy $H_{\mathcal{S}}$ of a random graph $\mathcal{G}$ is defined as entropy of a random structure $\mathcal{S}$: 
\[ H_{\mathcal{S}} = \mathbb{E}[-log P(S)] = - \sum_{S \in \mathcal{S}} P(S) log P(S) \]
\end{frame}

\begin{frame}{Theoretical results}
For Erdős–Rényi model $\mathcal{G}_{n,p}$  it is proven that structural entropy is bound by \[ H_{\mathcal{S}} = \binom{n}{2}h(p) - n log n + O(n) \] \pause \vspace{4mm} \\
For Preferential Attachment $\mathcal{PA}_{m, n}$ for fixed $m \geq 3$ is is proven that structural entropy is bounded by:
\[ H_{\mathcal{S}} = (m-1)n log n + R(n), \]
where $R(n)$ satisfies $Cn \leq |R(n)| \leq O(n log log n)$ for some nonzero constant $C=C(m)$. \pause \vspace{4mm} \\
For both models we are provided with algorithms that asymptotically achieve structural entropies.
\end{frame}

\note{
Show that the size of \textit{automorphism group} is exactly one with high probability. This means that almost every graph in a random model is asymmetric. Then, using the following equality:
\[ H_{\mathcal{S}} = H_{\mathcal{G}} - log\ n! + \sum_{S \in \mathcal{S}} P(S)log\ |Aut(S)|\]
}

\begin{frame}{Planned work}
Conduct a comprehensive benchmark of theoretical algorithms with asymptotic guarantees against `real world` data compressors (e.g. LZ77, ANS)
\begin{itemize}
\item<2-> Implement dedicated algorithms for structural compression and benchmark algorithms
\item<3-> Compare the aforementioned algorithms on synthetic and real world graphs
\item<4-> Conduct the benchmark for other models of random graphs against heuristic compression algorithms for the models
\end{itemize}
\end{frame}

\note{
$P(G) = p^k q^{\binom{n}{2}-k}$ \\
Theoretically LZ77 can achieve  $(h(p) + o(1))\binom{n}{2}$ for Erdos Renji and $mn log(mn)$ for Preferential Attachment the question is how big is the second term for various graphs
}
\end{document}